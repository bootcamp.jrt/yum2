drop schema if exists yum;
CREATE SCHEMA IF NOT EXISTS `yum` DEFAULT CHARACTER SET utf8 ;
USE Yum;

CREATE TABLE user (
   user_id int NOT NULL  AUTO_INCREMENT,
   last_name varchar(25),
   first_name varchar(25),
   email varchar(25),
   role varchar(25),
   pass varchar(25),
   registration_date Date NOT NULL,
   approved boolean not null default 0,
   picture mediumblob,
   version INT NOT NULL DEFAULT 0,
   token varchar(40),
   expiration TIMESTAMP,
   PRIMARY KEY (user_id)
);


CREATE TABLE food (
	food_id integer not null primary key auto_increment, 
    food_name varchar(20),
    food_type varchar(20),
    description varchar(150),
	version INT NOT NULL DEFAULT 0,
    price double,
    archived boolean DEFAULT false
);

CREATE TABLE daily_menu 
(
	daily_menu_id INT NOT NULL AUTO_INCREMENT,
    daily_menu_date DATE,
	version INT NOT NULL DEFAULT 0,
    PRIMARY KEY (daily_menu_id)
	
);


CREATE TABLE daily_order (
  daily_order_id INT NOT NULL AUTO_INCREMENT,
  user_id INT ,
  daily_menu_id INT,
  version INT NOT NULL DEFAULT 0,
  FOREIGN KEY (user_id) REFERENCES user(user_id), 
  PRIMARY KEY (daily_order_id),
  FOREIGN KEY (daily_menu_id) REFERENCES daily_menu(daily_menu_id)
  );

CREATE TABLE order_item (
	daily_order_id int,
    food_id int,
	FOREIGN KEY (daily_order_id) REFERENCES daily_order(daily_order_id),
    FOREIGN KEY (food_id) REFERENCES food(food_id),
    PRIMARY KEY (daily_order_id, food_id),
    quantity int
); 

CREATE TABLE daily_menu_food (
	daily_menu_id int,
    food_id int,
    FOREIGN KEY (food_id) REFERENCES food(food_id),
    FOREIGN KEY (daily_menu_id) REFERENCES daily_menu(daily_menu_id),
    PRIMARY KEY (daily_menu_id, food_id)
);

CREATE TABLE global_settings (
	global_settings_id INT NOT NULL DEFAULT 1,
	time_last_order TIME,
    currency_type varchar(20),
    notes MEDIUMTEXT,
    terms_service MEDIUMTEXT,
    privacy_policy MEDIUMTEXT,
	version_of_foodlist INT NOT NULL DEFAULT 0,
	version INT NOT NULL DEFAULT 0,
    PRIMARY KEY (global_settings_id)
);

INSERT INTO user
(user_id, last_name, first_name,email,role,pass,registration_date,approved)
VALUES (1, 'admin','admin','admin@yum.com','admin','b4WE4bXVBj0=',CURDATE(), true);


INSERT INTO `global_settings` 
VALUES (1,'13:00:00','&euro;','<p>All meals include slices of bread.</p>&#10;<p>Meals are delivered in a micorwave compatible plastic box. Disposable utensils are also included.</p>&#10;<p>Delivery time is 13:30.</p>&#10;<p>Payments will be collected by the reception desk. Mind to have the exact amount.</p>','<p><span><span>The Company collects three types of information about users: (1) information the user gives us when registering as a customer, (2) information the user gives us to execute the order of the e-FOOD.gr, (3) information the user gives us at stakes competitions held occasionally.&#160;</span><span>Upon completion of any order form on our website, you will be asked your name, address, zip code in your area, your address, your phone.&#160;</span><span>Additionally they can request more specific information, such as dispatch - delivery of an order, billing information or details about the offer you have requested.&#160;</span><span>In no case is the collection and processing of sensitive data.&#160;</span><span>They are not required to provide any additional element, which is not necessary or reasonably be required to provide the services that the user has requested.&#160;</span><span>The Company uses the information you provide during the electronic dispatch of the form in order to contact you regarding (i) the delivery of order at your place, (ii) to confirm and identify the client in any necessary case ( iii) new or alternative products offered by e-FOOD.gr, (iv) special offers e-FOOD.gr, (v) receipt of gifts after the competition draw.&#160;</span><span>You can choose whether or not to receive such communications by the Company by sending your request via e-mail to the email address&#160;</span></span><span><span>info@e-FOOD.gr.&#160;</span><span>The Company fully complies with the provisions of Law. 2472/1997 on the protection of personal data and has made the necessary notifications to the Authority for Personal Data Protection.&#160;</span><span>The processing of personal data of users of the Company performed informed with the consent of the user.</span></span></p>&#10;<p><span><span>The Company is committed to the confidentiality of personal data of all visitors to e-FOOD.gr and to protecting the personal data you may provide to us.&#160;</span><span>Specifically, we consider important to know how personal data treatment nature concerning you and we may receive through this website and the data processing practices we use through the use of Internet and other electronic communication networks.&#160;</span><span>O user is required to read this Privacy Policy and Privacy Policy to understand how we use and protect the information provided or the way of their use and protection in the future.&#160;</span><span>The registration of user data services through this site implies the user consent to the collection, use and transfer of personal data under the terms of this privacy policy privacy.&#160;</span><span>The user, however, will be asked to give explicit consent for any such action.</span></span></p>','<p><span><span>The Company collects three types of information about users: (1) information the user gives us when registering as a customer, (2) information the user gives us to execute the order of the e-FOOD.gr, (3) information the user gives us at stakes competitions held occasionally.&#160;</span><span>Upon completion of any order form on our website, you will be asked your name, address, zip code in your area, your address, your phone.&#160;</span><span>Additionally they can request more specific information, such as dispatch - delivery of an order, billing information or details about the offer you have requested.&#160;</span><span>In no case is the collection and processing of sensitive data.&#160;</span><span>They are not required to provide any additional element, which is not necessary or reasonably be required to provide the services that the user has requested.&#160;</span><span>The Company uses the information you provide during the electronic dispatch of the form in order to contact you regarding (i) the delivery of order at your place, (ii) to confirm and identify the client in any necessary case ( iii) new or alternative products offered by e-FOOD.gr, (iv) special offers e-FOOD.gr, (v) receipt of gifts after the competition draw.&#160;</span><span>You can choose whether or not to receive such communications by the Company by sending your request via e-mail to the email address&#160;</span></span><span><span>info@e-FOOD.gr.&#160;</span><span>The Company fully complies with the provisions of Law. 2472/1997 on the protection of personal data and has made the necessary notifications to the Authority for Personal Data Protection.&#160;</span><span>The processing of personal data of users of the Company performed informed with the consent of the user.</span></span></p>&#10;<p><span><span>The Company is committed to the confidentiality of personal data of all visitors to e-FOOD.gr and to protecting the personal data you may provide to us.&#160;</span><span>Specifically, we consider important to know how personal data treatment nature concerning you and we may receive through this website and the data processing practices we use through the use of Internet and other electronic communication networks.&#160;</span><span>O user is required to read this Privacy Policy and Privacy Policy to understand how we use and protect the information provided or the way of their use and protection in the future.&#160;</span><span>The registration of user data services through this site implies the user consent to the collection, use and transfer of personal data under the terms of this privacy policy privacy.&#160;</span><span>The user, however, will be asked to give explicit consent for any such action.</span></span></p>',0,4);










