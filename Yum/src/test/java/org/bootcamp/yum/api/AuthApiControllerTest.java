
/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import org.bootcamp.test.MockAuthRule;
import org.bootcamp.yum.api.service.AuthorizeService;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserType;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.exceptions.ExceptionControllerAdvice;
import org.joda.time.LocalDate;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;

import static org.mockito.BDDMockito.given;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class AuthApiControllerTest {
    
    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final AuthorizeService authService = new AuthorizeService();

    @Mock
    private UserRepository mockUserRepository;
   
    private MockMvc mockMvc;    
    
    public AuthApiControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
//        mockMvc = MockMvcBuilders.standaloneSetup( new AuthApiController(authService)).build();
        mockMvc = MockMvcBuilders.standaloneSetup(new AuthApiController(authService))
        .setControllerAdvice(new ExceptionControllerAdvice())
        .build();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Testing POST /auth/login : HttpResponses: 200, 400, 404, 500
     * Server returns a token object: 
     * {
     *  "token": "string"
     * }
     * @throws org.bootcamp.yum.api.ApiException
     * @throws java.lang.Exception
     */
    @Test
    public void testAuthLoginPost200() throws Exception{
        LocalDate regDate = LocalDate.now();
        
        User user = new User(2);
        user.setEmail("foo@foo.com");
        user.setPassword("12345678");
        user.setRole(UserType.HUNGRY);
        user.setRegistrationDate(regDate);
        user.setApproved(true);
        user.setVersion(0);
        
        given(mockUserRepository.findByEmail("foo@foo.com")).willReturn(user);   
                
        String json = "{\"email\": \"foo@foo.com\", \"password\": \"12345678\"}";        
      
        mockMvc.perform(post("/api/auth/login")
                       .content(json)
                       .contentType(MediaType.APPLICATION_JSON_UTF8))
                 // 1. Check the Http Status
                .andExpect(status().isOk());
    }

    @Test
    // We try to send a malformed JSON file. The HTTP response will be 400.
    public void testAuthLoginPost400() throws Exception {
        UserTypeConverter userTypeConverter = new UserTypeConverter();
        LocalDate regDate = LocalDate.now();
        
        User user = new User(2);
        user.setEmail("foo@foo.com");
        user.setPassword("12345678");
        user.setRole(UserType.HUNGRY);
        user.setRegistrationDate(regDate);
        user.setVersion(0);
        
        given(mockUserRepository.findByEmail("foo@foo.com")).willReturn(user);   
                
           String json = "{\"email\": \"foo@foo.com\", \"password\": \"1234567\"}";       
      
        mockMvc.perform(post("/api/auth/login")
                       .content(json)
                       .contentType(MediaType.APPLICATION_JSON_UTF8))
                 // 1. Check the Http Status
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }
     
    
    
    @Test
    // We try to send an empty JSON file. The HTTP response will be 500.
    public void testAuthLoginPost500() throws Exception {
        UserTypeConverter userTypeConverter = new UserTypeConverter();
        LocalDate regDate = LocalDate.now();
        
        User user = new User(2);
        user.setEmail("foo@foo.com");
        user.setPassword("12345678");
        user.setRole(UserType.HUNGRY);
        user.setRegistrationDate(regDate);
        user.setVersion(0);
        
        given(mockUserRepository.findByEmail("foo@foo.com")).willReturn(user);   
                
        String json = "{}";       
      
        mockMvc.perform(post("/api/auth/login")
                       .content(json)
                       .contentType(MediaType.APPLICATION_JSON_UTF8))
                 // 1. Check the Http Status
                .andExpect(status().isInternalServerError());
    }    
    
    @Test
    // We try to login with email which not exists. 
    public void testAuthForgotpwdPost400() throws Exception {
        UserTypeConverter userTypeConverter = new UserTypeConverter();
        LocalDate regDate = LocalDate.now();
        
        User user = new User(2);
        user.setEmail("foo@foo.com");
        user.setPassword("12345678");
        user.setRole(UserType.HUNGRY);
        user.setRegistrationDate(regDate);
        user.setVersion(0);
        
        given(mockUserRepository.findByEmail("foo@foo.com")).willReturn(user);   
                
        String json = "{\"email\": \"foo@foo.com}";        
      
        mockMvc.perform(post("/api/auth/forgotpwd")
                       .content(json)
                       .contentType(MediaType.APPLICATION_JSON_UTF8))
                 // 1. Check the Http Status
                .andExpect(status().isBadRequest());
    }
    
    
    
    @Test
    public void testAuthForgotpwdPost500() throws Exception {
        LocalDate regDate = LocalDate.now();
        
        User user = new User(2);
        user.setEmail("foo@foo.com");
        user.setPassword("12345678");
        user.setRole(UserType.HUNGRY);
        user.setRegistrationDate(regDate);
        user.setVersion(0);
        
        given(mockUserRepository.findByEmail("foo@foo.com")).willReturn(user);   
                
        String json = "{}";       
      
        mockMvc.perform(post("/api/auth/forgotpwd")
                       .content(json)
                       .contentType(MediaType.APPLICATION_JSON_UTF8))
                 // 1. Check the Http Status
                .andExpect(status().isInternalServerError());
    }
    
    @Test
    public void testAuthRegisterPost405() throws Exception {
        LocalDate regDate = LocalDate.now();
        
        User user = new User(2);
        user.setEmail("foo@foo.com");
        user.setPassword("12345678");
        user.setRole(UserType.HUNGRY);
        user.setRegistrationDate(regDate);
        user.setVersion(0);
        
        given(mockUserRepository.findByEmail("foo@foo.com")).willReturn(user);   
            
        String json = "{\"firstName\": \"Stavros\",\"lastName\": \"Apostolakis\",\"email\": \"foo@foo.com\",\"password\": \"12345678\"}";
      
        mockMvc.perform(post("/api/auth/register")
                       .content(json)
                       .contentType(MediaType.APPLICATION_JSON_UTF8))
                 // 1. Check the Http Status
                .andExpect(status().isBadRequest());
    }
        
    @Test
    public void testAuthRegisterPost500() throws Exception {
        LocalDate regDate = LocalDate.now();
        
        User user = new User(2);
        user.setEmail("foo@foo.com");
        user.setPassword("12345678");
        user.setRole(UserType.HUNGRY);
        user.setRegistrationDate(regDate);
        user.setVersion(0);
        
        given(mockUserRepository.findByEmail("foo@foo.com")).willReturn(user);   
            
        String json = "{}";
      
        mockMvc.perform(post("/api/auth/register")
                       .content(json)
                       .contentType(MediaType.APPLICATION_JSON_UTF8))
                 // 1. Check the Http Status
                .andExpect(status().isInternalServerError());
    }
}
