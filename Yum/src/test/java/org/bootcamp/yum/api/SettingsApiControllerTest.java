
/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import org.bootcamp.test.MockAuthRule;
import org.bootcamp.yum.api.service.SettingsService;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.bootcamp.yum.data.repository.UserRepository;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.*;
import static org.mockito.BDDMockito.given;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserType;
import org.bootcamp.yum.exceptions.ExceptionControllerAdvice;

@RunWith(MockitoJUnitRunner.class)
public class SettingsApiControllerTest {
    
    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final SettingsService settingsService = new SettingsService();

    @Mock
    private UserRepository mockUserRepository;
   
    private MockMvc mockMvc;
    private User userA;
    private User userB;
    private User userC;
    
    public SettingsApiControllerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
//        mockMvc = MockMvcBuilders.standaloneSetup( new SettingsApiController(settingsService)).build();
        mockMvc = MockMvcBuilders.standaloneSetup(new SettingsApiController(settingsService))
        .setControllerAdvice(new ExceptionControllerAdvice())
        .build();
        
        userA = new User(2);
        userA.setFirstName("Nikos");
        userA.setLastName("Nikos");
        userA.setEmail("foo@foo.com");
       userA.setRole(UserType.HUNGRY);
        userA.setVersion(0);
        
        userB = new User(2);
        userB.setFirstName("Stavros");
        userB.setLastName("Apostolakis");
       userB.setEmail("foo@foo.com");
        userB.setRole(UserType.HUNGRY);
        userB.setPassword("12345678");
        userB.setVersion(0);
        
        userC = new User(2);
        userC.setFirstName("Stavros");
        userC.setLastName("Apostolakis");
        userC.setEmail("foo@foo.com");
        userC.setRole(UserType.HUNGRY);
        userC.setPassword("12345678");
        userC.setVersion(1);
        
        
        
    }
    
    @After
    public void tearDown() {
    }

   /**
    * Testing GET /settings : HttpResponses: 200 
     * Server returns a User object: 
     * {
     * "firstName": "string",
     * "lastName": "string",
     * "email": "string",
     * "role": "string",
     * "picture": "string",
     * "version": integer
    * }
     * @throws java.lang.Exception
     */
    @Ignore
    @Test
    public void testSettingsGet() throws Exception {
   

        UserTypeConverter userTypeConverter = new UserTypeConverter();
    
        given(mockUserRepository.findById(2)).willReturn(userA);   

        mockMvc.perform(get("/api/settings"))
            // 1. Check the Http Status
            .andExpect(status().isOk())
            // 2. Check if the format of the data is in JSON
           // .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
            // 3. Check if the JSON data from the mock repository matching with
            // with the data we create here (user variable)    
            .andExpect(jsonPath("$.firstName", is("Nikos")))
            .andExpect(jsonPath("$.lastName", is("Nikos")))
            .andExpect(jsonPath("$.email", is("foo@foo.com")))
            .andExpect(jsonPath("$.role", is(userTypeConverter.convertToDatabaseColumn(UserType.HUNGRY))))
            .andExpect(jsonPath("$.version", is(0)));  
    }
       
    /**
     * Testing PUT /settings : HttpResponses: 200 , 400 , 409 , 500
     * Server receive a User object: 
     * {
     * "firstName": "string",
     * "lastName": "string",
     * "email": "string",
     * "role": "string",
     * "picture": "string",
     * "version": integer
     * }
     * @throws java.lang.Exception
     */
    
    @Ignore
    @Test
    // We try to update the data of a user which already exists in the DB without
    // change the values of non-increment fields. The HTTP response will be 200.
    public void testSettingsPutResponse200() throws Exception {
//        UserTypeConverter userTypeConverter = new UserTypeConverter();
//        User user = new User(2);
//        user.setFirstName("Stavros");
//        user.setLastName("Apostolakis");
//        user.setEmail("foo@foo.com");
//        user.setRole(UserType.HUNGRY);
//        user.setPassword("12345678");
//        user.setVersion(0);
       
        given(mockUserRepository.findById(2)).willReturn(userB);   
           
        String json = "{\"firstName\": \"Stavros\", "
                     + "\"lastName\" : \"Apostolakis\", "
                     + "\"email\": \"sapost@foo.com\", " 
                     + "\"role\": \"hungry\", "
                     + "\"password\": \"12345678\", "
                     + "\"version\": 0}";
        
        mockMvc.perform(put("/api/settings")
               .content(json)
               .contentType(MediaType.APPLICATION_JSON_UTF8)
            )
            // 1. Check the Http Status
            .andExpect(status().isNoContent());
            // 2. Check if the format of the data is in JSON
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//            .andExpect(jsonPath("$.firstName", is("Stavros")))
//            .andExpect(jsonPath("$.lastName", is("Apostolakis")))
//            .andExpect(jsonPath("$.email", is("sapost@foo.com")))
//            .andExpect(jsonPath("$.role", is(userTypeConverter.convertToDatabaseColumn(UserType.HUNGRY))))
//            .andExpect(jsonPath("$.password", is("12345678")))
//            .andExpect(jsonPath("$.version", is(0)));
    }
    
    @Ignore
    @Test
    // We try to send a malformed JSON file. The HTTP response will be 400.
    public void testSettingsPutResponse400() throws Exception {
        given(mockUserRepository.findById(2)).willReturn(userB);   
           
        String json = "{\"firstName\": \"string\", "
                     + "\"lastName\" : \"string\", "
                     + "\"email\": \"string\", " 
                     + "\"role\": \"string\", "
                     + "\"password\": \"string\", "
                     + "\"version\": 0}";
        
        mockMvc.perform(put("/api/settings")
               .content(json)
               .contentType(MediaType.APPLICATION_JSON_UTF8)
            )
            // 1. Check the Http Status
            .andExpect(status().isBadRequest());
        
    }
    
    @Ignore
    @Test
    // We try to send an empty JSON file. The HTTP response will be 500.
    public void testSettingsPutResponse500() throws Exception {
        given(mockUserRepository.findById(2)).willReturn(userB);   
           
        String json = "{}";
        
        mockMvc.perform(put("/api/settings")
               .content(json)
               .contentType(MediaType.APPLICATION_JSON_UTF8)
            )
            // 1. Check the Http Status
            .andExpect(status().isInternalServerError());
        
    }
    
//***********************************************************************************************************    
    
    @Ignore    
    @Test
    // We try to update the data of a user which already exists in the DB without
    // change the values of non-increment fields again. The version is autoincrement
    // so now will have the value 1. The HTTP response will be 409.
    public void testSettingsPutResponse409() throws Exception {
        UserTypeConverter userTypeConverter = new UserTypeConverter();
        
        User user = new User(2);
        user.setFirstName("Stavros");
        user.setLastName("Apostolakis");
        user.setEmail("foo@foo.com");
        user.setRole(UserType.HUNGRY);
        user.setPassword("12345678");

        user.setVersion(1);
        
        given(mockUserRepository.findById(2)).willReturn(userC);   
           
        String json = "{\"firstName\": \"Nikos\", "
                     + "\"lastName\" : \"Apostolakis\", "
                     + "\"email\": \"sapost@foo.com\", " 
                     + "\"role\": \"hungry\", "
                     + "\"password\": \"12345678\", "
                     + "\"version\": 0}";
           
        mockMvc.perform(put("/api/settings")
               .content(json)
               .contentType(MediaType.APPLICATION_JSON_UTF8)
            )
            // 1. Check the Http Status
            .andExpect(status().isConflict());
            // 2. Check if the format of the data is in JSON
//            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
//            .andExpect(jsonPath("$.firstName", is("Stavros")))
//            .andExpect(jsonPath("$.lastName", is("Apostolakis")))
//            .andExpect(jsonPath("$.email", is("foo@foo.com")))
//            .andExpect(jsonPath("$.role", is(userTypeConverter.convertToDatabaseColumn(UserType.HUNGRY))))
//            .andExpect(jsonPath("$.password", is("12345678")))
//            .andExpect(jsonPath("$.version", is(null)));

    }
    
//***********************************************************************************************************    

    /**
     * Test of settingsPicturePost method, of class SettingsApiController.
     */
//    @Test
//    @Ignore
//    public void testSettingsPicturePost() {
//        System.out.println("settingsPicturePost");
//    }
}
