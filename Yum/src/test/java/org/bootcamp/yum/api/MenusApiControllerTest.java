/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;

import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import static org.mockito.BDDMockito.given;
import static org.hamcrest.Matchers.*;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.bootcamp.yum.data.entity.Food;

import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.yum.api.service.MenusService;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.exceptions.ExceptionControllerAdvice;
import org.joda.time.LocalDate;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MenusApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final MenusService menusService = new MenusService();

    @Mock
    private DailyMenuRepository mockDailyMenuRepository;
    
    @Mock
    private UserRepository mockUserRepository;

    private MockMvc mockMvc;
    private static List<DailyMenu> mockMenuList;
    private static DailyMenu mockMenu;
    private static DailyMenu mockMenu2;
    private static Food mockFood1;
    private static Food mockFood2;
    private static User mockUser;

    @Before
    public void setup() {
       //mockMvc = MockMvcBuilders.standaloneSetup(new MenusApiController(menusService)).build();
        mockMvc = MockMvcBuilders.standaloneSetup(new MenusApiController(menusService))
        .setControllerAdvice(new ExceptionControllerAdvice())
       .build();

    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();   
        mockMenuList = new ArrayList<>();
        mockMenu = new DailyMenu(5);
        mockMenu2 = new DailyMenu(6);
        mockFood1 = new Food();
        mockFood2 = new Food();
        mockMenu.setDailyMenuDate(LocalDate.parse("2017-05-10"));
        mockFood1.setArchived(false);
        mockFood1.setDescription("food1");
        mockFood1.setName("yum");
        mockFood1.setPrice(new BigDecimal(10));
        mockFood1.setType(foodTypeConverter.convertToEntityAttribute("Salad"));
        mockFood2.setArchived(false);
        mockFood2.setDescription("food2");
        mockFood2.setName("yum2");
        mockFood2.setPrice(new BigDecimal(100));
        mockFood2.setType(foodTypeConverter.convertToEntityAttribute("Salad"));
        mockMenu.addFood(mockFood1);
        mockMenu.addFood(mockFood2);
        mockMenu2.setDailyMenuDate(LocalDate.parse("2017-05-11"));
        mockMenu2.addFood(mockFood2);
        mockMenuList.add(mockMenu);
        mockMenuList.add(mockMenu2);
    }

    /**
     * Test of menusMonthlyGet method, of class MenusApiController.
     */
    @Test
    @WithMockAuth(id="1")
    public void testMenusMonthlyMonthOfYearGet() throws Exception {

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockUserRepository.findById(1)).willReturn(new User());

        mockMvc.perform(get("/api/menus/monthly/{monthOfYear}", "5-2017"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockAuth(id="1")
    public void testMenusMonthlyMonthOfYearGet2() throws Exception {

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockUserRepository.findById(1)).willReturn(new User());

        mockMvc.perform(get("/api/menus/monthly/{monthOfYear}", "4k2017"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test of menusMonthlyMonthOfYearGet method, of class MenusApiController.
     */
    @Test
    @WithMockAuth(id="1")
    public void testMenusMonthlyGet() throws Exception {

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockUserRepository.findById(1)).willReturn(new User());

        mockMvc.perform(get("/api/menus/monthly"))
                .andExpect(jsonPath("$", hasSize(31)))
                .andExpect(status().isOk());

    }

    /**
     * Test of menusWeeklyGet method, of class MenusApiController.
     */
    @Test
    @WithMockAuth(id="1")
    public void testMenusWeeklyGet() throws Exception {

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockUserRepository.findById(1)).willReturn(new User());

        mockMvc.perform(get("/api/menus/weekly"))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockAuth(id="1")
    public void menusWeeklyWeekOfYearGet() throws Exception {

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockUserRepository.findById(1)).willReturn(new User());

        mockMvc.perform(get("/api/menus/weekly/{weekOfYear}", "19-2017"))
                .andExpect(jsonPath("$", hasSize(5)))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockAuth(id="1")
    public void menusWeeklyWeekOfYearGet2() throws Exception {

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockUserRepository.findById(1)).willReturn(new User());

        mockMvc.perform(get("/api/menus/weekly/{weekOfYear}", "67-2017"))
                .andExpect(status().isBadRequest());
    }
    
    @Test
    @WithMockAuth(id="1")
    public void menusWeeklyWeekOfYearGet3() throws Exception {

        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockUserRepository.findById(1)).willReturn(new User());

        mockMvc.perform(get("/api/menus/weekly/{weekOfYear}", "sd-2017"))
                .andExpect(status().isBadRequest());
    }

    
}
