/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.yum.api.model.FoodEdit;
import org.bootcamp.yum.api.model.FoodItem;

import org.bootcamp.yum.api.service.FoodsService;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.FoodType;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.exceptions.ExceptionControllerAdvice;

import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import static org.mockito.BDDMockito.given;
import static org.hamcrest.Matchers.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;

@RunWith(MockitoJUnitRunner.class)
public class FoodsApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final FoodsService foodsService = new FoodsService();

    @Mock
    private FoodRepository mockFoodRepository;

    private MockMvc mockMvc;
    
    private static List<Food>  mockFoodList;
    private static Food mockFood, mockFood1;
    private static Page<Food> mockFoodPage;
    

    @BeforeClass
    public static void setUpClass() throws Exception { 
        
        mockFoodList = new ArrayList<>();
        mockFood = new Food(3);
        mockFood.setArchived(false);
        mockFood.setDescription("test Mpoureki");
        mockFood.setName("Mpoureki");
        mockFood.setType(FoodType.MAIN);
        mockFood.setPrice(new BigDecimal("3.15"));
        mockFood.setVersion(4);
        mockFoodList.add(mockFood);
        
        mockFood1 = new Food(1);

        mockFood1.setArchived(false);
        mockFood1.setDescription("test Tzatziki");
        mockFood1.setName("Tzatziki");
        mockFood1.setType(FoodType.SALAD);
        mockFood1.setPrice(new BigDecimal("2.05"));
        mockFood1.setVersion(2);
        mockFoodList.add(mockFood1);
        
        
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new FoodsApiController(foodsService))
        .setControllerAdvice(new ExceptionControllerAdvice())
        .build();
    }

    @After
    public void tearDown() throws Exception {
    }


    /**
     * Test of foodsFoodIdGet method, of class FoodsApiController.
     */
    @Test
    public void testFoodsFoodIdGet() throws Exception {
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();

        FoodEdit askedFoodFull = new FoodEdit();

        FoodItem askedFood = new FoodItem();

        askedFood.setDescription(mockFood1.getDescription());
        askedFood.setFoodName(mockFood1.getName());
        askedFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(mockFood1.getType()));
        askedFood.setPrice(mockFood1.getPrice().doubleValue());
        askedFood.setVersion(mockFood1.getVersion());

        askedFoodFull.setEditable(Boolean.TRUE);
        askedFoodFull.setFood(askedFood);
        
        
        given(mockFoodRepository.findById(1)).willReturn(mockFood1);

        mockMvc.perform(get("/api/foods/{foodId}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.food.foodName", is("Tzatziki")))
                .andExpect(jsonPath("$.food.price", is(2.05)));

    }
    
   // @Ignore
    @Test
    public void testFoodsFoodIdGet2() throws Exception {
        
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();

        FoodEdit askedFoodFull = new FoodEdit();

        FoodItem askedFood = new FoodItem();

        askedFood.setDescription(mockFood.getDescription());
        askedFood.setFoodName(mockFood.getName());
        askedFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(mockFood.getType()));
        askedFood.setPrice(mockFood.getPrice().doubleValue());
        askedFood.setVersion(mockFood.getVersion());

        askedFoodFull.setEditable(Boolean.TRUE);
        askedFoodFull.setFood(askedFood);
        
        
        given(mockFoodRepository.findById(3)).willReturn(mockFood);

        mockMvc.perform(get("/api/foods/{foodId}", 3))
                .andExpect(status().isOk());

    }

}
