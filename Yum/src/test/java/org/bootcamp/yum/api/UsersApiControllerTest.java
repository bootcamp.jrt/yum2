/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.List;
import org.bootcamp.yum.api.model.Approval;
import org.bootcamp.yum.api.model.UserExtended;
import org.bootcamp.yum.api.model.UserGetId;
import org.bootcamp.yum.api.model.UserIdPutByAdmin;
import org.bootcamp.yum.api.model.UserPut;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;

import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.InjectMocks;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.hamcrest.Matchers.*;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.http.MediaType;

import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.yum.api.service.UserService;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserType;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.exceptions.ExceptionControllerAdvice;
import org.joda.time.LocalDate;
import org.junit.Ignore;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.Mockito.when;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class UsersApiControllerTest {

    public UsersApiControllerTest() {
    }

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final UserService userService = new UserService();

    @Mock
    private UserRepository mockUserRepository;

    private MockMvc mockMvc;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new UsersApiController(userService)).setControllerAdvice(new ExceptionControllerAdvice()).build();

        //when(userService.usersIdGet(int id, any(type)).thenReturn(Boolean.TRUE));
    }

    private static List<User> mockUserList;
    private static List<UserExtended> mockUserListExtended;
    private static User mockUser1, mockUser2;
    private static UserExtended mockUser1Ext, mockUser2Ext;
    private static Page<User> userPage;

    @BeforeClass
    public static void setUpClass() {

        //User List MockUP and mockUser1 and mockUser2
        mockUserList = new ArrayList<>();
        mockUserListExtended = new ArrayList<>();
        UserTypeConverter userTypeConverter = new UserTypeConverter();
        LocalDate curr = new LocalDate();
        userPage = null;
        mockUser1 = new User(1);
        mockUser2 = new User(2);
        mockUser1.setPassword("12345678");
        mockUser1.setFirstName("Zisis");
        mockUser1.setLastName("Kikas");
        mockUser1.setEmail("email@email.com");
        mockUser1.setRole(userTypeConverter.convertToEntityAttribute("admin"));
        mockUser1.setRegistrationDate(curr);
        mockUser1.setApproved(true);

        mockUser2.setPassword("12345678");
        mockUser2.setFirstName("Kostas");
        mockUser2.setLastName("Kikas");
        mockUser2.setEmail("email");
        mockUser2.setRole(userTypeConverter.convertToEntityAttribute("admin"));
        mockUser2.setRegistrationDate(curr);
        mockUser2.setApproved(true);

        mockUserList.add(mockUser1);
        mockUserList.add(mockUser2);

        mockUser1Ext = mockUser1.userToUserExtended();
        mockUser2Ext = mockUser2.userToUserExtended();

        mockUserListExtended.add(mockUser1Ext);
        mockUserListExtended.add(mockUser2Ext);
        userPage = new PageImpl<User>(mockUserList, new PageRequest(0, 2), mockUserList.size());
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }


    /**
     * Test of usersIdForgotpwdPost method, of class UsersApiController.
     */
    @Ignore
    @Test
    public void testUsersIdForgotpwdPost() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        mockMvc.perform(
                post("/api/users/{id}/forgotpwd", mockUser1.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                        )
                .andExpect(status().isNoContent());
    }


    /**
     * Test of usersIdGet method, of class UsersApiController.
     */
    @Test
    public void testUsersIdGet() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        // when(userService.usersIdGet(1)).thenReturn(mockUser);
        mockMvc.perform(get("/api/users/{id}", 1))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.firstName", is("Zisis")));
        verify(mockUserRepository, times(1)).findById(1);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockUserRepository);
        // we verify that we didnt modify the first food item in the repository
        assertEquals(mockUser1, mockUserRepository.findById(1));
    }

    @Test
    public void testUsersIdGetUserNotFound() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        // when(userService.usersIdGet(1)).thenReturn(mockUser);
        mockMvc.perform(get("/api/users/{id}", 2))
                .andExpect(status().isNotFound());
    }

    /**
     * Test of usersIdPut method, of class UsersApiController.
     */
    @Test
    public void testUsersIdPut() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        mockMvc.perform(
                put("/api/users/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n"
                                + "  \"password\": \"88888888888\",\n"
                                + "  \"version\": 0,\n"
                                + "  \"approved\": true,\n"
                                + "  \"user\": {\n"
                                + "    \"firstName\": \"Kostas\",\n"
                                + "    \"lastName\": \"Kikas\",\n"
                                + "    \"email\": \"emaaaail@gmail.com\",\n"
                                + "    \"role\": \"admin\",\n"
                                + "    \"password\": \"88888888888\",\n"
                                + "    \"version\": 0\n"
                                + "  }\n"
                                + "}"))
                .andExpect(status().isNoContent());
        assertEquals(mockUser1.getFirstName(), mockUser2.getFirstName());

    }

    /**
     * Test of usersPost method, of class UsersApiController.
     */
    @Test
    public void testUsersPost() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        mockMvc.perform(
                post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"firstName\": \"string\",\"lastName\": \"string\",\"email\": \"str6464ing@email.com\",\"role\": \"admin\",\"password\": \"str64ing\",\"version\": 0}"))
                .andExpect(status().isNoContent());
        //assertEquals(mockUser1.getFirstName(), mockUserRepository.findById(1).getFirstName());
    }

    @Test
    public void testUsersPostBadMail() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        mockMvc.perform(
                post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"firstName\": \"string\",\"lastName\": \"string\",\"email\": \"str6464mail.com\",\"role\": \"admin\",\"password\": \"str64ing\",\"version\": 0}"))
                .andExpect(status().isBadRequest());

        //assertEquals(mockUser1.getFirstName(), mockUserRepository.findById(1).getFirstName());
    }

    @Test
    public void testUsersPostEmptyFields() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        mockMvc.perform(
                post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"firstName\": \"\",\"lastName\": \"\",\"email\": \"str6464@mail.com\",\"role\": \"admin\",\"password\": \"str64ing\",\"version\": 0}"))
                .andExpect(status().isBadRequest());

        //assertEquals(mockUser1.getFirstName(), mockUserRepository.findById(1).getFirstName());
    }

    
    
    
    @Test
    public void testUsersIdApprovePutSuperAdminFail() throws Exception {

        given(mockUserRepository.findById(1)).willReturn(mockUser1);
        mockMvc.perform(
                put("/api/users/{id}/approve", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n"
                                + "  \"appr\": true\n"
                                + "}"))
                .andExpect(status().isBadRequest());

    }

}
