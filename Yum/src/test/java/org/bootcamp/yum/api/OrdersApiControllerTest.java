/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import org.bootcamp.yum.exceptions.ExceptionControllerAdvice;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.hamcrest.Matchers.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.http.MediaType;

import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.yum.api.service.OrdersService;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.GlobalSettings;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.OrderItemKey;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.GlobalSettingsRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.junit.Rule;

import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

@RunWith(MockitoJUnitRunner.class)
public class OrdersApiControllerTest 
{
    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final OrdersService ordersService = new OrdersService();
    
    @Mock
    private DailyOrderRepository mockDailyOrderrepo;
    @Mock
    private DailyMenuRepository mockDailyMenurepo;
    @Mock
    private UserRepository mockUserrepo;
    @Mock
    private FoodRepository mockFoodrepo;
    @Mock
    private GlobalSettingsRepository mockGlobalSettingsrepo;
    @Mock
    private OrderItemRepository mockOrderItemrepo;

    private MockMvc mockMvc;
    
    public OrdersApiControllerTest() {
    }
    
    private static Food mockFood1;
    private static Food mockFood2;
    private static OrderItem mockOrderItem1;
    private static OrderItem mockOrderItem2;
    private static List<Food> mockFoods;
    private static List<org.bootcamp.yum.data.entity.DailyOrder> mockDailyOrders;
    private static org.bootcamp.yum.data.entity.DailyOrder mockDailyOrder;
    private static DailyMenu mockDailyMenu;
    private static GlobalSettings mockGlobalSettings;
    
    @BeforeClass
    public static void setUpClass() 
    {
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
             
        // Mock food
        mockFood1 = new Food(10);
        mockFood1.setDescription("Mock arni sthn souvla.");
        mockFood1.setName("Mock Arni");
        mockFood1.setPrice(new BigDecimal(55.00));
        mockFood1.setVersion(0);
        mockFood1.setType(foodTypeConverter.convertToEntityAttribute("Salad"));
        
        mockFood2 = new Food(11);
        mockFood2.setDescription("Mock Mprokolo. Einai ahdia.");
        mockFood2.setName("Mock Mprokolo");
        mockFood2.setPrice(new BigDecimal(2.00));
        mockFood2.setVersion(0);
        mockFood2.setType(foodTypeConverter.convertToEntityAttribute("Salad"));
        
        // Mock Order
        mockDailyOrder = new org.bootcamp.yum.data.entity.DailyOrder(10);
        
        
        // Mock OrderItem
        OrderItemKey key1 = new OrderItemKey();
        key1.setDailyOrderId(10);
        key1.setFoodId(10);
        mockOrderItem1 = new OrderItem(key1);
        mockOrderItem1.setFood(mockFood1);
        mockOrderItem1.setQuantity(5);
        OrderItemKey key2 = new OrderItemKey();
        key2.setDailyOrderId(10);
        key2.setFoodId(11);
        mockOrderItem2 = new OrderItem(key2);
        mockOrderItem2.setFood(mockFood2);
        mockOrderItem2.setQuantity(15);
        
        mockDailyOrder.addOrderItem(mockOrderItem1);
        mockDailyOrder.addOrderItem(mockOrderItem2);
        
        // Mock list of foods to put inside each DailyMenu
        mockFoods = new ArrayList<>();
        
        

        // Mock List of Orders
        mockDailyOrders = new ArrayList<org.bootcamp.yum.data.entity.DailyOrder>();

        // Mock DailyMenu  
        mockDailyMenu = new DailyMenu(10);
        mockDailyMenu.setVersion(0);
        mockDailyMenu.setFoods(mockFoods);
        mockDailyMenu.setDailyMenuDate(LocalDate.now().plusDays(5));

        mockFood1.addOrderItem(mockOrderItem1);
        mockFood2.addOrderItem(mockOrderItem2);
        mockFoods.add(mockFood1);
        mockFoods.add(mockFood2);
        mockDailyOrder.setDailyMenu(mockDailyMenu);
        mockDailyOrder.setVersion(0);
        // Add orders 1 and 2 to the list.
        mockDailyOrders.add(mockDailyOrder);
        
        // Mock GlobalSettings
        mockGlobalSettings = new GlobalSettings();
        mockGlobalSettings.setTimeLastOrder(LocalTime.MIDNIGHT);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup( new OrdersApiController(ordersService) )
            .setControllerAdvice(new ExceptionControllerAdvice())
            .build();
    }
    
    @After
    public void tearDown() {
    }
    
    /****** Test of ordersMonthlyGet method ******/
   // @Ignore
    @Test
    // The WothMockAuth annotation is needed only if in the controller, you need to access the User that has the token
    @WithMockAuth(id="1")
    public void testOrdersMonthlyGetNormal() throws Exception 
    {
        // telling Mockito to use this mock list every time the findAll method is called on the dailyOrderrepo
        given(mockDailyOrderrepo.findAll()).willReturn(mockDailyOrders);
        
        // telling mockito there is one user in the database, with id 1
        // this step is specially needed if you use the User of the token in your controller.
        // in this case, use the annotation @WithMockAuth(id="1")
        given(mockUserrepo.findById(1)).willReturn(new User());
        
        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/orders/monthly")).andExpect(status().isOk()) // The HTTP status code is 200
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)) // The content-type of the response is json 
            .andExpect(jsonPath("$", hasSize(31))); 
            //.andExpect(jsonPath("$[5].foods[1].quantity", is(15))); 
           // .andExpect(jsonPath("$[0].foods[0].food.foodName", is("Mock Arni")));
        
    }
    
    
    /****** Test of ordersMonthlyMonthGet method ******/

    @Test
    @WithMockAuth(id="1")
    public void ordersMonthlyMonthGetNormal() throws Exception 
    {
        given(mockDailyOrderrepo.findAll()).willReturn(mockDailyOrders);
        
        given(mockUserrepo.findById(1)).willReturn(new User());
        
        mockMvc.perform(get("/api//orders/monthly/{month}", LocalDate.now().getMonthOfYear()+"-"+LocalDate.now().getYear()))
            .andExpect(status().isOk()) // The HTTP status code is 200
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)) // The content-type of the response is json 
            .andExpect(jsonPath("$", hasSize(31))); 
        
//        verify(mockDailyOrderrepo, times(1)).findAll();
//        verifyNoMoreInteractions(mockDailyOrderrepo);
    }

    @Test
    public void ordersMonthlyMonthGetBadPattern() throws Exception 
    {
        mockMvc.perform(get("/api//orders/monthly/{month}", "baddd input"))
            .andExpect(status().isBadRequest()); // The HTTP status code is 400
        verify(mockDailyOrderrepo, times(0)).findAll();
    }

    @Test
    public void ordersMonthlyMonthGetBadMonth() throws Exception 
    {
        mockMvc.perform(get("/api//orders/monthly/{month}", "19-2017"))
            .andExpect(status().isBadRequest()); // The HTTP status code is 400
        verify(mockDailyOrderrepo, times(0)).findAll();
    }

    @Test
    public void ordersMonthlyMonthGetBadYear() throws Exception 
    {
        mockMvc.perform(get("/api//orders/monthly/{month}", "11-1017"))
            .andExpect(status().isBadRequest()); // The HTTP status code is 400
        verify(mockDailyOrderrepo, times(0)).findAll();
    }
    
    
    /****** Test of ordersPost method ******/

    @Test
    @WithMockAuth(id="1")
    public void ordersPostNoDailyMenu() throws Exception 
    {
        given(mockUserrepo.findOne(1)).willReturn( new User() );
        given(mockDailyMenurepo.findByDailyMenuDate(new LocalDate("2022-05-13"))).willReturn(null);
        //given(mockDailyOrderrepo.findByUserIdAndDailyMenuId(1,10)).willReturn(new ArrayList<org.bootcamp.yum.data.entity.DailyOrder>() );
        //given(mockGlobalSettingsrepo.findOne(1)).willReturn(mockGlobalSettings);

        mockMvc.perform(post("/api/orders/" )
            .contentType(MediaType.APPLICATION_JSON)
            .content( "{" +
                "  \"day\": \"2022-05-13\"," +
                "  \"email\": true," +
                "  \"id\": 0," +
                "  \"dmId\": 0," +
                "  \"version\": 0," +
                "  \"foods\": [" +
                "    {" +
                "      \"foodId\": 10," +
                "      \"quantity\": 5" +
                "    }" +
                "  ]" +
                "}"))
            .andExpect(status().isNotFound()); 
    }
    
    
   
}

