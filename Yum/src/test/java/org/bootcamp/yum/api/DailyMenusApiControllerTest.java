/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.bootcamp.test.MockAuthRule;

import org.junit.Before;
import org.junit.Test;
import org.junit.Rule;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import static org.mockito.BDDMockito.given;
import static org.hamcrest.Matchers.*;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import org.springframework.http.MediaType;

import org.bootcamp.yum.data.entity.Food;

import org.bootcamp.yum.api.service.DailyMenuService;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.exceptions.ExceptionControllerAdvice;
import org.joda.time.LocalDate;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@RunWith(MockitoJUnitRunner.class)
public class DailyMenusApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final DailyMenuService dailyMenusService = new DailyMenuService();

    @Mock
    private DailyMenuRepository mockDailyMenuRepository;

    @Mock
    private FoodRepository mockFoodRepository;

    private MockMvc mockMvc;
    private static List<DailyMenu> mockMenuList;
    private static List<Food> mockFoodList;
    private static DailyMenu mockMenu;
    private static DailyMenu mockMenu2;
    private static DailyMenu newMenu;
    private static Food mockFood1;
    private static Food mockFood2;
    private static Food mockFood3;
    private static Food newFood;
    private static String newDailyMenu;

    public DailyMenusApiControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
        mockMenuList = new ArrayList<>();
        mockFoodList = new ArrayList<>();
        mockMenu = new DailyMenu(5);
        mockMenu2 = new DailyMenu(6);
        newMenu = new DailyMenu(7);
        newMenu.setDailyMenuDate(LocalDate.parse("2017-05-14"));
        newFood = new Food(4);

        mockFood1 = new Food(1);
        mockFood2 = new Food(2);
        mockFood3 = new Food(3);
        mockMenu.setDailyMenuDate(LocalDate.parse("2017-05-10"));
        mockFood1.setArchived(false);
        mockFood1.setDescription("food1");
        mockFood1.setName("yum");
        mockFood1.setPrice(new BigDecimal(10));
        mockFood1.setType(foodTypeConverter.convertToEntityAttribute("Salad"));
        mockFood2.setArchived(false);
        mockFood2.setDescription("food2");
        mockFood2.setName("yum2");
        mockFood2.setPrice(new BigDecimal(100));
        mockFood2.setType(foodTypeConverter.convertToEntityAttribute("Salad"));
        mockFood3.setArchived(false);
        mockFood3.setDescription("food3");
        mockFood3.setName("yu3");
        mockFood3.setPrice(new BigDecimal(13));
        mockFood3.setType(foodTypeConverter.convertToEntityAttribute("Salad"));
        mockMenu.addFood(mockFood1);
        mockMenu.addFood(mockFood2);
        mockMenu2.setDailyMenuDate(LocalDate.parse("2017-05-11"));
        mockMenu2.addFood(mockFood2);

        mockMenuList.add(mockMenu);
        newMenu.addFood(mockFood1);
        mockMenuList.add(mockMenu2);
        mockFoodList.add(mockFood1);
        mockFoodList.add(mockFood2);
        mockFoodList.add(mockFood3);
        mockMenuList.add(newMenu);
        newDailyMenu = new String("{\n"
                + "\"dailyMenuDate\": \"2017-05-30\",\n"
                + "\"dailyMenuId\": 0,\n"
                + "\"final\": false,\n"
                + "\"version\": 0,\n"
                + "\"foods\": [\n"
                + "{\n"
                + "\"foodId\": 1,\n"
                + "\"foodName\": \"Kounoupidi\",\n"
                + "\"foodType\": \"Salad\",\n"
                + "\"description\": \"string\",\n"
                + "\"isOrdered\": true\n"
                + "}\n"
                + "]\n"
                + "}");
    }

    @Before
    public void setup() {
        //mockMvc = MockMvcBuilders.standaloneSetup(new DailyMenusApiController(dailyMenusService)).build();
        mockMvc = MockMvcBuilders.standaloneSetup(new DailyMenusApiController(dailyMenusService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
    }

    @Test
    public void testDailyMenusMonthlyGet() throws Exception {
        given(mockDailyMenuRepository.findAll()).willReturn(mockMenuList);
        given(mockFoodRepository.findById(1)).willReturn(mockFood1);
        given(mockFoodRepository.findById(2)).willReturn(mockFood2);

        mockMvc.perform(get("/api/dailyMenus/monthly"))
                .andExpect(jsonPath("$", hasSize(31)))
                .andExpect(status().isOk());
    }

    @Test
    public void dailyMenusPost() throws Exception {
        given(mockFoodRepository.findById(1)).willReturn(mockFood1);
        given(mockDailyMenuRepository.findByDailyMenuDate(LocalDate.parse("2017-05-14"))).willReturn(mockMenu);

        mockMvc.perform(post("/api/dailyMenus")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n"
                        + "  \"dailyMenuDate\": \"2017-05-14\",\n"
                        + "  \"dailyMenuId\": 0,\n"
                        + "  \"final\": true,\n"
                        + "  \"version\": 0,\n"
                        + "  \"foods\": [\n"
                        + "    {\n"
                        + "      \"foodId\": 1,\n"
                        + "      \"foodName\": \"string\",\n"
                        + "      \"foodType\": \"string\",\n"
                        + "      \"description\": \"string\",\n"
                        + "      \"isOrdered\": true\n"
                        + "    }\n"
                        + "  ]\n"
                        + "}")
        )
                .andDo(print())
                .andExpect(status().is4xxClientError());
    }

    /* @Test
    public void testDailyMenusMonthlyMonthGet() {
       
    }

   
    @Test
    public void testDailyMenusPost() {
      
    }*/
}
