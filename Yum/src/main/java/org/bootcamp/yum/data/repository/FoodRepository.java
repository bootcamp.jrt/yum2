/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.data.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.FoodType;

@Repository        
public interface FoodRepository extends PagingAndSortingRepository<Food, Integer>
{

    Food findById(int id);
    Food findByName(String name);
    List<Food> findByNameAndArchived(String name, boolean archived);
    Food findByIdAndArchived(Integer foodId, boolean archived);
    Page <Food> findByTypeAndArchived(FoodType type, boolean archived, Pageable pageable);
    Page <Food> findAll(Pageable pageable);
    Page <Food> findByArchived(boolean archived, Pageable pageable);
}
