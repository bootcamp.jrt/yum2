/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.data.converter;

import javax.persistence.AttributeConverter;
import org.bootcamp.yum.data.entity.FoodType;


public class FoodTypeConverter implements AttributeConverter<FoodType, String>{

    @Override
    public String convertToDatabaseColumn(FoodType x) {
        switch (x) {
            case MAIN:
                return "Main dish";
            case SALAD:
                return "Salad";
            case DRINK:
                return "Drink";
            default:
                // do we need this? if for nothing else it catches forgotten case when enum is modified
                throw new IllegalArgumentException("Invalid value " + x);
            // actually the value is valid, just this externalized switch sucks of course
        }
    }

    @Override
    public FoodType convertToEntityAttribute(String y) {
        switch (y) {
            case "Main dish":
                return FoodType.MAIN;
            case "Salad":
                return FoodType.SALAD;
            case "Drink":
                return FoodType.DRINK;
            default:
                return null;
        }
    }

}
