/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 *
 * @author user
 */
@Embeddable
public class OrderItemKey implements Serializable{
    @Column(name = "daily_order_id", nullable = false)
     private int dailyOrderId;
    @Column(name = "food_id", nullable = false)
     private int foodId;

    public int getDailyOrderId() {
        return dailyOrderId;
    }

    public void setDailyOrderId(int id) {
        this.dailyOrderId = id;
    }

    public int getFoodId() {
        return foodId;
    }

    public void setFoodId(int foodId) {
        this.foodId = foodId;
    }
    public String toString(){
        return "Daily order id: "+dailyOrderId+" Food id: "+foodId+"\n";
    }
}
