/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import static javax.persistence.FetchType.LAZY;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import java.sql.Timestamp;

import org.bootcamp.yum.api.model.UserExtended;
import org.bootcamp.yum.api.model.UserGetId;
import org.bootcamp.yum.data.converter.DateConverter;
import org.bootcamp.yum.data.converter.PasswordProtection;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.joda.time.LocalDate;

@Entity
@Table(name = "user")
public class User {

    /*public enum UserRole{
        Hungry, Chef, Admin
    }*/
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX
            = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])", Pattern.CASE_INSENSITIVE);

    // This constructor is needed for mock data, when the database is not here to autogenerate the id
    public User(int id) {
        this.id = id;
    }

// this default constructor is needed once you add the previous one. 
    public User() {
    }

    @Id
    @Column(name = "user_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "email")
    private String email;

    @Version
    @Column(name = "version")
    private int version;

    @Convert(converter = UserTypeConverter.class)
    @Column(name = "role")
    private UserType role;

    @Column(name = "pass")
    private String password;

    @Column(name = "registration_date")
    @Convert(converter = DateConverter.class)
    private LocalDate registrationDate;

    @Column(name = "approved")
    private boolean approved;

    @Basic(fetch=LAZY)
    @Lob @Column(name="picture")
    private byte[] picture;    

    @Column(name = "token")
    private String token;

    @Column(name = "expiration")
    private Timestamp expiration;
    
    @OneToMany(mappedBy = "user")
    private List<DailyOrder> dailyOrders;

    public List<DailyOrder> getDailyOrders() {
        return dailyOrders;
    }

    public void addDailyOrders(DailyOrder dailyOrder) {
        this.dailyOrders.add(dailyOrder);
    }

    public void setDailyOrders(ArrayList<DailyOrder> dailyOrders) {
        this.dailyOrders = dailyOrders;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserType getRole() {
        return role;
    }

    public void setRole(UserType role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Timestamp getExpiration() {
        return expiration;
    }

    public void setExpiration(Timestamp expiration) {
        this.expiration = expiration;
    }
    
    public String toString() {
        return id + ": " + lastName + " " + firstName + " " + email + " " + role + "Daily Order:\n";
    }

    public static boolean validate(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(emailStr);
        return matcher.find();
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 61 * hash + this.id;
        return hash;
    }

// The equals method is useful for the assertEquals
    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }

    public boolean checkIfPassIsEncrypted() throws Exception {
        PasswordProtection pp = new PasswordProtection();
        if (this == null) {
            return false;
        } else if (pp.decrypt(this.getPassword()) == null) {
            return false;
        } else {
            return true;
        }
    }

    public UserExtended userToUserExtended() {
        UserTypeConverter utc = new UserTypeConverter();
        UserExtended userExtended = new UserExtended();
        userExtended.setId(id);
        userExtended.setRegistrationDate(registrationDate);
        UserGetId userGetId = new UserGetId();
        userGetId.setApproved(approved);
        userGetId.setVersion(version);
        userGetId.setRole(utc.convertToDatabaseColumn(role));
        userGetId.setPicture(picture);
        userGetId.setLastName(lastName);
        userGetId.setFirstName(firstName);
        userGetId.setEmail(email);
        userExtended.setUser(userGetId);

        return userExtended;
    }

    public boolean isSuperAdmin() {
        if (id == 1) {
            return true;
        } else {
            return false;
        }
    }

    public boolean hasPastOrders() {

        //true is in the case : ONLY past orders 
        boolean pastOrders = true;
        if (!dailyOrders.isEmpty()) {

            for (DailyOrder dOrder : dailyOrders) {
                if (dOrder.getDailyMenu().getDailyMenuDate().isAfter(LocalDate.now())) {
                    pastOrders = false;
                }
            }
            return pastOrders;
        } else {
            return false;
        }

    }

    public boolean hasFutureOrders() {

        //true if daily orders contain dates after the tomorrow date. 
        boolean futureOrders = true;
        if (!dailyOrders.isEmpty()) {

            for (DailyOrder dOrder : dailyOrders) {
                if (dOrder.getDailyMenu().getDailyMenuDate().isBefore(LocalDate.now().plusDays(1))) {
                    futureOrders = false;
                }

            }
            return futureOrders;
        } else {
            return false;
        }
    }

    public void deleteDailyOrders() {

        //if user has ONLY future orders and NO past orders, delete them
        if (hasFutureOrders() && !hasPastOrders()) {
            dailyOrders.removeAll(dailyOrders);

        }

    }
}
