/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.*;

import org.bootcamp.yum.data.converter.TimeConverter;

@Entity
@Table(name = "global_settings")
public class GlobalSettings {

    @Id
    @Column(name = "global_settings_id")
    private int globalSettingsId;

    @Column(name = "currency_type")
    private String currency;

    @Column(name = "notes")
    private String notes;

    @Column(name = "terms_service")
    private String termsService;

    @Column(name = "privacy_policy")
    private String privacyPolicy;

    @Column(name = "time_last_order")
    @Convert(converter = TimeConverter.class)
    private LocalTime timeLastOrder;

    @Column(name = "version_of_foodlist")
    private int versionOfFoodlist;

    @Column(name = "version")
    private int version;

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getTermsService() {
        return termsService;
    }

    public void setTermsService(String termsService) {
        this.termsService = termsService;
    }

    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public LocalTime getTimeLastOrder() {
        return timeLastOrder;
    }

    public void setTimeLastOrder(LocalTime timeLastOrder) {
        this.timeLastOrder = timeLastOrder;
    }

    public int getVersionOfFoodlist() {
        return versionOfFoodlist;
    }

    public void setVersionOfFoodlist(int versionOfFoodlist) {
        this.versionOfFoodlist = versionOfFoodlist;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setGlobalSettingsId(int globalSettingsId) {
        this.globalSettingsId = globalSettingsId;
    }

}
