/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.data.entity;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import org.joda.time.LocalDate;

import org.bootcamp.yum.api.model.DailyMenuFoods;
import org.bootcamp.yum.api.model.DailyMenuUpdate;
import org.bootcamp.yum.api.model.DailyMenuUpdateFoods;
import org.bootcamp.yum.data.converter.DateConverter;

@Entity
@Table(name = "daily_menu")
public class DailyMenu {

    @Id
    @Column(name = "daily_menu_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int dailyMenuId;

    @Column(name = "daily_menu_date")
    @Convert(converter = DateConverter.class)
    private LocalDate dailyMenuDate;

    @Version
    @Column(name = "version")
    private int version;

    @OneToMany(mappedBy = "dailyMenu")
    private List<DailyOrder> dailyOrders;

    @ManyToMany
    @JoinTable(name = "daily_menu_food", joinColumns = @JoinColumn(name = "daily_menu_id"), inverseJoinColumns = @JoinColumn(name = "food_id"))
    private List<Food> foods;
    
    // This constructor is needed for mock data, when the database is not here to autogenerate the id
    public DailyMenu(int id) 
    {
        this.dailyMenuId = id;
    }

    // this default constructor is needed once you add the previous one. 
    public DailyMenu() {}


    public LocalDate getDailyMenuDate() {
        return dailyMenuDate;
    }

    public void setDailyMenuDate(LocalDate dailyMenuDate) {
        this.dailyMenuDate = dailyMenuDate;
    }

    public int getDailyMenuId() {
        return dailyMenuId;
    }

    public List<Food> getFoods() {
        return this.foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }

    public void addFood(Food food) {
        if(this.foods == null){
            this.foods = new ArrayList<>();
        }
        else{
            this.foods.add(food);
        }
    }

    public List<DailyOrder> getDailyOrders() {
        return this.dailyOrders;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }
    
    public DailyMenuUpdate dailyMenu2DMUpdate()
    {
        DailyMenuUpdate dailyMenuDTO = new org.bootcamp.yum.api.model.DailyMenuUpdate();
        
        dailyMenuDTO.setDailyMenuDate(dailyMenuDate);
        dailyMenuDTO.setVersion(version);
        List<DailyMenuUpdateFoods> dmuFoods = new ArrayList<>();
        for (Food aFood : foods)
        {
            dmuFoods.add(aFood.foodToDMUFood());
        }
        dailyMenuDTO.setFoods(dmuFoods);
        
        return dailyMenuDTO; 
    }
    
    public org.bootcamp.yum.api.model.DailyMenu dailyMenuToDTO()
    {
        org.bootcamp.yum.api.model.DailyMenu dailyMenuDTO = new org.bootcamp.yum.api.model.DailyMenu();
        
        dailyMenuDTO.setDailyMenuDate(dailyMenuDate);
        dailyMenuDTO.setVersion(version);
        dailyMenuDTO.setDailyMenuId(dailyMenuId); 
        
        List<DailyMenuFoods> dailymenuFoods = new ArrayList<>();
        for (Food aFood : foods)
        {
            dailymenuFoods.add(aFood.foodToDailyMenuFood());
        }
        dailyMenuDTO.setFoods(dailymenuFoods);
      
        return dailyMenuDTO; 
    }

    @Override
    public String toString() {
        return "DailyMenu{" + "dailyMenuId=" + dailyMenuId + ", dailyMenuDate=" + dailyMenuDate + "Daily Orders : " + dailyOrders + "}";
    }

}
