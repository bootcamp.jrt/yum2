/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.data.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.bootcamp.yum.api.model.DailyOrderFoods;


@Entity
@Table(name="order_item")
public class OrderItem implements Serializable{
    
    @EmbeddedId
    private OrderItemKey memberKey;


    public OrderItemKey getMemberKey() {
        return memberKey;
    }

    public DailyOrder getDailyOrder() {
        return dailyOrder;
    }

    @Column(name="quantity")
    private int quantity;
    
    @ManyToOne
    @JoinColumn(name = "daily_order_id",insertable=false,updatable=false)
    private DailyOrder dailyOrder;
    
    @ManyToOne
    @JoinColumn(name = "food_id",insertable=false,updatable=false)
    private Food food;
    
    // This constructor is needed for mock data, when the database is not here to autogenerate the id
    public OrderItem(OrderItemKey memberKey) 
    {
        this.memberKey = memberKey;
    }

    // this default constructor is needed once you add the previous one. 
    public OrderItem() {}


    public Food getFood() {
        return food;
    }

    public void setFood(Food food) {
        this.food = food;
    }



    public void setMemberKey(OrderItemKey memberKey) {
        this.memberKey = memberKey;
    }
    


    public void setDailyOrder(DailyOrder dailyOrder) {
        this.dailyOrder = dailyOrder;
    }



    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public String toString(){
        return memberKey.toString()+ "of quantity : "+quantity+" Food:  \n";
    }
    
    public DailyOrderFoods orderItemToDailyOrderFoods()
    {
        DailyOrderFoods dailyOrderFood = new DailyOrderFoods();
        dailyOrderFood.setFoodId(food.getId());
        dailyOrderFood.setQuantity(quantity);
        return dailyOrderFood;
    }
    
    @PrePersist
    private void prePersist() 
    {
        memberKey = new OrderItemKey();
        memberKey.setFoodId(Integer.valueOf(food.getId()));
        memberKey.setDailyOrderId(dailyOrder.getId());
    }
    
}

