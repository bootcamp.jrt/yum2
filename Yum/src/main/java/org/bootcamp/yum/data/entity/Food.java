/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.data.entity;

import org.bootcamp.yum.data.converter.FoodTypeConverter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.bootcamp.yum.api.model.DailyMenuFoods;
import org.bootcamp.yum.api.model.DailyMenuUpdateFoods;
import org.bootcamp.yum.api.model.FoodItem;


@Entity
@Table(name = "food")
public class Food {

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + this.id;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }

    @Id
    @Column(name = "food_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "food_name")
    private String name;

    @Version
    @Column(name = "version")
    private int version;

    @Convert(converter = FoodTypeConverter.class)
    @Column(name = "food_type")
    private FoodType type;

    @Column(name = "description")
    private String description;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "archived")
    private boolean archived;

    @OneToMany(mappedBy = "food")
    private List<OrderItem> orderItems;

    @ManyToMany(mappedBy = "foods")
    private List<DailyMenu> dailyMenus;

    public Food(int id) {
        this.id = id;
    }

    public Food() {
    }

    public List<DailyMenu> getDailyMenus() {
        return dailyMenus;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public List<OrderItem> getOrderItems() {
        return this.orderItems;
    }

    public void addOrderItem(OrderItem oi) {
        if (this.orderItems == null) {
            orderItems = new ArrayList<>();
        }
            this.orderItems.add(oi);
    }

    public String getName() {
        return name;
    }

    public FoodType getType() {
        return type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(FoodType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return name + " " + price + "\n";
    }

    public FoodItem foodToFoodItem() 
    {
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
        FoodItem foodItemToReturn = new FoodItem();
        foodItemToReturn.setFoodName(name);
        foodItemToReturn.setDescription(description);
        foodItemToReturn.setPrice(price.doubleValue());
        foodItemToReturn.setFoodType(foodTypeConverter.convertToDatabaseColumn(type));
        foodItemToReturn.setVersion(version); 

        return foodItemToReturn;
    }
    
    public DailyMenuUpdateFoods foodToDMUFood() 
    {
        DailyMenuUpdateFoods dmuFood = new DailyMenuUpdateFoods();
        dmuFood.setFoodId(id);

        return dmuFood;
    }
    
    public DailyMenuFoods foodToDailyMenuFood() 
    {
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
        DailyMenuFoods dailyMenuFood = new DailyMenuFoods();
        dailyMenuFood.setFoodId(id);
        dailyMenuFood.setDescription(description); 
        dailyMenuFood.setFoodName(name); 
        dailyMenuFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(type));
        dailyMenuFood.setIsOrdered(true); 

        return dailyMenuFood;
    }
}
