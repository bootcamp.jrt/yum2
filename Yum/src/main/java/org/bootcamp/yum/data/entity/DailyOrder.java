/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.data.entity;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import org.bootcamp.yum.api.model.DailyOrderFoods;

@Entity
@Table(name = "daily_order")
public class DailyOrder {
    
    @Id
    @Column(name = "daily_order_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    public int getId() {
        return id;
    }

    @Column(name = "user_id", insertable = false, updatable = false)
    private int userId;

    @Column(name = "daily_menu_id", insertable = false, updatable = false)
    private int dailyMenuId;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;


    @OneToMany(mappedBy="dailyOrder", cascade=CascadeType.ALL)
    private List<OrderItem> orderItems;

    @ManyToOne
    @JoinColumn(name = "daily_menu_id")
    private DailyMenu dailyMenu;
    
    @Version
    @Column(name = "version")
    private int version;
    
    // This constructor is needed for mock data, when the database is not here to autogenerate the id
    public DailyOrder(int id) 
    {
        this.id = id;
    }

    // this default constructor is needed once you add the previous one. 
    public DailyOrder() {}

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public DailyMenu getDailyMenu() {
        return dailyMenu;
    }

    public void setDailyMenu(DailyMenu dailyMenu) {
        this.dailyMenu = dailyMenu;
    }

    public int getDailyMenuId() {
        return dailyMenuId;
    }

    public void setDailyMenuId(int dId) {
        this.dailyMenuId = dId;
    }

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public void addOrderItem(OrderItem orderItem) {
        if (orderItems == null)
        {
            orderItems = new ArrayList<OrderItem>();
        }
        this.orderItems.add(orderItem);
    }
    
    public static boolean checkTimeOfOrder(LocalTime timeDeadline, LocalDate orderDay)
    {
        // Check if the time for orders for tomorrow has passed
        if (LocalTime.now().isAfter(timeDeadline) && orderDay.isEqual(LocalDate.now().plusDays(1)) )
        {
            return false;
        }
        // Check if the time of order is in the past
        else if (orderDay.isBefore( LocalDate.now().plusDays(1) ) )
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    
    public org.bootcamp.yum.api.model.DailyOrder dailyOrderDAO2DTO()
    {
        org.bootcamp.yum.api.model.DailyOrder dailyOrderDTO = new org.bootcamp.yum.api.model.DailyOrder();
        dailyOrderDTO.setDay(dailyMenu.getDailyMenuDate());
        List<DailyOrderFoods> dofList = new ArrayList<>();
        for (OrderItem anOrdeItem : orderItems)
        {
            DailyOrderFoods aDailyOrderFood = anOrdeItem.orderItemToDailyOrderFoods();
            dofList.add(aDailyOrderFood);
        }
        
        dailyOrderDTO.setFoods(dofList);
        dailyOrderDTO.setVersion(version);
        dailyOrderDTO.setId(id);
        return dailyOrderDTO; 
    }
    
    @Override
    public String toString ()
    {
        return id+": "+userId+" "+dailyMenuId+" User: "+user+" \n";
    }
            
}
