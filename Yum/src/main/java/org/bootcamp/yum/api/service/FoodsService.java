/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import static java.lang.Math.toIntExact;
import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.LocalDate;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.Food;
import org.bootcamp.yum.api.model.FoodEdit;
import org.bootcamp.yum.api.model.FoodItem;
import org.bootcamp.yum.api.model.Foods;
import org.bootcamp.yum.api.model.FoodsFoodList;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.GlobalSettings;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.GlobalSettingsRepository;


@Service
public class FoodsService {

    @Autowired
    private FoodRepository foodrepo;

    @Autowired
    private GlobalSettingsRepository globalSettingsrepo;

    public FoodEdit foodsFoodIdGet(int foodId, Boolean checkIfEditable) throws ApiException {

        org.bootcamp.yum.data.entity.Food retrievedFood = foodrepo.findById(foodId);

        FoodItem askedFood = new FoodItem();
        FoodEdit askedFoodFull = new FoodEdit();
        if (retrievedFood == null) {//check if food exists
            throw new ApiException(404, "Food not found");
        }//We dont return archived foods
        if (retrievedFood.isArchived() == true) {
            throw new ApiException(404, "Food not found");
        } else {
            askedFood.setFoodName(retrievedFood.getName());
            askedFood.setDescription(retrievedFood.getDescription());
            FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
            askedFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(retrievedFood.getType()));
            askedFood.setPrice(retrievedFood.getPrice().doubleValue());
            askedFoodFull.setFood(askedFood);
            askedFoodFull.getFood().setVersion(retrievedFood.getVersion());
            if (checkIfEditable == true) {
                askedFoodFull.setEditable(checkEditable(retrievedFood));
            }
            return askedFoodFull;

        }
    }

    public Foods foodsGet(Integer version, Boolean archived, Integer page, Integer pagesize, String type, String ordering) throws ApiException {

        if (version != null) {
            GlobalSettings globalSettings = globalSettingsrepo.findOne(1);
            if (globalSettings.getVersionOfFoodlist() == version) {
                throw new ApiException(304, "List up to date");
            } else {
                Iterable<org.bootcamp.yum.data.entity.Food> foodIterable = foodrepo.findAll();

                Foods listOfFoods = new Foods();
                FoodTypeConverter foodTypeConverter = new FoodTypeConverter();

                for (org.bootcamp.yum.data.entity.Food f : foodIterable) {
                    FoodsFoodList actuallyAFood = new FoodsFoodList();
                    Food food = new Food();

                    food.setFoodId(f.getId());
                    food.setDescription(f.getDescription());
                    food.setFoodName(f.getName());
                    food.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
                    food.setPrice(f.getPrice().doubleValue());

                    actuallyAFood.setFood(food);

                    listOfFoods.addFoodListItem(actuallyAFood);
                    listOfFoods.setVersion(globalSettings.getVersionOfFoodlist());
                }
                return listOfFoods;
            }
        } else {

            if (!(type.equals("Main dish") || type.equals("Salad") || type.equals("Drink") || type.equals("all"))) {
                throw new ApiException(400, "Bad request");
            } else if (!(ordering.equals("lowerprice") || ordering.equals("higherprice") || ordering.equals("mostpopular") || ordering.equals("leastpopular") || ordering.equals("none"))) {
                throw new ApiException(400, "Bad request");
            } else if (pagesize < 1) {
                throw new ApiException(400, "Bad request");
            } else if (page < 0) {
                throw new ApiException(400, "Bad request");
            } else {

                Page<org.bootcamp.yum.data.entity.Food> fip3 = null;
                FoodTypeConverter foodTypeConverter = new FoodTypeConverter();

                if (type.equals("all")) {
                    if (ordering.equals("lowerprice")) {
                        ordering = "price";
                        PageRequest pageRequest = new PageRequest(page, pagesize, Sort.Direction.ASC, ordering);
                        fip3 = foodrepo.findByArchived(archived, pageRequest);
                    } else if (ordering.equals("higherprice")) {
                        ordering = "price";
                        PageRequest pageRequest = new PageRequest(page, pagesize, Sort.Direction.DESC, ordering);
                        fip3 = foodrepo.findByArchived(archived, pageRequest);
                    } 
                    else {
                        ordering = "id";
                        PageRequest pageRequest = new PageRequest(page, pagesize, Sort.Direction.DESC, ordering);
                        fip3 = foodrepo.findByArchived(archived, pageRequest);
                    }
                } else {
                    if (ordering.equals("lowerprice")) {
                        ordering = "price";
                        PageRequest pageRequest = new PageRequest(page, pagesize, Sort.Direction.ASC, ordering);
                        fip3 = foodrepo.findByTypeAndArchived(foodTypeConverter.convertToEntityAttribute(type), archived, pageRequest);
                    } else if (ordering.equals("higherprice")) {
                        ordering = "price";
                        PageRequest pageRequest = new PageRequest(page, pagesize, Sort.Direction.DESC, ordering);
                        fip3 = foodrepo.findByTypeAndArchived(foodTypeConverter.convertToEntityAttribute(type), archived, pageRequest);
                    } 
                    else {
                        ordering = "id";
                        PageRequest pageRequest = new PageRequest(page, pagesize, Sort.Direction.DESC, ordering);
                        fip3 = foodrepo.findByTypeAndArchived(foodTypeConverter.convertToEntityAttribute(type), archived, pageRequest);
                    }
                }
                
                GlobalSettings globalSettings = globalSettingsrepo.findOne(1);
                Foods listOfFoods = new Foods();

                for (org.bootcamp.yum.data.entity.Food f : fip3) {

                    FoodsFoodList actuallyAFood = new FoodsFoodList();
                    Food food = new Food();

                    food.setFoodId(f.getId());
                    food.setDescription(f.getDescription());
                    food.setFoodName(f.getName());
                    food.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
                    food.setPrice(f.getPrice().doubleValue());
                    actuallyAFood.setFood(food);
                    listOfFoods.addFoodListItem(actuallyAFood);
                    listOfFoods.setVersion(globalSettings.getVersionOfFoodlist());
                }
                Iterable<org.bootcamp.yum.data.entity.Food> foodIterable = foodrepo.findAll();

                int counter = 0;

                if (type.equals("all")) {
                    counter = toIntExact(foodrepo.count());
                } else {
                    for (org.bootcamp.yum.data.entity.Food f : foodIterable) {

                        if (foodTypeConverter.convertToDatabaseColumn(f.getType()).equals(type)) {
                            counter++;
                        }
                    }
                }
                listOfFoods.setTotalNumber(counter);
                return listOfFoods;
            }
        }
    }

    @Transactional
    public void foodsPost(FoodItem foodItem) throws ApiException {
        
//        try {
//            Thread.sleep(6000);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(FoodsService.class.getName()).log(Level.SEVERE, null, ex);
//        }
        
        List foodNamesNotArchived = foodrepo.findByNameAndArchived(foodItem.getFoodName(), false);
        GlobalSettings globalSettings = globalSettingsrepo.findOne(1);
        if (foodNamesNotArchived.size() > 0) {//food with this name already exists.
            throw new ApiException(400, "Food already exists");
        } else {
            FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
            org.bootcamp.yum.data.entity.Food foodToInsertDb = new org.bootcamp.yum.data.entity.Food();
            foodToInsertDb.setDescription(foodItem.getDescription());
            foodToInsertDb.setName(foodItem.getFoodName());
            //check that we only accept food items with the certain food type
            if (!(foodItem.getFoodType().equalsIgnoreCase("Salad") || foodItem.getFoodType().equalsIgnoreCase("Main dish") || foodItem.getFoodType().equalsIgnoreCase("Drink"))) {
                throw new ApiException(400, "Bad request");
            }

            foodToInsertDb.setType(foodTypeConverter.convertToEntityAttribute(foodItem.getFoodType()));
            BigDecimal thePrice = new BigDecimal(foodItem.getPrice());
            foodToInsertDb.setPrice(thePrice);
            foodToInsertDb.setVersion(0);
            globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
            foodrepo.save(foodToInsertDb);
        }
    }

    @Transactional
    public int foodsFoodIdPut(Integer foodId, Boolean clone, FoodItem foodItemToUpdate) throws ApiException, ConcModException {
        org.bootcamp.yum.data.entity.Food foodById = foodrepo.findById(foodId);
        GlobalSettings globalSettings = globalSettingsrepo.findOne(1);

        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();

        if (foodById == null) {
            throw new ApiException(400, "Food not found");
        } else if ( //checking if the edit is not actually an edit 
                foodItemToUpdate.getFoodName().equals(foodById.getName())
                && foodItemToUpdate.getPrice().equals(foodById.getPrice().doubleValue())
                && foodItemToUpdate.getFoodType().equals(foodTypeConverter.convertToDatabaseColumn(foodById.getType()))
                && foodItemToUpdate.getDescription().equals(foodById.getDescription())) {
            System.out.println("***");
            throw new ApiException(400, "Bad request");
        } else if (foodById.isArchived() == true && clone == false) {
            throw new ApiException(400, "Bad request");
            //check if description about to be changed is the same as before
        } else if (!(foodItemToUpdate.getFoodType().equalsIgnoreCase("Salad") || foodItemToUpdate.getFoodType().equalsIgnoreCase("Main dish") || foodItemToUpdate.getFoodType().equalsIgnoreCase("Drink"))) {
            throw new ApiException(400, "Bad request");
        } else if (foodItemToUpdate.getVersion() < foodById.getVersion()) {

            FoodItem newestFoodItem = foodById.foodToFoodItem();
            throw new ConcModException(409, "Concurrent modification error.", newestFoodItem);

        } else if (foodById.isArchived() == false && clone == true) {
            if (checkEditable(foodById)) {
                foodById.setDescription(foodItemToUpdate.getDescription());
                foodById.setName(foodItemToUpdate.getFoodName());
                foodById.setPrice(new BigDecimal(foodItemToUpdate.getPrice()));
                foodById.setType(foodTypeConverter.convertToEntityAttribute(foodItemToUpdate.getFoodType()));
                foodById.setArchived(false);
                foodById.setVersion(foodById.getVersion() + 1);
                foodrepo.save(foodById);
                globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
                return foodById.getVersion();
            } else {
                if (foodItemToUpdate.getDescription().equals(foodById.getDescription())) {
                    throw new ApiException(400, "Bad request");
                }
                foodById.setArchived(true);
                org.bootcamp.yum.data.entity.Food foodToInsertDb = new org.bootcamp.yum.data.entity.Food();
                foodToInsertDb.setDescription(foodItemToUpdate.getDescription());
                foodToInsertDb.setName(foodItemToUpdate.getFoodName());
                if (!(foodItemToUpdate.getFoodType().equalsIgnoreCase("Salad") || foodItemToUpdate.getFoodType().equalsIgnoreCase("Main dish") || foodItemToUpdate.getFoodType().equalsIgnoreCase("Drink"))) {
                    throw new ApiException(400, "Bad request");
                }
                foodToInsertDb.setType(foodTypeConverter.convertToEntityAttribute(foodItemToUpdate.getFoodType()));
                BigDecimal thePrice = new BigDecimal(foodItemToUpdate.getPrice());
                foodToInsertDb.setPrice(thePrice);
                foodToInsertDb.setVersion(foodById.getVersion() + 1);
                globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
                foodrepo.save(foodToInsertDb);
            }
        } else if (foodById.isArchived() == true && clone == true) {
            org.bootcamp.yum.data.entity.Food foodToInsertDb = new org.bootcamp.yum.data.entity.Food();
            foodToInsertDb.setDescription(foodItemToUpdate.getDescription());
            foodToInsertDb.setName(foodItemToUpdate.getFoodName());
            if (!(foodItemToUpdate.getFoodType().equalsIgnoreCase("Salad") || foodItemToUpdate.getFoodType().equalsIgnoreCase("Main dish") || foodItemToUpdate.getFoodType().equalsIgnoreCase("Drink"))) {
                throw new ApiException(400, "Bad request");
            }
            foodToInsertDb.setType(foodTypeConverter.convertToEntityAttribute(foodItemToUpdate.getFoodType()));
            BigDecimal thePrice = new BigDecimal(foodItemToUpdate.getPrice());
            foodToInsertDb.setPrice(thePrice);
            foodToInsertDb.setVersion(foodById.getVersion());
            foodToInsertDb.setVersion(foodById.getVersion() + 1);
            globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
            foodrepo.save(foodToInsertDb);
        } else if (foodById.isArchived() == false && clone == false) {
            if (!checkEditable(foodById)) {
                foodById.setDescription(foodItemToUpdate.getDescription());
                foodById.setVersion(foodById.getVersion() + 1);
                globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
                foodrepo.save(foodById);
                return foodById.getVersion();
            }
            foodById.setDescription(foodItemToUpdate.getDescription());
            foodById.setName(foodItemToUpdate.getFoodName());
            foodById.setPrice(new BigDecimal(foodItemToUpdate.getPrice()));
            foodById.setType(foodTypeConverter.convertToEntityAttribute(foodItemToUpdate.getFoodType()));
            foodById.setArchived(false);
            foodById.setVersion(foodById.getVersion() + 1);
            globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
            foodrepo.save(foodById);
            return foodById.getVersion();
        }

        return foodById.getVersion();
    }

    public Food foodsFindByNameNameGet(String name) throws ApiException {
        List<org.bootcamp.yum.data.entity.Food> retrievedFood = foodrepo.findByNameAndArchived(name, false);
        Food askedFood = new Food();

        if (retrievedFood.size() == 0) {
            throw new ApiException(400, "Food not found");
        } else {
            org.bootcamp.yum.data.entity.Food retrievedFoodItem = retrievedFood.get(0);
            askedFood.setFoodId(retrievedFoodItem.getId());
            askedFood.setFoodName(retrievedFoodItem.getName());
            askedFood.setDescription(retrievedFoodItem.getDescription());
            FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
            askedFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(retrievedFoodItem.getType()));
            askedFood.setPrice(retrievedFoodItem.getPrice().doubleValue());
        }
        return askedFood;
    }

    public void foodsFoodIdDelete(Integer archived, Integer foodId) throws ApiException {
        boolean notDelete = false;
        LocalDate localDate = new LocalDate();
        org.bootcamp.yum.data.entity.Food retrievedFood = foodrepo.findByIdAndArchived(foodId, false);
        GlobalSettings globalSettings = globalSettingsrepo.findOne(1);

        if (retrievedFood == null) {
            throw new ApiException(400, "Food not found");
        }

        if (archived == 1) {
            retrievedFood.setArchived(true);
            foodrepo.save(retrievedFood);
            globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
        } else {
            for (OrderItem oItem : retrievedFood.getOrderItems()) {
                if (oItem.getDailyOrder() != null) {
                    notDelete = true;
                    if (oItem.getDailyOrder().getDailyMenu().getDailyMenuDate().isAfter(localDate)) {
                        throw new ApiException(400, "Bad Request");
                    }
                }
            }

            if (notDelete == true) {
                throw new ApiException(400, "Food cannot be deleted");
            } else {
                foodrepo.delete(retrievedFood);
                globalSettings.setVersionOfFoodlist(globalSettings.getVersionOfFoodlist() + 1);
            }
        }

    }

    public boolean checkEditable(org.bootcamp.yum.data.entity.Food food) {
        List<DailyMenu> dailyMenus = food.getDailyMenus();
        for (DailyMenu dailyMenu : dailyMenus) {
            List<DailyOrder> dailyOrders = dailyMenu.getDailyOrders();
            if (!dailyOrders.isEmpty()) {
                for (DailyOrder dailyOrder : dailyOrders) {
                    List<OrderItem> orderItems = dailyOrder.getOrderItems();
                    for (OrderItem orderItem : orderItems) {
                        if (orderItem.getFood().getId() == food.getId()) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

}
