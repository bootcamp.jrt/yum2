/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.LocalDate;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exceptions.ApiException;

import org.bootcamp.JwtCodec;

import org.bootcamp.yum.api.model.Body;
import org.bootcamp.yum.api.model.LoggedUserInfo;
import org.bootcamp.yum.api.model.Login;
import org.bootcamp.yum.api.model.Password;
import org.bootcamp.yum.api.model.Register;
import org.bootcamp.yum.api.model.TermsPolicy;
import org.bootcamp.yum.data.converter.PasswordProtection;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.bootcamp.yum.data.entity.User;
import static org.bootcamp.yum.data.entity.User.validate;
import org.bootcamp.yum.data.entity.UserType;
import org.bootcamp.yum.data.repository.GlobalSettingsRepository;
import org.bootcamp.yum.data.repository.UserRepository;


@Service
public class AuthorizeService {

    @Autowired
    private UserRepository userrepo;

    @Autowired
    private MailingService mailingService;

    @Autowired
    private GlobalSettingsRepository globalSettingsrepo;

    public LoggedUserInfo authLoginPost(Login body) throws ApiException {

        boolean encyptionCheck = false;

        PasswordProtection passwordProtection = null;

        try {
            passwordProtection = new PasswordProtection();
        } catch (Exception ex) {
            throw new ApiException(400, "All wrong");
        }

        User retrievedUser = userrepo.findByEmail(body.getEmail());

        if (retrievedUser != null) {
            try {
                encyptionCheck = retrievedUser.checkIfPassIsEncrypted();
            } catch (Exception ex) {
                throw new ApiException(400, "All wrong");
            }
        }

        if (body == null
                || body.getEmail().trim().equals("")
                || body.getEmail().trim().contains("\\s+")
                || body.getPassword().equals("")
                || body.getPassword().length() < 6) {
            //throw new ApiException(400, "Bad request, didnt give required fields");
            throw new ApiException(400, "Bad Request, invalid input fields");
            //check if email is ok
        } else if (!validate(body.getEmail())) {
            throw new ApiException(400, "Invalid mail");
        } else if (retrievedUser == null) {
            //throw new ApiException(404, "User not found");
            throw new ApiException(404, "User Not Found");

        } else if (!retrievedUser.isApproved()) {
            //throw new ApiException(404, "User not found");
            throw new ApiException(405, "User Not approved");
        } else if (encyptionCheck == false && !retrievedUser.getPassword().equals(body.getPassword())) {//wrong password
            //throw new ApiException(400, "Wrong Password");
            throw new ApiException(400, "Authentication failed");
        }/*else if (!passwordProtection.decrypt(retrievedUser.getPassword()).equals(body.getPassword())) {//!retrievedUser.getPassword().equals(body.getPassword())) {
            throw new ApiException(400, "Wrong Password");
        }*/ else if (encyptionCheck == true && !passwordProtection.decrypt(retrievedUser.getPassword()).equals(body.getPassword())) {
            //throw new ApiException(400, "Wrong Password");
            throw new ApiException(400, "Authentication failed");
        } else {
            //roles is an array of string:
            ArrayList<String> roles = new ArrayList<>();
            //if use exists in system then roles should be "hungry"
            roles.add("hungry");
            if (retrievedUser.getRole() == UserType.CHEF) {
                roles.add("chef");
            }
            if (retrievedUser.getRole() == UserType.ADMINISTRATOR) {
                roles.add("chef");
                roles.add("admin");
            }
            int listSize = roles.size();
            System.out.println(listSize);
            String subject = String.valueOf(retrievedUser.getId());            
            String compactJws = JwtCodec.encode(subject, roles); 

            LoggedUserInfo userInfo = new LoggedUserInfo();
            //*******************************************************
            // Refactoring Code for the new API login/post
            //*******************************************************
            userInfo.setFirstName(retrievedUser.getFirstName());
            userInfo.setLastName(retrievedUser.getLastName());
            userInfo.setUserId(retrievedUser.getId());
            userInfo.setRole(retrievedUser.getRole().toString());
            userInfo.setToken(compactJws);
            if (retrievedUser.getPicture() != null) 
            {
                userInfo.setHasPicture(1);
            }
            else
            {
                userInfo.setHasPicture(0);
            }
            //*******************************************************
            return userInfo;
        }
    }

    public void authForgotpwdPost(Body body) throws ApiException {

        User retrievedUser = userrepo.findByEmail(body.getEmail());
        //check if correct body format
        if (body == null
                || body.getEmail().trim().equals("")
                || body.getEmail().trim().contains("\\s+")
                || !validate(body.getEmail())) { //invalid email format
            throw new ApiException(400, "Invalid email input");
        } else if (retrievedUser == null) {
            //no such user
            throw new ApiException(404, "A user with that email was not found");
        } else if (!retrievedUser.isApproved()) {
            throw new ApiException(406, "User Not approved");
        } else {
            mailingService.sendResetPasswordLinkEmail(retrievedUser);
        }
    }

    public void authChangepwdSecretPut(Password password, String secret) throws ApiException {

        User userToChangeHisPass;
        Date date = new Date();
        java.sql.Timestamp todayTimestamp = new java.sql.Timestamp(date.getTime());
        if (userrepo.findByToken(secret) != null) {
            userToChangeHisPass = userrepo.findByToken(secret);
        } else {
            throw new ApiException(400, "You have to ask for a password reset");
        }
        
        if(userToChangeHisPass.getExpiration().before(todayTimestamp)){
            userToChangeHisPass.setToken(null);
            userToChangeHisPass.setExpiration(null);
            throw new ApiException(400, "Too late, ask again for a password reset");
        }
   

        PasswordProtection passwordProtection;
        String encrPassword = null;
        try {
            passwordProtection = new PasswordProtection();
            encrPassword = passwordProtection.encrypt(password.getPassword());
        } catch (Exception ex) {
            Logger.getLogger(AuthorizeService.class.getName()).log(Level.SEVERE, null, ex);
        }

        userToChangeHisPass.setPassword(encrPassword);
        userToChangeHisPass.setToken(null);
        userToChangeHisPass.setExpiration(null);
        userrepo.save(userToChangeHisPass);

    }

    public void authRegisterPost(Register body) throws ApiException {

        //check if all fields are OK -- or passWord more than 6 chars
        if (body == null
                || body.getFirstName().trim().equals("")
                || body.getLastName().trim().equals("")
                || body.getPassword().equals("")
                || body.getEmail().trim().equals("")
                || body.getPassword().length() < 6) {
            //throw new ApiException(400, "Bad request, didnt give required fields");
            throw new ApiException(400, "Bad request, invalid input fields");
            //check if user with same email  exist
        } else if (userrepo.findByEmail(body.getEmail()) != null) {
            throw new ApiException(400, "Such user with email already exists");
        } else if (!validate(body.getEmail())) {
            throw new ApiException(400, "Invalid email format");
        } else {
            PasswordProtection passwordProtection;
            String encrPassword = null;
            try {
                passwordProtection = new PasswordProtection();
                encrPassword = passwordProtection.encrypt(body.getPassword());
            } catch (Exception ex) {
                Logger.getLogger(AuthorizeService.class.getName()).log(Level.SEVERE, null, ex);
            }

            UserTypeConverter userTypeConverter = new UserTypeConverter();
            User newUser = new User();
            newUser.setEmail(body.getEmail());
            newUser.setFirstName(body.getFirstName());
            newUser.setLastName(body.getLastName());
            newUser.setPassword(encrPassword);
            newUser.setRole(userTypeConverter.convertToEntityAttribute("hungry"));
            LocalDate curr = new LocalDate();
            newUser.setRegistrationDate(curr);
            newUser.setVersion(0);
            userrepo.save(newUser);
            User userInDb = userrepo.findByEmail(newUser.getEmail());
            mailingService.sendNewUserEmailToAllAdmins(userInDb);
        }
    }

    @Transactional
    public TermsPolicy authTermsGet() throws ApiException {

        org.bootcamp.yum.data.entity.GlobalSettings globalSettings = globalSettingsrepo.findOne(1);
        TermsPolicy tp = new TermsPolicy();

        tp.setPolicy(globalSettings.getPrivacyPolicy());
        tp.setTerms(globalSettings.getTermsService());
        return tp;
    }

}
