/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.transaction.Transactional;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import org.joda.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.AllUsers;
import org.bootcamp.yum.api.model.Approval;
import org.bootcamp.yum.api.model.UserExtended;
import org.bootcamp.yum.api.model.UserGetId;
import org.bootcamp.yum.api.model.UserIdPutByAdmin;
import org.bootcamp.yum.api.model.UserPut;
import org.bootcamp.yum.data.converter.PasswordProtection;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.bootcamp.yum.data.entity.User;
import static org.bootcamp.yum.data.entity.User.validate;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserService {

    @Autowired
    private UserRepository userrepo;

    @Autowired
    private DailyOrderRepository dailyOrderrepo;
    @Autowired
    private MailingService mailingService;

    public boolean checkRole(String email) {
        if (email.equals("hungry") || email.equals("chef") || email.equals("admin")) {
            return true;
        } else {
            return false;
        }
    }

    public AllUsers usersGet(String sorting, Integer page, Integer pagesize, String order) throws ApiException {
        
        int totalNoUser = toIntExact(userrepo.count());
        
        if (totalNoUser + (pagesize - 1) < (page + 1) * pagesize) {
            throw new ApiException(400, "Bad page number parameter"); }
        else if (!(sorting.equals("email") || sorting.equals("role") || sorting.equals("date") || sorting.equals("approved"))) {
            throw new ApiException(400, "Bad sorting parameter");
        } else if (!(order.equals("asc") || order.equals("desc"))) {
            throw new ApiException(400, "Bad ordering parameter");
        } else if (pagesize < 1) {
            throw new ApiException(400, "Bad pagesize parameter");
        } else {
            if (sorting.equals("date")) {
                sorting = "registrationDate";
            }
            PageRequest pageRequest = null;
            if (order.equals("asc")) {
                pageRequest = new PageRequest(page, pagesize, Sort.Direction.ASC, sorting);
            } else {
                pageRequest = new PageRequest(page, pagesize, Sort.Direction.DESC, sorting);
            }
            Page<org.bootcamp.yum.data.entity.User> userEntities = userrepo.findAll(pageRequest);

            List<UserExtended> userList = new ArrayList<UserExtended>();
            UserTypeConverter userTypeConverter = new UserTypeConverter();
            for (org.bootcamp.yum.data.entity.User usr : userEntities) {
                UserGetId userModel = new UserGetId();
                UserExtended userModelFull = new UserExtended();
                userModelFull.setId(usr.getId());
                userModelFull.setRegistrationDate(usr.getRegistrationDate());
                userModel.setFirstName(usr.getFirstName());
                userModel.setLastName(usr.getLastName());
                userModel.setPicture(usr.getPicture());
                userModel.setEmail(usr.getEmail());
                userModel.setRole(userTypeConverter.convertToDatabaseColumn(usr.getRole()));
                userModel.setVersion(usr.getVersion());
                userModel.setApproved(usr.isApproved());
                userModelFull.setUser(userModel);
                userList.add(userModelFull);
                
                if(usr.getPicture() != null){
                    userModel.setHasPicture(1);
                }
                else{
                    userModel.setHasPicture(0);
                }
                
            }
            
            AllUsers allUsers = new AllUsers();
            allUsers.setUsers(userList);
            allUsers.setTotalNumber(totalNoUser);

            return allUsers;
        }
    }

    @Transactional
    public void usersIdDelete(Integer id, Boolean force) throws ApiException {
        
        User userRetrieved = userrepo.findById(id);
        User currentUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        if (userRetrieved == null) {
            throw new ApiException(404, "User not found");
        } else if (userRetrieved.isSuperAdmin()) {//cannot delete super Admin
            throw new ApiException(400, "Bad request, cannot delete super admin");
        } else if (userRetrieved.getId() == currentUser.getId()) {//someone cannot delete himself
            throw new ApiException(400, "Bad request, cannot delete yourself");
        } else if (force && userRetrieved.hasFutureOrders() && !userRetrieved.hasPastOrders()) {
            //here we attemp to delete user's future orders.
            //in the case he has an order for TODAY, we cant delete. 

            for (org.bootcamp.yum.data.entity.DailyOrder dOrder : userRetrieved.getDailyOrders()) {
                dailyOrderrepo.delete(dOrder.getId());
            }
            //we remove items that are mapped in the user list for daily orders
            userRetrieved.deleteDailyOrders();
            userrepo.delete(userRetrieved);

        } else if (userRetrieved.hasPastOrders() ) {
            throw new ApiException(405, "Bad request, cannot delete a user with past orders in his record");
        } else if ( userRetrieved.hasFutureOrders()) {
            throw new ApiException(406, "User has orders in the future, cannot be deleted without force");
        }
        else {
            userrepo.delete(userRetrieved);
        }
    }

    public void usersIdForgotpwdPost(Integer id) throws ApiException {

        User askedUser = userrepo.findById(id);

        if (askedUser == null) {
            throw new ApiException(404, "User not found");
        }

           mailingService.sendResetPasswordLinkEmail(askedUser);
    }

    public UserGetId usersIdGet(Integer id) throws ApiException {

        User userRetrieved = userrepo.findById(id);
        if (userRetrieved == null) {
            throw new ApiException(404, "User not found");
        } else {
            UserGetId userModel = new UserGetId();
            userModel.setFirstName(userRetrieved.getFirstName());
            userModel.setLastName(userRetrieved.getLastName());
            userModel.setEmail(userRetrieved.getEmail());
            userModel.setApproved(userRetrieved.isApproved());
            UserTypeConverter userTypeConverter = new UserTypeConverter();
            userModel.setRole(userTypeConverter.convertToDatabaseColumn(userRetrieved.getRole()));
            userModel.setPicture(userRetrieved.getPicture());
            userModel.setVersion(userRetrieved.getVersion());
            
            if(userRetrieved.getPicture() != null){
                userModel.setHasPicture(1);
            }else{
                userModel.setHasPicture(0);
            }

            return userModel;
        }

    }

    @Transactional
    public UserIdPutByAdmin usersIdPut(Integer id, UserIdPutByAdmin userToUpdate) throws ApiException, Exception, ConcModException {

        UserTypeConverter userTypeConverter = new UserTypeConverter();
        User userByMail = userrepo.findByEmail(userToUpdate.getUser().getEmail());
        User userRetrieved = userrepo.findById(id);
        if (checkRole(userToUpdate.getUser().getRole()) == false) {
            throw new ApiException(400, "Incorrect role assigned");
        } else if (userByMail != null && userByMail.getId() != id) {//userrepo.findByEmail(userToUpdate.getUser().getEmail()) != null) {
            throw new ApiException(400, "That e-mail is already in use");
        } else if (userToUpdate.getUser().getFirstName().trim().equals("")
                || userToUpdate.getUser().getLastName().trim().equals("")) {
            throw new ApiException(400, "Empty input field(s)");
        } else if (!validate(userToUpdate.getUser().getEmail().trim())) {
            throw new ApiException(400, "Invalid e-mail address");
        } else if (userRetrieved == null) {
            throw new ApiException(404, "User not found");
        } else if (userToUpdate.getVersion() < userRetrieved.getVersion()) {

            UserIdPutByAdmin latestUserModel = new UserIdPutByAdmin();
            UserPut lesserModel = new UserPut();
            lesserModel.setFirstName(userRetrieved.getFirstName());
            lesserModel.setLastName(userRetrieved.getLastName());
            lesserModel.setEmail(userRetrieved.getEmail());
            lesserModel.setPassword(userRetrieved.getPassword());
            lesserModel.setVersion(userRetrieved.getVersion());
            lesserModel.setRole(userTypeConverter.convertToDatabaseColumn(userRetrieved.getRole()));
            latestUserModel.setUser(lesserModel);
            latestUserModel.setApproved(userRetrieved.isApproved());
            latestUserModel.setPassword(userRetrieved.getPassword());
            latestUserModel.setVersion(userRetrieved.getVersion());
            throw new ConcModException(409, "User already updated. The latest version is returned", latestUserModel);

        } else {
            UserIdPutByAdmin userModel = new UserIdPutByAdmin();
            userRetrieved.setFirstName(userToUpdate.getUser().getFirstName());
            userRetrieved.setLastName(userToUpdate.getUser().getLastName());
            userRetrieved.setEmail(userToUpdate.getUser().getEmail());
            // The form that calls this api does not have a password input
            userRetrieved.setVersion(userToUpdate.getUser().getVersion() + 1);
            userRetrieved.setRole(userTypeConverter.convertToEntityAttribute(userToUpdate.getUser().getRole()));
            userRetrieved.setApproved(userToUpdate.getApproved());
            userrepo.save(userRetrieved);
            return userModel;
        }

    }

    @Transactional
    public void usersPost(UserPut user) throws ApiException, Exception {

        Iterable<org.bootcamp.yum.data.entity.User> userEntities = userrepo.findAll();
        UserTypeConverter userTypeConverter = new UserTypeConverter();

        //check if same user exists with the SAME email
        if (userEntities != null) {
            for (org.bootcamp.yum.data.entity.User usr : userEntities) {
                if (usr.getEmail().equals(user.getEmail())) {
                    throw new ApiException(400, "That email is already in use.");
                }
            }
        }//check if form contains any empty input field
        if (user.getFirstName().equals("")
                || user.getLastName().trim().equals("")
                || (user.getPassword().equals("") || user.getPassword().length() < 6)
                || user.getEmail().equals("")) {
            throw new ApiException(400, "Empty input field(s)");
        }
        if (!validate(user.getEmail())) {
            throw new ApiException(400, "Invalid e-mail address");
        }
        //checking if we attempt to insert an invalid role for the new user
        if (!(user.getRole().equalsIgnoreCase("HUNGRY") || user.getRole().equalsIgnoreCase("CHEF") || user.getRole().equalsIgnoreCase("ADMIN"))) {
            throw new ApiException(400, "Incorrect role assigned");
        }

        PasswordProtection passwordProtection = new PasswordProtection();
        org.bootcamp.yum.data.entity.User userToInsertDb = new org.bootcamp.yum.data.entity.User();
        String encryptedPassword = passwordProtection.encrypt(user.getPassword());
        userToInsertDb.setPassword(encryptedPassword);
        userToInsertDb.setFirstName(user.getFirstName());
        userToInsertDb.setLastName(user.getLastName());
        userToInsertDb.setEmail(user.getEmail());
        userToInsertDb.setRole(userTypeConverter.convertToEntityAttribute(user.getRole()));
        LocalDate curr = new LocalDate();
        userToInsertDb.setRegistrationDate(curr);
        userToInsertDb.setVersion(0);

        userrepo.save(userToInsertDb);

    }

    @Transactional
    public void usersIdApprovePut(Integer id, Approval approval, Boolean force) throws ApiException, ConcModException {

        User userRetrieved = userrepo.findById(id);
        //no such user found
        if (userRetrieved == null) {
            throw new ApiException(404, "User doesnt exist");
        } else if (userRetrieved.isSuperAdmin()) {//cannot approve//disapprove super Admin
            throw new ApiException(400, "Bad request, cannot approve//disapprove super admin");
        } else if (force && userRetrieved.hasFutureOrders()) {

            //here we attemp to disapprove a user who contains mixed pas and future orders.
            //we delete his future orders, keep his past orders, and disapprove him. 
            for (org.bootcamp.yum.data.entity.DailyOrder dOrder : userRetrieved.getDailyOrders()) {
                if (dOrder.getDailyMenu().getDailyMenuDate().isAfter(LocalDate.now().plusDays(1))) {
                    dailyOrderrepo.delete(dOrder.getId());
                }
            }

            if (approval.getAppr().booleanValue() == true) {
                userRetrieved.setApproved(true);
            } else if (approval.getAppr().booleanValue() == false) {
                userRetrieved.setApproved(false);
            } else {
                throw new ApiException(400, "Bad Request");
            }

        } else if (userRetrieved.hasFutureOrders()) {
            throw new ApiException(405, "Bad request, cannot disapprove a user with future orders in his record");
        } else {

            if (approval.getAppr().booleanValue() == true) {
                userRetrieved.setApproved(true);
                 mailingService.sendApprovalEmail(userRetrieved);
            } else if (approval.getAppr().booleanValue() == false) {
                userRetrieved.setApproved(false);
            } else {
                throw new ApiException(400, "Bad Request");
            }
        }

    }

    
    @Transactional
    public void postImageById(MultipartFile upfile,int id){
        
        byte[] bytes = null;
        
        // The actual logged in user
        User anotherUser = userrepo.findById(id);
        
        if (!upfile.isEmpty()) {
            try {
                bytes = upfile.getBytes();
            } catch (IOException ex) {
                Logger.getLogger(SettingsService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            InputStream in = new ByteArrayInputStream(bytes);
            
            BufferedImage image = null;
            try {
                image = ImageIO.read(in);
            } catch (IOException ex) {
                Logger.getLogger(SettingsService.class.getName()).log(Level.SEVERE, null, ex);
            }

            int min=0;

            if(image.getWidth()>image.getHeight())
                min=image.getHeight();
            else
                min=image.getWidth();

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                Thumbnails.of(image).sourceRegion(Positions.CENTER, min, min)
                    .size(180, 180).outputFormat("jpeg")
                    .toOutputStream(output);
            } catch (IOException ex) {
                Logger.getLogger(SettingsService.class.getName()).log(Level.SEVERE, null, ex);
            }

            //save this in the DAO
            bytes = output.toByteArray(); 
            
            anotherUser.setPicture(bytes);           
            userrepo.save(anotherUser);
        }
    }
    
    @Transactional
    public void deleteImageById(int id){
        
        // The actual logged in user
        User anotherUser = userrepo.findById(id);
        
        if(anotherUser.getPicture() != null){
            anotherUser.setPicture(null);            
            userrepo.save(anotherUser);     
        }
    }          
}
