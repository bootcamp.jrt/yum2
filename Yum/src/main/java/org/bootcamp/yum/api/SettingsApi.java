/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import io.swagger.annotations.*;
import java.io.File;
import javax.validation.constraints.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.UserGet;
import org.bootcamp.yum.api.model.UserPut;
import org.springframework.web.bind.annotation.RequestParam;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Api(value = "settings", description = "the settings API")
@RequestMapping(value="/api")
public interface SettingsApi {


    @ApiOperation(value = "Gets the settings of a user.", notes = "Gets the account settings of the current user.", response = UserGet.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "The account settings of the user.", response = UserGet.class) })
    @RequestMapping(value = "/settings",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin 
    ResponseEntity<UserGet> settingsGet() throws ApiException;
    
    @ApiOperation(value = "Changes the settings of a user.", notes = "Changes the account settings of the current user.", response = Void.class, authorizations = {
    @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "The account settings of the user changed succesfully.", response = Void.class),
        @ApiResponse(code = 400, message = "Bad request.", response = Void.class),
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = Void.class) })
    @RequestMapping(value = "/settings",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<Void> settingsPut(
        @ApiParam(value = "The user to update."  ) 
        @RequestBody UserPut userToUpdate) throws ApiException , ConcModException, Exception ;
    
    @ApiOperation(value = "Upload user picture.", notes = "", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "operation completed succesfully.", response = Void.class),
        @ApiResponse(code = 400, message = "operation could not complete.", response = Void.class) })
    @RequestMapping(value = "/settings/picture",
        produces = { "application/json" }, 
        consumes = { "multipart/form-data" },
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> settingsPicturePost(@ApiParam(value = "file detail") @RequestPart("file") MultipartFile file);
 
    @ApiOperation(value = "Delete user picture.", notes = "", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Image deleted succesfully.", response = Void.class),
        @ApiResponse(code = 400, message = "Operation could not completed", response = Void.class) })
    @RequestMapping(value = "/settings/picture",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> settingsPictureDelete();

    @ApiOperation(value = "Gets user's picture", notes = "", response = File.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Returns user's picture", response = File.class),
        @ApiResponse(code = 400, message = "Bad Request", response = File.class),
        @ApiResponse(code = 403, message = "Invalid token", response = File.class),
        @ApiResponse(code = 404, message = "image not found", response = File.class) })
    @RequestMapping(value = "/settings/picture/token",
        produces = { "image/jpeg" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Object> settingsPictureTokenGet( @NotNull @ApiParam(value = "The token of the current session.", required = true) 
    @RequestParam(value = "token", required = true) String token) throws ApiException;

}
