/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;


/**
 * AllUsers
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-11T12:33:29.635+03:00")

public class AllUsers   {
  @JsonProperty("totalNumber")
  private Integer totalNumber = null;

  @JsonProperty("users")
  private List<UserExtended> users = new ArrayList<UserExtended>();

  public AllUsers totalNumber(Integer totalNumber) {
    this.totalNumber = totalNumber;
    return this;
  }

   /**
   * Get totalNumber
   * @return totalNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getTotalNumber() {
    return totalNumber;
  }

  public void setTotalNumber(Integer totalNumber) {
    this.totalNumber = totalNumber;
  }

  public AllUsers users(List<UserExtended> users) {
    this.users = users;
    return this;
  }

  public AllUsers addUsersItem(UserExtended usersItem) {
    this.users.add(usersItem);
    return this;
  }

   /**
   * Get users
   * @return users
  **/
  @ApiModelProperty(value = "")
  public List<UserExtended> getUsers() {
    return users;
  }

  public void setUsers(List<UserExtended> users) {
    this.users = users;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AllUsers allUsers = (AllUsers) o;
    return Objects.equals(this.totalNumber, allUsers.totalNumber) &&
        Objects.equals(this.users, allUsers.users);
  }

  @Override
  public int hashCode() {
    return Objects.hash(totalNumber, users);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class AllUsers {\n");
    
    sb.append("    totalNumber: ").append(toIndentedString(totalNumber)).append("\n");
    sb.append("    users: ").append(toIndentedString(users)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

