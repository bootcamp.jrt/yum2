/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import javax.validation.constraints.*;

import io.swagger.annotations.*;
import java.io.File;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.UserExtended;
import org.bootcamp.yum.api.model.UserGetId;
import org.bootcamp.yum.api.model.UserIdPutByAdmin;
import org.bootcamp.yum.api.model.UserPut;
import org.bootcamp.yum.api.model.AllUsers;
import org.bootcamp.yum.api.model.Approval;
import org.springframework.web.bind.annotation.CrossOrigin;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Api(value = "users", description = "the users API")
@RequestMapping(value = "/api")
public interface UsersApi {

    @ApiOperation(value = "Gets a list of all the users", notes = "Gets the account information of all the users.", response = UserExtended.class, responseContainer = "List", authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The account information of all the users.", response = UserExtended.class)})
    @RequestMapping(value = "/users",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<AllUsers> usersGet(@NotNull @ApiParam(value = "Sort the list of users.", required = true, defaultValue = "date") @RequestParam(value = "sorting", required = true, defaultValue = "date") String sorting,
            @NotNull @ApiParam(value = "Show the users on the selected page.", required = true, defaultValue = "0") @RequestParam(value = "page", required = true, defaultValue = "0") Integer page,
            @NotNull @ApiParam(value = "Show the selected amount of users per page.", required = true, defaultValue = "10") @RequestParam(value = "pagesize", required = true, defaultValue = "10") Integer pagesize,
            @NotNull @ApiParam(value = "Select the ordering.", required = true, defaultValue = "asc") @RequestParam(value = "order", required = true, defaultValue = "asc") String order) throws ApiException;

    @ApiOperation(value = "Deletes a user.", notes = "deletes a user in the database", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "user succesfully deleted.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "user couldn't be deleted.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "user id couldn't be found.", response = Void.class)
        ,
        @ApiResponse(code = 406, message = "cannot delete user, record contains final orders.", response = Void.class)})
    @RequestMapping(value = "/users/{id}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> usersIdDelete(@ApiParam(value = "The id of the user to delete", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "Delete user's future orders.", defaultValue = "false") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force) throws ApiException;

    @ApiOperation(value = "Resets password.", notes = "Resets the password of a specific user.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "password reset succesfully.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "password could not be reset.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "user  couldn't be found.", response = Void.class)})
    @RequestMapping(value = "/users/{id}/forgotpwd",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> usersIdForgotpwdPost(@ApiParam(value = "The id of the user.", required = true) @PathVariable("id") Integer id) throws ApiException;

    @ApiOperation(value = "Gets the specific user", notes = "Gets the specific user by the id.", response = UserGetId.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "user information.", response = UserGetId.class)
        ,
        @ApiResponse(code = 404, message = "That user does not exist.", response = UserGetId.class)})
    @RequestMapping(value = "/users/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)   
    @CrossOrigin
    ResponseEntity<UserGetId> usersIdGet(@ApiParam(value = "The user's id that you want to search.", required = true) @PathVariable("id") Integer id) throws ApiException;

    @ApiOperation(value = "Changes the settings of a user.", notes = "Changes the account settings of the current user.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The account settings of the user changed succesfully.", response = UserIdPutByAdmin.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = UserIdPutByAdmin.class)
        ,
        @ApiResponse(code = 404, message = "That account does not exist.", response = UserIdPutByAdmin.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = UserIdPutByAdmin.class)})
    @RequestMapping(value = "/users/{id}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<UserIdPutByAdmin> usersIdPut(@ApiParam(value = "The id of the food item to update", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "The user to update.") @RequestBody UserIdPutByAdmin userToUpdate) throws ApiException, Exception, ConcModException;

    @ApiOperation(value = "Creates a new user", notes = "creates a new user in the database", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "user succesfully created.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "user couldn't be created.", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "user already exists.", response = Void.class)})
    @RequestMapping(value = "/users",
            produces = {"application/json"},
            method = RequestMethod.POST)   
    @CrossOrigin
    ResponseEntity<Void> usersPost(@ApiParam(value = "The user to create.") @RequestBody UserPut user) throws ApiException, Exception;

    @ApiOperation(value = "Select user approval.", notes = "Approves or not a specific user.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "operation completed succesfully.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "operation could not complete.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "user  couldn't be found.", response = Void.class)})
    @RequestMapping(value = "/users/{id}/approve",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<Void> usersIdApprovePut(@ApiParam(value = "The id of the user.", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "User approval.") @RequestBody Approval approval,
            @ApiParam(value = "Delete user's future orders, and set user to dissaproved.", defaultValue = "false") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force) throws ApiException, ConcModException;

    @ApiOperation(value = "Deletes a user.", notes = "deletes a user in the database", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "user's picture succesfully deleted.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "user's picture couldn't be deleted.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "user or user's picture not found.", response = Void.class)})
    @RequestMapping(value = "/users/{id}/picture",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> usersIdPictureDelete(@ApiParam(value = "The id of the user to delete", required = true) @PathVariable("id") Integer id) throws ApiException;

    @ApiOperation(value = "Upload user picture.", notes = "", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "operation completed succesfully.", response = Void.class),
        @ApiResponse(code = 400, message = "operation could not complete.", response = Void.class),
        @ApiResponse(code = 404, message = "user  couldn't be found.", response = Void.class) })
    @RequestMapping(value = "/users/{id}/picture",
        produces = { "application/json" }, 
        consumes = { "multipart/form-data" },
        method = RequestMethod.POST)
        @CrossOrigin
    ResponseEntity<Void> usersIdPicturePost(@ApiParam(value = "The id of the user.",required=true ) @PathVariable("id") Integer id,
        @ApiParam(value = "file detail") @RequestPart("file") MultipartFile upfile) throws ApiException;

    @ApiOperation(value = "Gets the requested user's picture.", notes = "Returns the requested user's picture.", response = File.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "admin", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Returns requested user's picture", response = File.class),
        @ApiResponse(code = 400, message = "Bad Request", response = File.class),
        @ApiResponse(code = 403, message = "Invalid token", response = File.class),
        @ApiResponse(code = 404, message = "User or picture not found.", response = File.class) })
    @RequestMapping(value = "/users/{id}/picture/token",
        produces = { "image/jpeg" }, 
        method = RequestMethod.GET)
        @CrossOrigin
    ResponseEntity<Object> usersIdPictureTokenGet(@ApiParam(value = "The requested user id.",required=true ) @PathVariable("id") Long id,
         @NotNull @ApiParam(value = "The admin's token.", required = true) @RequestParam(value = "token", required = true) String token) throws ApiException;     
}
