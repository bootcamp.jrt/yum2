/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.api.model.Menus;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Api(value = "menus", description = "the menus API")
@RequestMapping(value = "/api")
public interface MenusApi {

    @ApiOperation(value = "Gets the menu of a specified day.", notes = "Returns the menu of a specified day.", response = Menus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The menu for the day chosen.", response = Menus.class)
        ,
        @ApiResponse(code = 400, message = "Specify a valid date({day}-{month}-{year}).", response = Menus.class)})
    @RequestMapping(value = "/menus/day/{dayDate}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Menus> menusDayDayDateGet(@ApiParam(value = "The requested day", required = true) @PathVariable("dayDate") String dayDate) throws ApiException;

    @ApiOperation(value = "Gets daily menus of the current month.", notes = "Returns a list of daily menus of the current month.", response = Menus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The menu for each day of the month.", response = Menus.class)})
    @RequestMapping(value = "/menus/monthly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Menus> menusMonthlyGet() throws ApiException;

    @ApiOperation(value = "Gets monthly menu of a specified month.", notes = "Returns a list of monthly menus of a specified month.", response = Menus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The menu for each day of the month of a specified month.", response = Menus.class)
        ,
        @ApiResponse(code = 400, message = "Specify a valid month and year({month}-{year}).", response = Menus.class)})
    @RequestMapping(value = "/menus/monthly/{monthOfYear}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Menus> menusMonthlyMonthOfYearGet(@ApiParam(value = "The requested month", required = true) @PathVariable("monthOfYear") String monthOfYear) throws ApiException;

    @ApiOperation(value = "Gets weekly menu", notes = "Returns a list of daily (5) menus.", response = Menus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The menu for each day of the week.", response = Menus.class)})
    @RequestMapping(value = "/menus/weekly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Menus> menusWeeklyGet() throws ApiException;

    @ApiOperation(value = "Gets weekly menu of a specified week.", notes = "Returns a list of daily (5) menus of a specified week.", response = Menus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The menu for each day of the week of a specified week.", response = Menus.class)
        ,
        @ApiResponse(code = 400, message = "Specify a valid week and year({week}-{year}).", response = Menus.class)
        ,
        @ApiResponse(code = 409, message = "Week 53 does ot exist for this year", response = Menus.class)})
    @RequestMapping(value = "/menus/weekly/{weekOfYear}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Menus> menusWeeklyWeekOfYearGet(@ApiParam(value = "The requested week", required = true) @PathVariable("weekOfYear") String weekOfYear) throws ApiException;
}
