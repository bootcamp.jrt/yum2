/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.sql.Time;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * GlobalSettings
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-04T10:07:22.325+03:00")

public class GlobalSettings {

    @JsonProperty("timeOfLastOrder")
    private Time timeOfLastOrder = null;

    @JsonProperty("currency")
    private String currency = null;

    @JsonProperty("notes")
    private String notes = null;

    @JsonProperty("termsOfService")
    private String termsOfService = null;

    @JsonProperty("privacyPolicy")
    private String privacyPolicy = null;

    @JsonProperty("versionOfFoodList")
    private Integer versionOfFoodList = null;

    @JsonProperty("version")
    private Integer version = null;

    public Time getTimeOfLastOrder() {
        return timeOfLastOrder;
    }

    public void setTimeOfLastOrder(Time timeOfLastOrder) {
        this.timeOfLastOrder = timeOfLastOrder;
    }

    public GlobalSettings currency(String currency) {
        this.currency = currency;
        return this;
    }

    /**
     * Get currency
     *
     * @return currency
  *
     */
    @ApiModelProperty(required = true, value = "")
    @NotNull
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public GlobalSettings notes(String notes) {
        this.notes = notes;
        return this;
    }

    /**
     * Get notes
     *
     * @return notes
  *
     */
    @ApiModelProperty(value = "")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public GlobalSettings termsOfService(String termsOfService) {
        this.termsOfService = termsOfService;
        return this;
    }

    /**
     * Get termsOfService
     *
     * @return termsOfService
  *
     */
    @ApiModelProperty(value = "")
    public String getTermsOfService() {
        return termsOfService;
    }

    public void setTermsOfService(String termsOfService) {
        this.termsOfService = termsOfService;
    }

    public GlobalSettings privacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
        return this;
    }

    /**
     * Get privacyPolicy
     *
     * @return privacyPolicy
  *
     */
    @ApiModelProperty(value = "")
    public String getPrivacyPolicy() {
        return privacyPolicy;
    }

    public void setPrivacyPolicy(String privacyPolicy) {
        this.privacyPolicy = privacyPolicy;
    }

    public GlobalSettings versionOfFoodList(Integer versionOfFoodList) {
        this.versionOfFoodList = versionOfFoodList;
        return this;
    }

    /**
     * Get versionOfFoodList
     *
     * @return versionOfFoodList
  *
     */
    @ApiModelProperty(value = "")
    public Integer getVersionOfFoodList() {
        return versionOfFoodList;
    }

    public void setVersionOfFoodList(Integer versionOfFoodList) {
        this.versionOfFoodList = versionOfFoodList;
    }

    public GlobalSettings version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
  *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GlobalSettings globalSettings = (GlobalSettings) o;
        return Objects.equals(this.timeOfLastOrder, globalSettings.timeOfLastOrder)
                && Objects.equals(this.currency, globalSettings.currency)
                && Objects.equals(this.notes, globalSettings.notes)
                && Objects.equals(this.termsOfService, globalSettings.termsOfService)
                && Objects.equals(this.privacyPolicy, globalSettings.privacyPolicy)
                && Objects.equals(this.versionOfFoodList, globalSettings.versionOfFoodList)
                && Objects.equals(this.version, globalSettings.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeOfLastOrder, currency, notes, termsOfService, privacyPolicy, versionOfFoodList, version);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class GlobalSettings {\n");

        sb.append("    timeOfLastOrder: ").append(toIndentedString(timeOfLastOrder)).append("\n");
        sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
        sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
        sb.append("    termsOfService: ").append(toIndentedString(termsOfService)).append("\n");
        sb.append("    privacyPolicy: ").append(toIndentedString(privacyPolicy)).append("\n");
        sb.append("    versionOfFoodList: ").append(toIndentedString(versionOfFoodList)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
