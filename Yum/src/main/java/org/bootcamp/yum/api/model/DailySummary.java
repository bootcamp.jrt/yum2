/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.LocalDate;

/**
 * DailySummary
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-17T18:40:14.111+03:00")

public class DailySummary   {
  @JsonProperty("date")
  private LocalDate date = null;

  @JsonProperty("orderedFoodList")
  private List<DailySummaryOrderedFoodList> orderedFoodList = new ArrayList<DailySummaryOrderedFoodList>();

  @JsonProperty("userList")
  private List<DailySummaryUserList> userList = new ArrayList<DailySummaryUserList>();

  public DailySummary date(LocalDate date) {
    this.date = date;
    return this;
  }

   /**
   * Get date
   * @return date
  **/
  @ApiModelProperty(value = "")
  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public DailySummary orderedFoodList(List<DailySummaryOrderedFoodList> orderedFoodList) {
    this.orderedFoodList = orderedFoodList;
    return this;
  }

  public DailySummary addOrderedFoodListItem(DailySummaryOrderedFoodList orderedFoodListItem) {
    this.orderedFoodList.add(orderedFoodListItem);
    return this;
  }

   /**
   * Get orderedFoodList
   * @return orderedFoodList
  **/
  @ApiModelProperty(value = "")
  public List<DailySummaryOrderedFoodList> getOrderedFoodList() {
    return orderedFoodList;
  }

  public void setOrderedFoodList(List<DailySummaryOrderedFoodList> orderedFoodList) {
    this.orderedFoodList = orderedFoodList;
  }

  public DailySummary userList(List<DailySummaryUserList> userList) {
    this.userList = userList;
    return this;
  }

  public DailySummary addUserListItem(DailySummaryUserList userListItem) {
    this.userList.add(userListItem);
    return this;
  }

   /**
   * Get userList
   * @return userList
  **/
  @ApiModelProperty(value = "")
  public List<DailySummaryUserList> getUserList() {
    return userList;
  }

  public void setUserList(List<DailySummaryUserList> userList) {
    this.userList = userList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailySummary dailySummary = (DailySummary) o;
    return Objects.equals(this.date, dailySummary.date) &&
        Objects.equals(this.orderedFoodList, dailySummary.orderedFoodList) &&
        Objects.equals(this.userList, dailySummary.userList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(date, orderedFoodList, userList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailySummary {\n");
    
    sb.append("    date: ").append(toIndentedString(date)).append("\n");
    sb.append("    orderedFoodList: ").append(toIndentedString(orderedFoodList)).append("\n");
    sb.append("    userList: ").append(toIndentedString(userList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

