/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.api.model.Body;
import org.bootcamp.yum.api.model.Login;
import org.bootcamp.yum.api.model.Register;
import org.bootcamp.yum.api.model.LoggedUserInfo;
import org.bootcamp.yum.api.model.Password;
import org.bootcamp.yum.api.model.TermsPolicy;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Api(value = "auth", description = "the auth API")
@RequestMapping(value = "/api")
public interface AuthApi {

    @ApiOperation(value = "", notes = "User changes his password.", response = Void.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Password successfully reseted", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Couldnt change password.", response = Void.class)})
    @RequestMapping(value = "/auth/changepwd/{secret}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<Void> authChangepwdSecretPut(@ApiParam(value = "The user to update.", required = true) @RequestBody Password password,
            @ApiParam(value = "The secret send from email.", required = true) @PathVariable("secret") String secret) throws ApiException;

    @ApiOperation(value = "", notes = "Allow user to retrieve his password.", response = Void.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Reset password successfuly started", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "The user was not found.", response = Void.class)})
    @RequestMapping(value = "/auth/forgotpwd",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> authForgotpwdPost(@ApiParam(value = "The user that wants to retrieve his password.", required = true) @RequestBody Body body) throws ApiException, Exception;

    @ApiOperation(value = "", notes = "Allow users to log in, and to receive a Token", response = LoggedUserInfo.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Login Success", response = LoggedUserInfo.class)
        ,
        @ApiResponse(code = 403, message = "If user is not found (bad credentials) OR if user can not login (not approved)", response = LoggedUserInfo.class)})
    @RequestMapping(value = "/auth/login",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<LoggedUserInfo> authLoginPost(@ApiParam(value = "The email/password", required = true) @RequestBody Login body) throws ApiException;

    @ApiOperation(value = "", notes = "Allows unknown users to register.", response = Void.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Register Success", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "The user already exists.", response = Void.class)})
    @RequestMapping(value = "/auth/register",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> authRegisterPost(@ApiParam(value = "The user to be registered", required = true) @RequestBody Register body) throws ApiException;

    @ApiOperation(value = "Gets the terms of service and privacy policy.", notes = "Gets the terms of service and privacy policy for anonymous users.", response = TermsPolicy.class, tags = {"auth",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The terms and the policy.", response = TermsPolicy.class)})
    @RequestMapping(value = "/auth/terms",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<TermsPolicy> authTermsGet() throws ApiException;

}
