/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.util.List;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.bootcamp.yum.exceptions.ConcurrentCreationException;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcurrentDeletionException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.DailyMenu;
import org.bootcamp.yum.api.model.DailyMenuUpdate;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-20T10:37:23.143+03:00")

@Api(value = "dailyMenus", description = "the dailyMenus API")
@RequestMapping(value = "/api")
public interface DailyMenusApi {

    @ApiOperation(value = "Gets a daily menu", notes = "Returns a daily menu matching the id.", response = DailyMenu.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The daily menu machting the id.", response = DailyMenu.class)
        ,
        @ApiResponse(code = 404, message = "That daily menu does not exist.", response = DailyMenu.class)})
    @RequestMapping(value = "/dailyMenus/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailyMenu> dailyMenusIdGet(@ApiParam(value = "The Daily menus's id", required = true) @PathVariable("id") Integer id)  throws ApiException;

    @ApiOperation(value = "Gets a daily menu", notes = "Returns a daily menu matching the id.", response = DailyMenuUpdate.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Daily Menu succesfully updated.", response = DailyMenuUpdate.class)
        ,
        @ApiResponse(code = 400, message = "Daily Menu could not be updated.", response = DailyMenuUpdate.class)
        ,
        @ApiResponse(code = 404, message = "That daily menu does not exist.", response = DailyMenuUpdate.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = DailyMenuUpdate.class)})
    @RequestMapping(value = "/dailyMenus/{id}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<DailyMenuUpdate> dailyMenusIdPut(@ApiParam(value = "The Daily menus's id", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "The daily menu to update.") @RequestBody DailyMenuUpdate dailyMenuToUpdate) throws ApiException, ConcModException, ConcurrentDeletionException;

    @ApiOperation(value = "Gets daily menus of the current month.", notes = "Returns a list of daily menus of the current month.", response = DailyMenu.class, responseContainer = "List", authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The menu for each day of the week for the whole current month.", response = DailyMenu.class)})
    @RequestMapping(value = "/dailyMenus/monthly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<List<DailyMenu>> dailyMenusMonthlyGet() throws ApiException;

    @ApiOperation(value = "Gets daily menus of the month chosen.", notes = "Returns a list of daily menus of the chosen month.", response = DailyMenu.class, responseContainer = "List", authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The menu for each day of the week for the chosen month.", response = DailyMenu.class)
        ,
        @ApiResponse(code = 400, message = "Specify a valid month and year({month}-{year}).", response = DailyMenu.class)})
    @RequestMapping(value = "/dailyMenus/monthly/{month}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<List<DailyMenu>> dailyMenusMonthlyMonthGet(@ApiParam(value = "The requested month", required = true) @PathVariable("month") String month) throws ApiException;

    @ApiOperation(value = "Creates a daily menu for the day", notes = "creates a daily menu in the database", response = DailyMenu.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "daily menu succesfully created.", response = DailyMenu.class)
        ,
        @ApiResponse(code = 400, message = "daily menu couldn't be created.", response = DailyMenu.class)})
    @RequestMapping(value = "/dailyMenus",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<DailyMenu> dailyMenusPost(@ApiParam(value = "The daily menu to create.") @RequestBody DailyMenu dailyMenu) throws ApiException, ConcurrentCreationException;
}
