/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;

/**
 * DailyMenuFoods
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-04T20:11:18.401+03:00")

public class DailyMenuFoods   {
  @JsonProperty("foodId")
  private Integer foodId = null;

  @JsonProperty("foodName")
  private String foodName = null;

  @JsonProperty("foodType")
  private String foodType = null;

  @JsonProperty("description")
  private String description = null;

  @JsonProperty("isOrdered")
  private Boolean isOrdered = null;

  public DailyMenuFoods foodId(Integer foodId) {
    this.foodId = foodId;
    return this;
  }

   /**
   * Get foodId
   * @return foodId
  **/
  @ApiModelProperty(value = "")
  public Integer getFoodId() {
    return foodId;
  }

  public void setFoodId(Integer foodId) {
    this.foodId = foodId;
  }

  public DailyMenuFoods foodName(String foodName) {
    this.foodName = foodName;
    return this;
  }

   /**
   * Get foodName
   * @return foodName
  **/
  @ApiModelProperty(value = "")
  public String getFoodName() {
    return foodName;
  }

  public void setFoodName(String foodName) {
    this.foodName = foodName;
  }

  public DailyMenuFoods foodType(String foodType) {
    this.foodType = foodType;
    return this;
  }

   /**
   * Get foodType
   * @return foodType
  **/
  @ApiModelProperty(value = "")
  public String getFoodType() {
    return foodType;
  }

  public void setFoodType(String foodType) {
    this.foodType = foodType;
  }

  public DailyMenuFoods description(String description) {
    this.description = description;
    return this;
  }

   /**
   * Get description
   * @return description
  **/
  @ApiModelProperty(value = "")
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public DailyMenuFoods isOrdered(Boolean isOrdered) {
    this.isOrdered = isOrdered;
    return this;
  }

   /**
   * Get isOrdered
   * @return isOrdered
  **/
  @ApiModelProperty(value = "")
  public Boolean getIsOrdered() {
    return isOrdered;
  }

  public void setIsOrdered(Boolean isOrdered) {
    this.isOrdered = isOrdered;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailyMenuFoods dailyMenuFoods = (DailyMenuFoods) o;
    return Objects.equals(this.foodId, dailyMenuFoods.foodId) &&
        Objects.equals(this.foodName, dailyMenuFoods.foodName) &&
        Objects.equals(this.foodType, dailyMenuFoods.foodType) &&
        Objects.equals(this.description, dailyMenuFoods.description) &&
        Objects.equals(this.isOrdered, dailyMenuFoods.isOrdered);
  }

  @Override
  public int hashCode() {
    return Objects.hash(foodId, foodName, foodType, description, isOrdered);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailyMenuFoods {\n");
    
    sb.append("    foodId: ").append(toIndentedString(foodId)).append("\n");
    sb.append("    foodName: ").append(toIndentedString(foodName)).append("\n");
    sb.append("    foodType: ").append(toIndentedString(foodType)).append("\n");
    sb.append("    description: ").append(toIndentedString(description)).append("\n");
    sb.append("    isOrdered: ").append(toIndentedString(isOrdered)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

