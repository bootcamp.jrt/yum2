/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.OptimisticLockException;
import javax.transaction.Transactional;

import javax.validation.constraints.*;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.access.prepost.PreAuthorize;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.Food;
import org.bootcamp.yum.api.model.FoodEdit;
import org.bootcamp.yum.api.model.FoodItem;
import org.bootcamp.yum.api.model.Foods;
import org.bootcamp.yum.api.service.FoodsService;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.OrderItem;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-30T16:48:53.123+03:00")
@ComponentScan("org.bootcamp.yum.data.repository")
@Controller
public class FoodsApiController implements FoodsApi {


    private FoodsService foodsService;

    @Autowired
    public FoodsApiController(FoodsService foodsService) {
        this.foodsService = foodsService;
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<FoodEdit> foodsFoodIdGet(@ApiParam(value = "The food's foodId", required = true) @PathVariable("foodId") Integer foodId,
            @NotNull @ApiParam(value = "Can edit all properties or only description.", required = true, defaultValue = "false") @RequestParam(value = "checkIfEditable", required = true, defaultValue = "false") Boolean checkIfEditable) throws ApiException {
            FoodEdit fEdit = foodsService.foodsFoodIdGet(foodId, checkIfEditable);
                return new ResponseEntity<FoodEdit>(fEdit, HttpStatus.OK);

    }
    
    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<Foods> foodsGet( @NotNull @ApiParam(value = "Show only archived or not foods.", required = true, defaultValue = "false") @RequestParam(value = "archived", required = true, defaultValue="false") Boolean archived,
         @NotNull @ApiParam(value = "Show foods on certain page.", required = true, defaultValue = "0") @RequestParam(value = "page", required = true, defaultValue="0") Integer page,
         @NotNull @ApiParam(value = "Show restricted amount of foods per page.", required = true, defaultValue = "10") @RequestParam(value = "pagesize", required = true, defaultValue="10") Integer pagesize,
         @NotNull @ApiParam(value = "Show only the selected type of food.", required = true, defaultValue = "all") @RequestParam(value = "type", required = true, defaultValue="all") String type,
         @NotNull @ApiParam(value = "Select ordering of foods.", required = true, defaultValue = "none") @RequestParam(value = "ordering", required = true, defaultValue="none") String ordering,
         @ApiParam(value = "") @RequestParam(value = "version", required = false) Integer version) throws ApiException{
        Foods foods = foodsService.foodsGet(version, archived, page, pagesize, type, ordering);
        return new ResponseEntity<Foods>(foods, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    public ResponseEntity<Void> foodsPost(@ApiParam(value = "The new food to create.") @RequestBody FoodItem foodItem) throws ApiException {

            foodsService.foodsPost(foodItem);
                return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Transactional
    public ResponseEntity<FoodItem> foodsFoodIdPut(@ApiParam(value = "The id of the food item to update", required = true) @PathVariable("foodId") Integer foodId,
            @NotNull @ApiParam(value = "Create a clone if food is not editable.", required = true, defaultValue = "false") @RequestParam(value = "clone", required = true, defaultValue = "false") Boolean clone,
            @ApiParam(value = "The food to update.") @RequestBody FoodItem foodItemToUpdate) throws ApiException, ConcModException {

            try {
                FoodItem foodItem = new FoodItem();  
                foodItem.setVersion(foodsService.foodsFoodIdPut(foodId, clone, foodItemToUpdate));
                    return new ResponseEntity<FoodItem>(foodItem, HttpStatus.OK);
            } catch (OptimisticLockException ex) {
                try {
                    FoodEdit fEdit = foodsService.foodsFoodIdGet(foodId, false);
                    throw new ConcModException(fEdit);
                } catch (ApiException ex1) {
                    Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                    throw new ApiException(500, "Concurrent modification exception: internal error");
                }
            }                
    }

    public boolean checkEditable(org.bootcamp.yum.data.entity.Food food) {
        List<DailyMenu> dailyMenus = food.getDailyMenus();
        for (DailyMenu dailyMenu : dailyMenus) {
            List<DailyOrder> dailyOrders = dailyMenu.getDailyOrders();
            if (!dailyOrders.isEmpty()) {
                for (DailyOrder dailyOrder : dailyOrders) {
                    List<OrderItem> orderItems = dailyOrder.getOrderItems();
                    for (OrderItem orderItem : orderItems) {
                        if (orderItem.getFood().getId() == food.getId()) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<Food> foodsFindByNameNameGet(@ApiParam(value = "The food's name that you want to search.", required = true) @PathVariable("name") String name) throws ApiException {
            Food food = foodsService.foodsFindByNameNameGet(name);
                return new ResponseEntity<Food>(food, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Transactional
    public ResponseEntity<Void> foodsFoodIdDelete(@NotNull @ApiParam(value = "Select whether to delete or archive.", required = true) @RequestParam(value = "archived", required = true) Integer archived,
            @ApiParam(value = "The id of the food item to delete", required = true) @PathVariable("foodId") Integer foodId) throws ApiException {

            foodsService.foodsFoodIdDelete(archived, foodId);
                return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
