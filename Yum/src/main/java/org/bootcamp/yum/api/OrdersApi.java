/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.util.List;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.bootcamp.yum.exceptions.ConcurrentCreationException;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcurrentDeletionException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.DailyOrder;
import org.bootcamp.yum.api.model.DailyOrderMonthSummary;
import org.bootcamp.yum.api.model.DailySummary;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Api(value = "orders", description = "the orders API")
@RequestMapping(value="/api")
public interface OrdersApi {

    @ApiOperation(value = "Gets daily order summary.", notes = "Returns the daily order summary of the specified date.", response = DailySummary.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "The daily order summary of the specified date.", response = DailySummary.class),
        @ApiResponse(code = 400, message = "Specify a valid day ({day}).", response = DailySummary.class),
        @ApiResponse(code = 404, message = "That daily order does not exist.", response = DailySummary.class) })
    @RequestMapping(value = "/orders/daily/{day}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailySummary> ordersDailyDayGet(@ApiParam(value = "The requested day",required=true ) @PathVariable("day") Integer day) throws ApiException;

    @ApiOperation(value = "Deletes an order", notes = "Deletes the daily order matching the id.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Daily Order succesfully deleted.", response = Void.class),
        @ApiResponse(code = 400, message = "Daily Order could not be deleted.", response = Void.class),
        @ApiResponse(code = 404, message = "That daily order does not exist.", response = Void.class),
        @ApiResponse(code = 410, message = "Query again for this menu.", response = Void.class) })
    @RequestMapping(value = "/orders/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> ordersIdDelete(@ApiParam(value = "The Daily order's id",required=true ) @PathVariable("id") Integer id,
        @ApiParam(value = "The version of the menu the daily order belongs." ,required=true ) @RequestBody DailyOrder menuVersion) throws ApiException, ConcurrentDeletionException;


    @ApiOperation(value = "Gets an order", notes = "Gets the daily order matching the id.", response = DailyOrder.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Daily Order succesfully received.", response = DailyOrder.class),
        @ApiResponse(code = 400, message = "Daily Order could not be received.", response = DailyOrder.class),
        @ApiResponse(code = 404, message = "That daily order does not exist.", response = DailyOrder.class) })
    @RequestMapping(value = "/orders/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<DailyOrder> ordersIdGet(@ApiParam(value = "The Daily order's id",required=true ) 
    @PathVariable("id") Integer id) throws ApiException ;


    @ApiOperation(value = "Updates an order", notes = "Updates the daily order matching the id.", response = DailyOrder.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Daily Order succesfully updated.", response = DailyOrder.class),
        @ApiResponse(code = 400, message = "Daily Order could not be updated.", response = DailyOrder.class),
        @ApiResponse(code = 404, message = "That daily order does not exist.", response = DailyOrder.class),
        @ApiResponse(code = 409, message = "concurrent modification error.", response = DailyOrder.class),
        @ApiResponse(code = 410, message = "Query again for this menu.", response = Void.class)})
    @RequestMapping(value = "/orders/{id}",
        produces = { "application/json" }, 
        method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<DailyOrder> ordersIdPut(@ApiParam(value = "The Daily order's id",required=true ) @PathVariable("id") Integer id,
        @ApiParam(value = "The daily order to update."  ) @RequestBody DailyOrder dailyOrderToUpdate) throws ApiException, ConcModException;


    @ApiOperation(value = "Gets daily orders of the current month.", notes = "Returns a list of the monthly orders with every food the have including quantity and total price", response = DailyOrderMonthSummary.class, responseContainer = "List", authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "The orders for each day of the month.", response = DailyOrderMonthSummary.class) })
    @RequestMapping(value = "/orders/monthly",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<List<DailyOrderMonthSummary>> ordersMonthlyGet() throws ApiException;


    @ApiOperation(value = "Gets monthly daily orders of a specific month.", notes = "Returns a list of daily orders of a specific month.", response = DailyOrderMonthSummary.class, responseContainer = "List", authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "The order for each day of the specific month.", response = DailyOrderMonthSummary.class),
        @ApiResponse(code = 400, message = "Specify a valid month ({month}).", response = DailyOrderMonthSummary.class) })
    @RequestMapping(value = "/orders/monthly/{month}",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<List<DailyOrderMonthSummary>> ordersMonthlyMonthGet(@ApiParam(value = "The requested month",required=true ) @PathVariable("month") String month) throws ApiException;


    @ApiOperation(value = "Creates a daily order", notes = "creates a daily order in the database", response = DailyOrder.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "hungry", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "daily order succesfully created.", response = DailyOrder.class),
        @ApiResponse(code = 400, message = "daily order could not be created.", response = DailyOrder.class),
        @ApiResponse(code = 406, message = "daily order already exists.", response = DailyOrder.class),
        @ApiResponse(code = 410, message = "Query again for this menu.", response = Void.class)})
    @RequestMapping(value = "/orders",
        produces = { "application/json" }, 
        method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<DailyOrder> ordersPost(@ApiParam(value = "The daily order to create."  ) @RequestBody DailyOrder dailyOrder) throws ApiException,ConcurrentCreationException;

}
