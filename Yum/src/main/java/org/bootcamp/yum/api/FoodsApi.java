/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import javax.validation.constraints.*;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.Food;
import org.bootcamp.yum.api.model.FoodEdit;
import org.bootcamp.yum.api.model.FoodItem;
import org.bootcamp.yum.api.model.Foods;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Api(value = "foods", description = "the foods API")
@RequestMapping(value = "/api")
public interface FoodsApi {

    @ApiOperation(value = "Gets a food item by its name", notes = "Returns a food item matching the name if it is not archived.", response = Food.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "A Food Item", response = Food.class)
        ,
        @ApiResponse(code = 404, message = "That food does not exist or is archived.", response = Food.class)})
    @RequestMapping(value = "/foods/findByName/{name}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<Food> foodsFindByNameNameGet(@ApiParam(value = "The food's name that you want to search.", required = true) @PathVariable("name") String name) throws ApiException;

    @ApiOperation(value = "Deletes a food.", notes = "deletes a food in the database", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "food succesfully deleted.", response = Void.class)
        ,
        @ApiResponse(code = 209, message = "food succesfully archived.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "food couldn't be deleted.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "food id couldn't be found.", response = Void.class)
        ,
        @ApiResponse(code = 406, message = "food can only be archived.", response = Void.class)})
    @RequestMapping(value = "/foods/{foodId}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    @CrossOrigin
    ResponseEntity<Void> foodsFoodIdDelete(@NotNull @ApiParam(value = "Select whether to delete or archive.", required = true) @RequestParam(value = "archived", required = true) Integer archived,
            @ApiParam(value = "The id of the food item to delete", required = true) @PathVariable("foodId") Integer foodId) throws ApiException;

    @ApiOperation(value = "Gets a food item", notes = "Returns a food item matching the foodId.", response = FoodEdit.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "A Food Item", response = FoodEdit.class)
        ,
        @ApiResponse(code = 404, message = "That food does not exists.", response = FoodEdit.class)})
    @RequestMapping(value = "/foods/{foodId}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<FoodEdit> foodsFoodIdGet(@ApiParam(value = "The food's foodId", required = true) @PathVariable("foodId") Integer foodId,
            @NotNull @ApiParam(value = "Can edit all properties or only description.", required = true, defaultValue = "false") @RequestParam(value = "checkIfEditable", required = true, defaultValue = "false") Boolean checkIfEditable) throws ApiException;

    @ApiOperation(value = "Updates a food.", notes = "updates a food in the database", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "food succesfully updated.", response = FoodItem.class)
        ,
        @ApiResponse(code = 400, message = "food couldn't be updated.", response = FoodItem.class)
        ,
        @ApiResponse(code = 404, message = "food id couldn't be found.", response = FoodItem.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = FoodItem.class)})
    @RequestMapping(value = "/foods/{foodId}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<FoodItem> foodsFoodIdPut(@ApiParam(value = "The id of the food item to update", required = true) @PathVariable("foodId") Integer foodId,
            @NotNull @ApiParam(value = "Create a clone if food is not editable.", required = true, defaultValue = "false") @RequestParam(value = "clone", required = true, defaultValue = "false") Boolean clone,
            @ApiParam(value = "The food to update.") @RequestBody FoodItem foodItemToUpdate) throws ApiException,  ConcModException;

    @ApiOperation(value = "Gets all foods.", notes = "Get all food items from database.", response = Foods.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags={ "chef", })
    @ApiResponses(value = { 
        @ApiResponse(code = 200, message = "Every food item returned.", response = Foods.class) })
    @RequestMapping(value = "/foods",
        produces = { "application/json" }, 
        method = RequestMethod.GET)
    @CrossOrigin        
    ResponseEntity<Foods> foodsGet( @NotNull @ApiParam(value = "Show only archived or not foods.", required = true, defaultValue = "false") @RequestParam(value = "archived", required = true, defaultValue="false") Boolean archived,
         @NotNull @ApiParam(value = "Show foods on certain page.", required = true, defaultValue = "0") @RequestParam(value = "page", required = true, defaultValue="0") Integer page,
         @NotNull @ApiParam(value = "Show restricted amount of foods per page.", required = true, defaultValue = "10") @RequestParam(value = "pagesize", required = true, defaultValue="10") Integer pagesize,
         @NotNull @ApiParam(value = "Show only the selected type of food.", required = true, defaultValue = "all") @RequestParam(value = "type", required = true, defaultValue="all") String type,
         @NotNull @ApiParam(value = "Select ordering of foods.", required = true, defaultValue = "none") @RequestParam(value = "ordering", required = true, defaultValue="none") String ordering,
         @ApiParam(value = "") @RequestParam(value = "version", required = false) Integer version) throws ApiException;       
        
    
    @ApiOperation(value = "Creates a new food", notes = "creates a new food in the database", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "new food succesfully created.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "new food could not be created.", response = Void.class)
        ,
        @ApiResponse(code = 406, message = "food already exists.", response = Void.class)})
    @RequestMapping(value = "/foods",
            produces = {"application/json"},
            method = RequestMethod.POST)
    @CrossOrigin
    ResponseEntity<Void> foodsPost(@ApiParam(value = "The new food to create.") @RequestBody FoodItem foodItem) throws ApiException;

}
