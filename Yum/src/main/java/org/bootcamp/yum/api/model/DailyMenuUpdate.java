/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.LocalDate;

/**
 * DailyMenuUpdate
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T16:37:18.242+03:00")

public class DailyMenuUpdate   {
  @JsonProperty("dailyMenuDate")
  private LocalDate dailyMenuDate = null;

  @JsonProperty("dailyMenuId")
  private Integer dailyMenuId = null;

  @JsonProperty("version")
  private Integer version = null;

  @JsonProperty("foods")
  private List<DailyMenuUpdateFoods> foods = new ArrayList<DailyMenuUpdateFoods>();

  public DailyMenuUpdate dailyMenuDate(LocalDate dailyMenuDate) {
    this.dailyMenuDate = dailyMenuDate;
    return this;
  }

   /**
   * Get dailyMenuDate
   * @return dailyMenuDate
  **/
  @ApiModelProperty(value = "")
  public LocalDate getDailyMenuDate() {
    return dailyMenuDate;
  }

  public void setDailyMenuDate(LocalDate dailyMenuDate) {
    this.dailyMenuDate = dailyMenuDate;
  }

  public DailyMenuUpdate dailyMenuId(Integer dailyMenuId) {
    this.dailyMenuId = dailyMenuId;
    return this;
  }

   /**
   * Get dailyMenuId
   * @return dailyMenuId
  **/
  @ApiModelProperty(value = "")
  public Integer getDailyMenuId() {
    return dailyMenuId;
  }

  public void setDailyMenuId(Integer dailyMenuId) {
    this.dailyMenuId = dailyMenuId;
  }

  public DailyMenuUpdate version(Integer version) {
    this.version = version;
    return this;
  }

   /**
   * Get version
   * @return version
  **/
  @ApiModelProperty(value = "")
  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public DailyMenuUpdate foods(List<DailyMenuUpdateFoods> foods) {
    this.foods = foods;
    return this;
  }

  public DailyMenuUpdate addFoodsItem(DailyMenuUpdateFoods foodsItem) {
    this.foods.add(foodsItem);
    return this;
  }

   /**
   * Get foods
   * @return foods
  **/
  @ApiModelProperty(value = "")
  public List<DailyMenuUpdateFoods> getFoods() {
    return foods;
  }

  public void setFoods(List<DailyMenuUpdateFoods> foods) {
    this.foods = foods;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailyMenuUpdate dailyMenuUpdate = (DailyMenuUpdate) o;
    return Objects.equals(this.dailyMenuDate, dailyMenuUpdate.dailyMenuDate) &&
        Objects.equals(this.dailyMenuId, dailyMenuUpdate.dailyMenuId) &&
        Objects.equals(this.version, dailyMenuUpdate.version) &&
        Objects.equals(this.foods, dailyMenuUpdate.foods);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dailyMenuDate, dailyMenuId, version, foods);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailyMenuUpdate {\n");
    
    sb.append("    dailyMenuDate: ").append(toIndentedString(dailyMenuDate)).append("\n");
    sb.append("    dailyMenuId: ").append(toIndentedString(dailyMenuId)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

