/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import java.io.IOException;
import java.io.InputStream;
import static java.lang.Math.toIntExact;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.transaction.Transactional;
import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;
import org.bootcamp.yum.data.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.UserGet;
import org.bootcamp.yum.api.model.UserPut;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.bootcamp.yum.data.entity.User;
import static org.bootcamp.yum.data.entity.User.validate;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.multipart.MultipartFile;


@Service
public class SettingsService {
    
    @Autowired
    private UserRepository userrepo;
    
    @Transactional
    public UserGet settingsGet() throws ApiException 
    {
        UserTypeConverter userTypeConverter = new UserTypeConverter();
        //User userByid = userrepo.findById(id);
        // Logged in user
        User loggedUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));

        UserGet user = new UserGet();
        
        // Check if the id is wrong, or if it doesn't exist
        if (loggedUser == null) 
        {
            throw new ApiException(404, "User not found");
        } 
        // DAO to DTO 
        else 
        {
            user.setFirstName(loggedUser.getFirstName());
            user.setLastName(loggedUser.getLastName());
            user.setEmail(loggedUser.getEmail());
            user.setRole(userTypeConverter.convertToDatabaseColumn(loggedUser.getRole()));
            user.setVersion(loggedUser.getVersion());
            
            if(loggedUser.getPicture() != null){
                user.setHasPicture(1);
            }
        }
        return user;
    }
    
    @Transactional
    public void settingsPut(UserPut userToUpdate) throws ApiException , ConcModException, Exception 
    {
        UserTypeConverter userTypeConverter = new UserTypeConverter();
        
         // Logged in user
        User userByid = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        
        // We will use that object to check id the email is already in use
        User userByMail = userrepo.findByEmail(userToUpdate.getEmail());
        
        
        
        // Check if the input user does no exist
        if (userToUpdate == null) 
        {
            throw new ApiException(404, "Input data not found");
        }
        // Check if the id is wrong, or if it doesn't exist
        else if (userByid == null) 
        {
            throw new ApiException(404, "User not found");
        }
        // If we have a user with this email and it is not the user we want to 
        // update, throw exception, because emails must be unique
        else if (userByMail != null && userByMail.getId() != userByid.getId()) 
        {
            throw new ApiException(400, "That e-mail is already in use");
        }
        // Check if any form field is empty. If the email is empty, an exception
        // would have already caught it
        else if (userToUpdate.getFirstName().trim().equals("")
                || userToUpdate.getLastName().trim().equals("")) 
        {
            throw new ApiException(400, "Empty input field(s)");
        }
        // Validate the email's format 
        else if (!validate(userToUpdate.getEmail().trim())) 
        {
            throw new ApiException(400, "Invalid e-mail address");
        }
        // Concurrent modification check
        else if (userToUpdate.getVersion() < userByid.getVersion())
        {
            UserPut userLatest = new UserPut();
            // userByid has already the latest version
            // Fill user DTO with data from the latest DAO, userByid
            userLatest.setFirstName(userByid.getFirstName());
            userLatest.setLastName(userByid.getLastName());
            userLatest.setEmail(userByid.getEmail());
            userLatest.setRole(userTypeConverter.convertToDatabaseColumn(userByid.getRole()));
            userLatest.setPassword(userByid.getPassword());
            userLatest.setVersion(userByid.getVersion());
            throw new ConcModException(409, "Concurrent modification error.", userLatest);
        }
        else
        {
            userByid.setFirstName(userToUpdate.getFirstName());
            userByid.setLastName(userToUpdate.getLastName());
            userByid.setEmail(userToUpdate.getEmail());
            userByid.setVersion(userToUpdate.getVersion()+1);
            userByid.setPassword(userToUpdate.getPassword());
            userrepo.save(userByid);          
        }    
    }
    
    @Transactional
    public void postImage(MultipartFile upfile){
        
        byte[] bytes = null;
        
        // Logged in user
        User loggedUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        
        if (!upfile.isEmpty()) {
            try {
                bytes = upfile.getBytes();
            } catch (IOException ex) {
                Logger.getLogger(SettingsService.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            InputStream in = new ByteArrayInputStream(bytes);
            
            BufferedImage image = null;
            try {
                image = ImageIO.read(in);
            } catch (IOException ex) {
                Logger.getLogger(SettingsService.class.getName()).log(Level.SEVERE, null, ex);
            }

            int min=0;

            if(image.getWidth()>image.getHeight())
                min=image.getHeight();
            else
                min=image.getWidth();

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            try {
                Thumbnails.of(image).sourceRegion(Positions.CENTER, min, min)
                    .size(180, 180).outputFormat("jpeg")
                    .toOutputStream(output);
            } catch (IOException ex) {
                Logger.getLogger(SettingsService.class.getName()).log(Level.SEVERE, null, ex);
            }
            //save this in the DAO
            bytes = output.toByteArray();
            loggedUser.setPicture(bytes);           
            userrepo.save(loggedUser);
        }
    }
    
    @Transactional
    public void deleteImage(){
        
        // The actual logged in user
        User loggedUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        
        if(loggedUser.getPicture() != null){
            loggedUser.setPicture(null);
            userrepo.save(loggedUser);             
        }
    }
}
