/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import javax.persistence.OptimisticLockException;

import org.bootcamp.yum.exceptions.ConcurrentCreationException;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcurrentDeletionException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.DailyOrder;
import org.bootcamp.yum.api.model.DailyOrderMonthSummary;
import org.bootcamp.yum.api.model.DailySummary;
import org.bootcamp.yum.api.service.OrdersService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-30T16:09:00.088+03:00")

@Controller
public class OrdersApiController implements OrdersApi {

    private OrdersService orderService;

    @Autowired
    public OrdersApiController(OrdersService orderService) {
        this.orderService = orderService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyOrder> ordersPost(@ApiParam(value = "The daily order to create.")
            @RequestBody DailyOrder dailyOrder) throws ApiException, ConcurrentCreationException {
        DailyOrder dailyOrderDTO = orderService.ordersPost(dailyOrder);
        return new ResponseEntity<DailyOrder>(dailyOrderDTO, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<List<DailyOrderMonthSummary>> ordersMonthlyGet() throws ApiException {
        List<DailyOrderMonthSummary> listToReturn = orderService.ordersMonthlyGet();
        return new ResponseEntity<>(listToReturn, HttpStatus.OK);
    }

    public ResponseEntity<List<DailyOrderMonthSummary>> ordersMonthlyMonthGet(@ApiParam(value = "The requested month", required = true)
            @PathVariable("month") String month) throws ApiException {
        List<DailyOrderMonthSummary> listToReturn = orderService.ordersMonthlyMonthGet(month);
        return new ResponseEntity<>(listToReturn, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailySummary> ordersDailyDayGet(@ApiParam(value = "The requested day", required = true)
            @PathVariable("day") Integer day) throws ApiException {
        DailySummary dailysumaryToReturn = orderService.ordersDailyDayGet(day);
        return new ResponseEntity<>(dailysumaryToReturn, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Void> ordersIdDelete(@ApiParam(value = "The Daily order's id", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "The version of the menu the daily order belongs.", required = true) @RequestBody DailyOrder menuVersion) throws ApiException,ConcurrentDeletionException {
        orderService.ordersIdDelete(id,menuVersion);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyOrder> ordersIdGet(@ApiParam(value = "The Daily order's id", required = true)
            @PathVariable("id") Integer id) throws ApiException {
        DailyOrder orderByIdToReturn = orderService.ordersIdGet(id);
        return new ResponseEntity<>(orderByIdToReturn, HttpStatus.OK);
    }

    @Override
    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<DailyOrder> ordersIdPut(@ApiParam(value = "The Daily order's id", required = true) @PathVariable("id") Integer id, @ApiParam(value = "The daily order to update.") @RequestBody DailyOrder dailyOrderToUpdate) throws ApiException, ConcModException {
        try {
            DailyOrder orderByIdToReturn = orderService.ordersIdPut(id, dailyOrderToUpdate);
            return new ResponseEntity<>(orderByIdToReturn, HttpStatus.OK);
        } catch (OptimisticLockException ex) {
            try {
                DailyOrder dailyOrderLatest = orderService.ordersIdGet(id);
                throw new ConcModException(409, "Concurrent modification error.", dailyOrderLatest);
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }
}
