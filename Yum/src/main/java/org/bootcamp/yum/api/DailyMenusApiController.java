/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.util.List;
import io.swagger.annotations.*;
import javax.persistence.OptimisticLockException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import org.bootcamp.yum.api.model.DailyMenu;
import org.bootcamp.yum.api.model.DailyMenuUpdate;
import org.bootcamp.yum.api.service.DailyMenuService;
import org.bootcamp.yum.exceptions.ConcurrentCreationException;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcurrentDeletionException;
import org.bootcamp.yum.exceptions.ConcModException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-04T15:41:12.684+03:00")

@Controller
public class DailyMenusApiController implements DailyMenusApi {

    private DailyMenuService dmService;

    @Autowired
    public DailyMenusApiController(DailyMenuService dmService) {
        this.dmService = dmService;
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyMenu> dailyMenusIdGet(@ApiParam(value = "The Daily menus's id", required = true) @PathVariable("id") Integer id) throws ApiException {
        // do some magic!
        DailyMenu dmById = dmService.dailyMenusIdGet(id);
        return new ResponseEntity<>(dmById,HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyMenuUpdate> dailyMenusIdPut(@ApiParam(value = "The Daily menus's id", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "The daily menu to update.") @RequestBody DailyMenuUpdate dailyMenuToUpdate) throws ApiException, ConcModException, ConcurrentDeletionException {

        try {
            DailyMenuUpdate dummyDMU = dmService.dailyMenusIdPut(id, dailyMenuToUpdate);
            return new ResponseEntity<>(dummyDMU, HttpStatus.OK);
        } catch (OptimisticLockException ex) {
            DailyMenu dailyMenuLatest = dmService.dailyMenusIdGet(id);
            throw new ConcModException(409, "Concurrent modification error.", dailyMenuLatest);
        }
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<List<DailyMenu>> dailyMenusMonthlyGet() throws ApiException {

        List<DailyMenu> dmList = dmService.dailyMenusMonthlyGet();
        return new ResponseEntity<List<DailyMenu>>(dmList, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<List<DailyMenu>> dailyMenusMonthlyMonthGet(@ApiParam(value = "The requested month", required = true) @PathVariable("month") String month) throws ApiException {

        List<DailyMenu> dmList = dmService.dailyMenusMonthlyMonthGet(month);
        return new ResponseEntity<>(dmList, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyMenu> dailyMenusPost(@ApiParam(value = "The daily menu to create.") @RequestBody DailyMenu dailyMenu) throws ApiException, ConcurrentCreationException {

        DailyMenu dailyMenuToReturn = dmService.dailyMenusPost(dailyMenu);
        return new ResponseEntity<>(dailyMenuToReturn, HttpStatus.OK);
    }
}
