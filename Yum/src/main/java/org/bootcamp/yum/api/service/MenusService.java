/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import org.joda.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.api.model.Menus;
import org.bootcamp.yum.api.model.MenusInner;
import org.bootcamp.yum.api.model.MenusInnerFoods;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class MenusService {

    @Autowired
    private DailyMenuRepository dailyMenurepo;

    @Autowired
    private UserRepository userrepo;

    public Menus menusMonthlyGet() throws ApiException {

        User currentUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        System.out.println("*****");
        System.out.println(currentUser.getId());

        LocalDate localDate = new LocalDate();
        Menus menus = new Menus();

        int iYear = localDate.getYear();
        int iMonth = localDate.getMonthOfYear();
        int iDay = 1;
        LocalDate localDate2 = new LocalDate().withYear(iYear).withMonthOfYear(iMonth).withDayOfMonth(1);
        Calendar mycal = new GregorianCalendar(iYear, iMonth - 1, iDay);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < daysInMonth; i++) {
            LocalDate ld = localDate2.plusDays(i);
            MenusInner mi = new MenusInner();
            mi.setDailyMenuDate(ld);
            menus.add(mi);
        }
        LocalDate start = localDate.dayOfMonth().withMinimumValue();
        LocalDate end = localDate.dayOfMonth().withMaximumValue();
        Iterable<org.bootcamp.yum.data.entity.DailyMenu> dm = dailyMenurepo.findByDailyMenuDateBetween(start, end);
        for (org.bootcamp.yum.data.entity.DailyMenu dMenu : dm) {
            if (dMenu.getDailyMenuDate().getMonthOfYear() == localDate.getMonthOfYear() && dMenu.getDailyMenuDate().getYear() == localDate.getYear()) {
                MenusInner menu = new MenusInner();
                List<Food> food = dMenu.getFoods();
                for (Food f : food) {
                    FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
                    org.bootcamp.yum.api.model.Food modelFood = new org.bootcamp.yum.api.model.Food();
                    modelFood.setFoodName(f.getName());
                    modelFood.setFoodId(f.getId());
                    modelFood.setDescription(f.getDescription());
                    modelFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
                    modelFood.setPrice(f.getPrice().doubleValue());
                    MenusInnerFoods mif = new MenusInnerFoods();
                    mif.setFood(modelFood);
                    List<OrderItem> orderItems = f.getOrderItems();
                    if (menu.getOrderId() == null) {
                        menu.setOrderId(0);
                    }
                    if (orderItems != null) {
                        for (OrderItem oItem : orderItems) {
                            if (oItem.getMemberKey().getFoodId() == f.getId()) {
                                DailyOrder dOrder = oItem.getDailyOrder();
                                if (dOrder.getUser().getId() == currentUser.getId() && dOrder.getDailyMenu() == dMenu) {
                                    mif.setQuantity(oItem.getQuantity());

                                    menu.setOrderId(oItem.getMemberKey().getDailyOrderId());

                                }
                            }
                        }
                    }
                    menu.addFoodsItem(mif);
                }
                menu.setDailyMenuDate(dMenu.getDailyMenuDate());
                menu.setDailyMenuId(dMenu.getDailyMenuId());

                menus.set(menu.getDailyMenuDate().getDayOfMonth() - 1, menu);

            }
        }
        return menus;
    }

    public Menus menusMonthlyMonthOfYearGet(String monthOfYear) throws ApiException {

        User currentUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        Menus menus = new Menus();

        if (monthOfYear.matches("[0-9][0-9]?-[0-9]{4}")) {
            String[] weekAndYear = monthOfYear.split("-");
            String month = weekAndYear[0];
            String year = weekAndYear[1];
            if (Integer.parseInt(month) < 13 && Integer.parseInt(month) >= 1 && Integer.parseInt(year) > 1900) {
                LocalDate localDate = new LocalDate().withYear(Integer.parseInt(year)).withMonthOfYear(Integer.parseInt(month));

                int iYear = localDate.getYear();
                int iMonth = localDate.getMonthOfYear();
                int iDay = 1;
                LocalDate localDate2 = new LocalDate().withYear(iYear).withMonthOfYear(iMonth).withDayOfMonth(1);
                Calendar mycal = new GregorianCalendar(iYear, iMonth - 1, iDay);
                int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
                for (int i = 0; i < daysInMonth; i++) {
                    LocalDate ld = localDate2.plusDays(i);
                    MenusInner mi = new MenusInner();
                    mi.setDailyMenuDate(ld);
                    menus.add(mi);
                }

                LocalDate start = localDate.dayOfMonth().withMinimumValue();
                LocalDate end = localDate.dayOfMonth().withMaximumValue();
                Iterable<org.bootcamp.yum.data.entity.DailyMenu> dm = dailyMenurepo.findByDailyMenuDateBetween(start, end);
                for (org.bootcamp.yum.data.entity.DailyMenu dMenu : dm) {
                    if (dMenu.getDailyMenuDate().getMonthOfYear() == localDate.getMonthOfYear() && dMenu.getDailyMenuDate().getYear() == localDate.getYear()) {
                        MenusInner menu = new MenusInner();
                        List<Food> food = dMenu.getFoods();
                        for (Food f : food) {
                            FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
                            org.bootcamp.yum.api.model.Food modelFood = new org.bootcamp.yum.api.model.Food();
                            modelFood.setFoodName(f.getName());
                            modelFood.setFoodId(f.getId());
                            modelFood.setDescription(f.getDescription());
                            modelFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
                            modelFood.setPrice(f.getPrice().doubleValue());
                            MenusInnerFoods mif = new MenusInnerFoods();
                            mif.setFood(modelFood);
                            List<OrderItem> orderItems = f.getOrderItems();
                            if (menu.getOrderId() == null) {
                                menu.setOrderId(0);
                            }
                            if (orderItems != null) {
                                for (OrderItem oItem : orderItems) {
                                    if (oItem.getMemberKey().getFoodId() == f.getId()) {
                                        DailyOrder dOrder = oItem.getDailyOrder();
                                        if (dOrder.getUser().getId() == currentUser.getId() && dOrder.getDailyMenu() == dMenu) {
                                            mif.setQuantity(oItem.getQuantity());

                                            menu.setOrderId(oItem.getMemberKey().getDailyOrderId());

                                        }
                                    }
                                }
                            }
                            menu.addFoodsItem(mif);
                        }
                        menu.setDailyMenuDate(dMenu.getDailyMenuDate());
                        menu.setDailyMenuId(dMenu.getDailyMenuId());
                        menus.set(menu.getDailyMenuDate().getDayOfMonth() - 1, menu);
                    }
                }
            } else {
                throw new ApiException(400, "Bad request");
            }
        } else {
            throw new ApiException(400, "Bad request");
        }
        return menus;
    }

    public Menus menusWeeklyGet() throws ApiException {

        LocalDate localDate = new LocalDate();

        return getMenusWeeklyByYearAndWeek(localDate.getYear(), localDate.getWeekOfWeekyear());

    }

    public Menus menusDayDayDateGet(String date) throws ApiException {

        String[] dayWeekAndYear = date.split("-");
        String year = dayWeekAndYear[0];
        String month = dayWeekAndYear[1];
        String day = dayWeekAndYear[2];

        User currentUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        LocalDate ldFromString = new LocalDate(year+"-"+month+"-"+day);
        org.bootcamp.yum.data.entity.DailyMenu dm = dailyMenurepo.findByDailyMenuDate(ldFromString);
        Menus menus = new Menus();
        MenusInner menu = new MenusInner();
        List<Food> food = dm.getFoods();
        for (Food f : food) {
            FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
            org.bootcamp.yum.api.model.Food modelFood = new org.bootcamp.yum.api.model.Food();
            modelFood.setFoodName(f.getName());
            modelFood.setFoodId(f.getId());
            modelFood.setDescription(f.getDescription());
            modelFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
            modelFood.setPrice(f.getPrice().doubleValue());
            MenusInnerFoods mif = new MenusInnerFoods();
            mif.setFood(modelFood);
            List<OrderItem> orderItems = f.getOrderItems();
            if (menu.getOrderId() == null) {
                menu.setOrderId(0);
            }
            if (orderItems != null) {
                for (OrderItem oItem : orderItems) {
                    if (oItem.getMemberKey().getFoodId() == f.getId()) {
                        DailyOrder dOrder = oItem.getDailyOrder();
                        if (dOrder.getUser().getId() == currentUser.getId() && dOrder.getDailyMenu() == dm) {
                            mif.setQuantity(oItem.getQuantity());

                            menu.setOrderId(oItem.getMemberKey().getDailyOrderId());

                        }
                    }
                }
            }
            menu.addFoodsItem(mif);
        }
        menu.setDailyMenuDate(dm.getDailyMenuDate());
        menu.setDailyMenuId(dm.getDailyMenuId());
        menu.setVersion(dm.getVersion());
        if (dm != null) {
            menus.add(menu);
        } else {
            menus.add(new MenusInner());
        }
        return menus;

    }

    public Menus menusWeeklyWeekOfYearGet(String weekOfYear) throws ApiException {

        if (weekOfYear.matches("[0-9][0-9]?-[0-9]{4}")) {
            String[] weekAndYear = weekOfYear.split("-");
            String week = weekAndYear[0];
            String year = weekAndYear[1];
            return getMenusWeeklyByYearAndWeek(Integer.parseInt(year), Integer.parseInt(week));

        } else {
            throw new ApiException(400, "Bad request");
        }
    }

    private Menus getMenusWeeklyByYearAndWeek(int year, int week) throws ApiException {

        User currentUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));

        Menus menus = new Menus();
        if (week <= 53 && week > 0 && year > 1900) {
            LocalDate localDate = null;
            try {
                localDate = new LocalDate().withYear(year).withDayOfWeek(1).withWeekOfWeekyear(week);
            } catch (org.joda.time.IllegalFieldValueException ex) {
                throw new ApiException(409, "Week 53 does ot exist for this year");
            }

            LocalDate start = localDate.withWeekOfWeekyear(week).withDayOfWeek(1);
            LocalDate end = localDate.withWeekOfWeekyear(week).withDayOfWeek(7);
            Iterable<org.bootcamp.yum.data.entity.DailyMenu> dm = dailyMenurepo.findByDailyMenuDateBetween(start, end);
            ArrayList<LocalDate> dateRange = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                LocalDate ld = localDate.plusDays(i);
                dateRange.add(ld);
                MenusInner mi = new MenusInner();
                mi.setDailyMenuDate(ld);
                menus.add(mi);
            }
            for (org.bootcamp.yum.data.entity.DailyMenu dMenu : dm) {
                for (LocalDate ld : dateRange) {
                    if (ld.toString().equals(dMenu.getDailyMenuDate().toString())) {
                        MenusInner menu = new MenusInner();
                        List<Food> food = dMenu.getFoods();
                        for (Food f : food) {
                            FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
                            org.bootcamp.yum.api.model.Food modelFood = new org.bootcamp.yum.api.model.Food();
                            modelFood.setFoodName(f.getName());
                            modelFood.setFoodId(f.getId());
                            modelFood.setDescription(f.getDescription());
                            modelFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
                            modelFood.setPrice(f.getPrice().doubleValue());
                            MenusInnerFoods mif = new MenusInnerFoods();
                            mif.setFood(modelFood);
                            List<OrderItem> orderItems = f.getOrderItems();
                            if (menu.getOrderId() == null) {
                                menu.setOrderId(0);
                            }
                            if (orderItems != null) {
                                for (OrderItem oItem : orderItems) {
                                    if (oItem.getMemberKey().getFoodId() == f.getId()) {
                                        DailyOrder dOrder = oItem.getDailyOrder();
                                        if (dOrder.getUser().getId() == currentUser.getId() && dOrder.getDailyMenu() == dMenu) {
                                            mif.setQuantity(oItem.getQuantity());

                                            menu.setOrderId(oItem.getMemberKey().getDailyOrderId());

                                        }
                                    }
                                }
                            }
                            menu.addFoodsItem(mif);
                        }
                        menu.setDailyMenuDate(ld);
                        menu.setDailyMenuId(dMenu.getDailyMenuId());
                        menus.set(menu.getDailyMenuDate().getDayOfWeek() - 1, menu);
                    }
                }
            }
        } else {
            throw new ApiException(400, "Bad request");
        }

        return menus;

    }
}
