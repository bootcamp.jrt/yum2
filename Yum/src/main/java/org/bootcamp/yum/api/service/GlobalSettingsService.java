/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.GlobalSettings;
import org.bootcamp.yum.data.converter.TimeConverter;
import org.bootcamp.yum.data.repository.GlobalSettingsRepository;

@Service
public class GlobalSettingsService {

    @Autowired
    private GlobalSettingsRepository globalSettingsrepo;
    
    @Transactional
    public GlobalSettings globalsettingsGet() throws ApiException {
        
        org.bootcamp.yum.data.entity.GlobalSettings globalSettings = globalSettingsrepo.findOne(1);
        GlobalSettings globSet = new GlobalSettings();
        TimeConverter timeConverter = new TimeConverter();

        globSet.setCurrency(globalSettings.getCurrency());
        globSet.setNotes(globalSettings.getNotes());
        globSet.setPrivacyPolicy(globalSettings.getPrivacyPolicy());
        globSet.setTermsOfService(globalSettings.getTermsService());
        globSet.setVersion(globalSettings.getVersion());
        globSet.setTimeOfLastOrder(timeConverter.convertToDatabaseColumn(globalSettings.getTimeLastOrder()));

        return globSet;
    }
    
    @Transactional
    public org.bootcamp.yum.data.entity.GlobalSettings globalsettingsPut(GlobalSettings settings) throws ApiException, ConcModException {
        
        org.bootcamp.yum.data.entity.GlobalSettings globalSettings = globalSettingsrepo.findOne(1);
        TimeConverter timeConverter = new TimeConverter();
        
        if (globalSettings== null){
            throw new ApiException(404, "Settings not found");
        }else if(settings.getVersion() < globalSettings.getVersion()){
            
            GlobalSettings latestGSright = new GlobalSettings();
            latestGSright.setCurrency(globalSettings.getCurrency());
            latestGSright.setNotes(globalSettings.getNotes());
            latestGSright.setPrivacyPolicy(globalSettings.getPrivacyPolicy());
            latestGSright.setTermsOfService(globalSettings.getTermsService());
            latestGSright.setTimeOfLastOrder(timeConverter.convertToDatabaseColumn(globalSettings.getTimeLastOrder()));
            latestGSright.setVersion(globalSettings.getVersion());
            
            throw new ConcModException(409, "Concurrent modification error", latestGSright);
        }else{
            globalSettings.setCurrency(settings.getCurrency());
            globalSettings.setNotes(settings.getNotes());
            globalSettings.setPrivacyPolicy(settings.getPrivacyPolicy());
            globalSettings.setTermsService(settings.getTermsOfService());          
            globalSettings.setTimeLastOrder(timeConverter.convertToEntityAttribute(settings.getTimeOfLastOrder()));
            globalSettings.setVersion(settings.getVersion()+1);           
            globalSettingsrepo.save(globalSettings);     
            return globalSettings;
        }  
    }
}
