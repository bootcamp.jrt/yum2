/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import static java.lang.Math.toIntExact;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.stereotype.Service;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.core.context.SecurityContextHolder;

import org.bootcamp.yum.api.model.DailyOrder;
import org.bootcamp.yum.api.model.DailyOrderFoods;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.UserType;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.GlobalSettingsRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.exceptions.ApiException;

@Service
public class MailingService {

    @Autowired
    private MailSender mailSender;

    @Value("${yum.mail.senderEmailAddress}")
    private String senderEmailAddress;

    @Value("${yum.hostname}")
    private String yumHostname;

    @Autowired
    private UserRepository userrepo;

    @Autowired
    private FoodRepository foodrepo;

    @Autowired
    private GlobalSettingsRepository globalSettingsrepo;
    
    public void sendNewUserEmailToAllAdmins(User newUser) {

        List<User> adminList = userrepo.findByRole(UserType.ADMINISTRATOR);

        String text = "\nA new user just registered. "
                + "Here are the new user details\r\n"
                + "first name: " + newUser.getFirstName() + "\r\n"
                + "last name: " + newUser.getLastName() + "\r\n"
                + "email: " + newUser.getEmail() + "\r\n\r\n"
                + "You have to approve this user so he/she can log in.\r\n"
                + "click on this link to approve the user:\r\n"
                + yumHostname + "/admin/users/" + newUser.getId();
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmailAddress);

        for (User user : adminList) {
            message.setTo(user.getEmail());
            message.setSubject("[Yum] new user registered");
            //add a greeting sentence to the text:
            message.setText("Dear " + user.getFirstName() + " " + user.getLastName() + ",\r\n" + text);
            mailSender.send(message);
        }
    }

    public void sendConfirmOrderEmailToHungry(DailyOrder order) throws ApiException {

//         send this email to the user associated with the order.
//         prepare the text like follow:
        User currentUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));

        Iterable<org.bootcamp.yum.data.entity.Food> foodList = foodrepo.findAll();
        HashMap<Integer, Food> foodMap = new HashMap<Integer, Food>();

        for (Food f : foodList) {
            foodMap.put(f.getId(), f);
        }

        int total = 0;
        String foodListTest = "";
        for (DailyOrderFoods food : order.getFoods()) {

            foodListTest += "            " + foodMap.get(food.getFoodId()).getName() + "   - Quantity: " + food.getQuantity() + " x " + foodMap.get(food.getFoodId()).getPrice() + " euro \r\n";
            total += food.getQuantity() * foodMap.get(food.getFoodId()).getPrice().intValue();
        }

        String text = "         Dear " + currentUser.getFirstName() + " " + currentUser.getLastName() + ",\r\n         \r\n"
                + "         You just placed this order for the day " + order.getDay() + " \r\n        \r\n"
                + foodListTest
                + "            Total :  " + total + " euro \r\n        \r\n         "
                + "You can modify this order until " + globalSettingsrepo.findOne(1).getTimeLastOrder().toString("HH:mm:ss") + " on " + order.getDay().minusDays(1).toString("EEEE, d MMM yyyy") + "  by going to the link:\r\n        "
                + yumHostname + "/hungry/" + order.getDay().getYear() + "/" + order.getDay().getWeekOfWeekyear() + "/\r\n         \r\n         "
                + "Thank you for your order!";

        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmailAddress);
        message.setTo(currentUser.getEmail());
        message.setSubject("[Yum] About your order");
        //add a greeting sentence to the text:
        message.setText(text);
        mailSender.send(message);

    }

    public void sendResetPasswordLinkEmail(User user) {

        // send this email to the user that requested a password reset.
        // prepare the text like follow:
        String uuid = UUID.randomUUID().toString();
        user.setToken(uuid);

        java.util.Date date = new java.util.Date(System.currentTimeMillis() + 3600 * 12000);
        java.sql.Timestamp timestamp = new java.sql.Timestamp(date.getTime());
        user.setExpiration(timestamp);

        userrepo.save(user);

        String text = "         Dear " + user.getFirstName() + " " + user.getLastName() + ",\r\n         \r\n         "
                + "You just requested your password to be reset. "
                + "If that was not you, please discard this message.\r\n        \r\n "
                + "        To enter your new password, please visit this link:\r\n         "
                + yumHostname + "/changepassword/" + user.getToken() + "\r\n         \r\n         "
                + "\n         ~Remember that this link is active and valid only for the next 12 hours from now~ " + "\r\n         \r\n         "
                + "Thank you for using Yum!";
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmailAddress);

        message.setTo(user.getEmail());
        message.setSubject("[Yum] you asked for a password reset");
        //add a greeting sentence to the text:
        message.setText(text);
        mailSender.send(message);

    }
    
    public void sendApprovalEmail(User user) {

        // send this email to the user that was approved.
        // prepare the text like follow:

   

        String text = " Dear " + user.getFirstName() + " " + user.getLastName() + ",\r\n\n"
                + " You have just been approved. \r "
                + "Your are now able to order using Yum.\r\r "
                + yumHostname + "\r\r"
                + "Thank you for using Yum!";
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(senderEmailAddress);

        message.setTo(user.getEmail());
        message.setSubject("[Yum]");
        //add a greeting sentence to the text:
        message.setText(text);
        mailSender.send(message);

    }

}
