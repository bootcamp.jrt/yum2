/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import org.bootcamp.yum.api.service.MenusService;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.api.model.Menus;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-04T10:21:41.303+03:00")

@Controller
public class MenusApiController implements MenusApi {

    private MenusService menusService;

    @Autowired
    public MenusApiController(MenusService menusService) {
        this.menusService = menusService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Menus> menusMonthlyGet() throws ApiException
    {
        Menus menus = menusService.menusMonthlyGet();
        return new ResponseEntity<>(menus, HttpStatus.OK);
    }
    
    
    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Menus> menusDayDayDateGet(@ApiParam(value = "The requested day",required=true ) @PathVariable("dayDate") String dayDate) throws ApiException  {
        Menus menus = menusService.menusDayDayDateGet(dayDate);
        return new ResponseEntity<>(menus, HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Menus> menusMonthlyMonthOfYearGet
    (@ApiParam(value = "The requested month",required=true ) @PathVariable("monthOfYear") String monthOfYear) throws ApiException 
    {
        Menus menus = menusService.menusMonthlyMonthOfYearGet(monthOfYear);
        return new ResponseEntity<>(menus, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Menus> menusWeeklyGet() throws ApiException 
    {
        Menus menus = menusService.menusWeeklyGet();
        return new ResponseEntity<>(menus, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Menus> menusWeeklyWeekOfYearGet
    (@ApiParam(value = "The requested week",required=true ) @PathVariable("weekOfYear") String weekOfYear) throws ApiException 
    {
        Menus menus = menusService.menusWeeklyWeekOfYearGet(weekOfYear);
        return new ResponseEntity<>(menus, HttpStatus.OK);
    }
}
