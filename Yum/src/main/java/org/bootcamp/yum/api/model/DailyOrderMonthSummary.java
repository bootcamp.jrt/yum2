/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.LocalDate;

/**
 * DailyOrderMonthSummary
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T10:53:26.548+03:00")

public class DailyOrderMonthSummary   {
  @JsonProperty("day")
  private LocalDate day = null;

  @JsonProperty("menuid")
  private Integer menuid = null;

  @JsonProperty("foods")
  private List<DailyOrderMonthSummaryFoods> foods = new ArrayList<DailyOrderMonthSummaryFoods>();

  public DailyOrderMonthSummary day(LocalDate day) {
    this.day = day;
    return this;
  }

   /**
   * Get day
   * @return day
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public LocalDate getDay() {
    return day;
  }

  public void setDay(LocalDate day) {
    this.day = day;
  }

  public DailyOrderMonthSummary menuid(Integer menuid) {
    this.menuid = menuid;
    return this;
  }

   /**
   * Get menuid
   * @return menuid
  **/
  @ApiModelProperty(value = "")
  public Integer getMenuid() {
    return menuid;
  }

  public void setMenuid(Integer menuid) {
    this.menuid = menuid;
  }

  public DailyOrderMonthSummary foods(List<DailyOrderMonthSummaryFoods> foods) {
    this.foods = foods;
    return this;
  }

  public DailyOrderMonthSummary addFoodsItem(DailyOrderMonthSummaryFoods foodsItem) {
    this.foods.add(foodsItem);
    return this;
  }

   /**
   * Get foods
   * @return foods
  **/
  @ApiModelProperty(value = "")
  public List<DailyOrderMonthSummaryFoods> getFoods() {
    return foods;
  }

  public void setFoods(List<DailyOrderMonthSummaryFoods> foods) {
    this.foods = foods;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailyOrderMonthSummary dailyOrderMonthSummary = (DailyOrderMonthSummary) o;
    return Objects.equals(this.day, dailyOrderMonthSummary.day) &&
        Objects.equals(this.menuid, dailyOrderMonthSummary.menuid) &&
        Objects.equals(this.foods, dailyOrderMonthSummary.foods);
  }

  @Override
  public int hashCode() {
    return Objects.hash(day, menuid, foods);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailyOrderMonthSummary {\n");
    
    sb.append("    day: ").append(toIndentedString(day)).append("\n");
    sb.append("    menuid: ").append(toIndentedString(menuid)).append("\n");
    sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

