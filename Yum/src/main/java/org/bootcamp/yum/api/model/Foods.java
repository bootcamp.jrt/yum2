/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.List;

/**
 * Foods
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-11T12:33:29.635+03:00")

public class Foods   {
  @JsonProperty("version")
  private Integer version = null;

  @JsonProperty("totalNumber")
  private Integer totalNumber = null;

  @JsonProperty("foodList")
  private List<FoodsFoodList> foodList = new ArrayList<FoodsFoodList>();

  public Foods version(Integer version) {
    this.version = version;
    return this;
  }

   /**
   * Get version
   * @return version
  **/
  @ApiModelProperty(value = "")
  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public Foods totalNumber(Integer totalNumber) {
    this.totalNumber = totalNumber;
    return this;
  }

   /**
   * Get totalNumber
   * @return totalNumber
  **/
  @ApiModelProperty(value = "")
  public Integer getTotalNumber() {
    return totalNumber;
  }

  public void setTotalNumber(Integer totalNumber) {
    this.totalNumber = totalNumber;
  }

  public Foods foodList(List<FoodsFoodList> foodList) {
    this.foodList = foodList;
    return this;
  }

  public Foods addFoodListItem(FoodsFoodList foodListItem) {
    this.foodList.add(foodListItem);
    return this;
  }

   /**
   * Get foodList
   * @return foodList
  **/
  @ApiModelProperty(value = "")
  public List<FoodsFoodList> getFoodList() {
    return foodList;
  }

  public void setFoodList(List<FoodsFoodList> foodList) {
    this.foodList = foodList;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Foods foods = (Foods) o;
    return Objects.equals(this.version, foods.version) &&
        Objects.equals(this.totalNumber, foods.totalNumber) &&
        Objects.equals(this.foodList, foods.foodList);
  }

  @Override
  public int hashCode() {
    return Objects.hash(version, totalNumber, foodList);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Foods {\n");
    
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    totalNumber: ").append(toIndentedString(totalNumber)).append("\n");
    sb.append("    foodList: ").append(toIndentedString(foodList)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

