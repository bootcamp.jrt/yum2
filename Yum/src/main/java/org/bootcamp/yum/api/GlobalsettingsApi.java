/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.CrossOrigin;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.GlobalSettings;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Api(value = "globalsettings", description = "the globalsettings API")
@RequestMapping(value = "/api")
public interface GlobalsettingsApi {

    @ApiOperation(value = "Gets the global settings", notes = "Gets the global settings", response = GlobalSettings.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "global settings", response = GlobalSettings.class)})
    @RequestMapping(value = "/globalsettings",
            produces = {"application/json"},
            method = RequestMethod.GET)
    @CrossOrigin
    ResponseEntity<GlobalSettings> globalsettingsGet() throws ApiException;

    @ApiOperation(value = "Changes the global settings.", notes = "Changes the global settings.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The global settings changed succesfully.", response = GlobalSettings.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = GlobalSettings.class)
        ,
        @ApiResponse(code = 404, message = "The settings do not exist.", response = GlobalSettings.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = GlobalSettings.class)})
    @RequestMapping(value = "/globalsettings",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    @CrossOrigin
    ResponseEntity<GlobalSettings> globalsettingsPut(@ApiParam(value = "The updated settings.") @RequestBody GlobalSettings settings) throws ApiException, ConcModException;
}
