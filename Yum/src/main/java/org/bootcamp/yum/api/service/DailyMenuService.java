/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.joda.time.LocalDate;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.exceptions.ConcurrentCreationException;
import org.bootcamp.yum.exceptions.ConcurrentDeletionException;
import org.bootcamp.yum.api.model.DailyMenuFoods;
import org.bootcamp.yum.api.model.DailyMenuUpdateFoods;
import org.bootcamp.yum.api.model.DailyMenu;
import org.bootcamp.yum.api.model.DailyMenuUpdate;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.FoodRepository;


@Service
public class DailyMenuService {

    @Autowired
    private DailyMenuRepository dailyMenurepo;

    @Autowired
    private FoodRepository foodrepo;


    @Transactional
    public DailyMenuUpdate dailyMenusIdPut(int id, DailyMenuUpdate dailyMenuToUpdate) throws ApiException, ConcModException, ConcurrentDeletionException {

        org.bootcamp.yum.data.entity.DailyMenu requestedDailyMenu = dailyMenurepo.findByDailyMenuId(id);

        if (requestedDailyMenu == null) {
            org.bootcamp.yum.data.entity.DailyMenu dMenu = dailyMenurepo.findByDailyMenuDate(dailyMenuToUpdate.getDailyMenuDate());
            if (dMenu == null) {
                DailyMenuUpdate latestDMU = new DailyMenuUpdate();
                latestDMU.setDailyMenuId(0);
                throw new ConcurrentDeletionException(410, "You deleted this menu earlier. Please recreate it again if this is what was intended.", latestDMU);
            } else {
                DailyMenuUpdate latestDMU = new DailyMenuUpdate();
                latestDMU.setDailyMenuDate(dMenu.getDailyMenuDate());

                List<DailyMenuUpdateFoods> foodsUpdate = new ArrayList<>();
                for (Food aFood : (List<Food>) dMenu.getFoods()) {
                    DailyMenuUpdateFoods aFoodUpdate = new DailyMenuUpdateFoods();
                    aFoodUpdate.setFoodId(aFood.getId());
                    foodsUpdate.add(aFoodUpdate);

                }

                latestDMU.setFoods(foodsUpdate);
                latestDMU.setVersion(dMenu.getVersion());
                latestDMU.setDailyMenuId(dMenu.getDailyMenuId());
                throw new ConcurrentDeletionException(410, "You deleted this menu earlier, and recreated it. Here is the new one.", latestDMU);
            }
            //throw new ApiException(400, "Daily Menu not found");
            //THIS DATE IS for the DAILY MENU from the calendar !!! 
        } else if (dailyMenuToUpdate.getDailyMenuDate() == null) {
            throw new ApiException(400, "Bad request, didnt give date field");
            //try to update the db while inserting same foods
        } else if (dailyMenurepo.findByDailyMenuDate(dailyMenuToUpdate.getDailyMenuDate()) == null) {
            throw new ApiException(400, "Bad request, no such daily menu to update");
            //try to update a menu with bad id's 
        } else if (!everyFoodExistsInDBPut(dailyMenuToUpdate.getFoods())) {
            throw new ApiException(400, "Bad request, false foods id's or duplicates");
        } else if (dailyMenuToUpdate.getVersion() < requestedDailyMenu.getVersion()) {

            // org.bootcamp.yum.data.entity.DailyMenu requestedDailyMenu = dailyMenurepo.findByDailyMenuId(id);
            DailyMenuUpdate latestDMU = new DailyMenuUpdate();
            latestDMU.setDailyMenuDate(requestedDailyMenu.getDailyMenuDate());

            List<DailyMenuUpdateFoods> foodsUpdate = new ArrayList<>();
            for (Food aFood : (List<Food>) requestedDailyMenu.getFoods()) {
                DailyMenuUpdateFoods aFoodUpdate = new DailyMenuUpdateFoods();
                aFoodUpdate.setFoodId(aFood.getId());
                foodsUpdate.add(aFoodUpdate);

            }

            latestDMU.setFoods(foodsUpdate);
            latestDMU.setVersion(requestedDailyMenu.getVersion());
            throw new ConcModException(409, "Concurrent modification error.", latestDMU);

        } else if (dailyMenuToUpdate.getFoods().size() == 0) {
            dailyMenurepo.delete(requestedDailyMenu);
            DailyMenuUpdate dummyDMU = requestedDailyMenu.dailyMenu2DMUpdate();
            return dummyDMU;
        } else {

            List<Food> foodsToUpdate = new ArrayList<>();
            List<DailyMenuUpdateFoods> foodsRetrieved = dailyMenuToUpdate.getFoods();
            for (DailyMenuUpdateFoods dmFoods : foodsRetrieved) {
                try {
                    ///Check if the food item that is inserted, actually exists.
                    Food copiedFood = foodrepo.findById(dmFoods.getFoodId());
                    if (copiedFood != null) {
                        foodsToUpdate.add(copiedFood);
                    }
                } catch (Exception e) {
                    throw new ApiException(400, "Food item not found");
                }
            }

            DailyMenuFoods dummyFood = new DailyMenuFoods();
            for (Food aFood : (List<Food>) requestedDailyMenu.getFoods()) {
                dummyFood.setFoodId(aFood.getId());

                if (isOrdered(dummyFood, requestedDailyMenu.getDailyMenuDate()) && !foodsToUpdate.contains(aFood)) {
                    foodsToUpdate.add(aFood);
                }

            }

            requestedDailyMenu.setFoods(foodsToUpdate);

            dailyMenurepo.save(requestedDailyMenu);

            DailyMenuUpdate dummyDMU = requestedDailyMenu.dailyMenu2DMUpdate();
            // @Version Independently changes the DB version
            // We add the +1 in order have the right value in the DTO.
            // The version will NOT change before the end of the transaction
            // If you want to skip the +1 return an Entity and setup the DTO in the controller
            dummyDMU.setVersion(dailyMenuToUpdate.getVersion() + 1);

            return dummyDMU;
        }
    }


    public DailyMenu dailyMenusIdGet(int dmId) throws ApiException {

        org.bootcamp.yum.data.entity.DailyMenu requestedDailyMenu = dailyMenurepo.findByDailyMenuId(dmId);

        if (requestedDailyMenu == null) {
            throw new ApiException(400, "Daily Menu not found");
            //THIS DATE IS for the DAILY MENU from the calendar !!! 
        } else {

            DailyMenu dmToReturn = new DailyMenu();

            dmToReturn = requestedDailyMenu.dailyMenuToDTO();

            for (DailyMenuFoods f : dmToReturn.getFoods()) {
                f.setIsOrdered(isOrdered(f, dmToReturn.getDailyMenuDate()));
            }

            return dmToReturn;

        }
    }

 
    public List<DailyMenu> dailyMenusMonthlyGet() throws ApiException {

        LocalDate localDate = new LocalDate();
        List<DailyMenu> dailyMenus = new ArrayList<>();

        int iYear = localDate.getYear();
        int iMonth = localDate.getMonthOfYear();
        int iDay = 1;
        LocalDate localDate2 = new LocalDate().withYear(iYear).withMonthOfYear(iMonth).withDayOfMonth(1);
        Calendar mycal = new GregorianCalendar(iYear, iMonth - 1, iDay);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < daysInMonth; i++) {
            DailyMenu dmFromModel2 = new DailyMenu();
            LocalDate ld = localDate2.plusDays(i);
            dmFromModel2.setDailyMenuDate(ld);
            dailyMenus.add(dmFromModel2);
        }

        LocalDate start = localDate.dayOfMonth().withMinimumValue();
        LocalDate end = localDate.dayOfMonth().withMaximumValue();
        Iterable<org.bootcamp.yum.data.entity.DailyMenu> dm = dailyMenurepo.findByDailyMenuDateBetween(start, end);
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
        for (org.bootcamp.yum.data.entity.DailyMenu dMenu : dm) {

            if (dMenu.getDailyMenuDate().getMonthOfYear() == localDate.getMonthOfYear() && dMenu.getDailyMenuDate().getYear() == localDate.getYear()) {
                DailyMenu dmFromModel = new DailyMenu();
                List<Food> dmFoods = dMenu.getFoods();
                for (Food f : dmFoods) {
                    DailyMenuFoods dailyMenuFood = new DailyMenuFoods();
                    dailyMenuFood.setFoodId(f.getId());
                    dailyMenuFood.setFoodName(f.getName());
                    dailyMenuFood.setDescription(f.getDescription());
                    dailyMenuFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
                    dailyMenuFood.setIsOrdered(isOrdered(dailyMenuFood, dMenu.getDailyMenuDate()));
                    dmFromModel.addFoodsItem(dailyMenuFood);
                }
                dmFromModel.setDailyMenuId(dMenu.getDailyMenuId());
                dmFromModel.setDailyMenuDate(dMenu.getDailyMenuDate());
                dmFromModel.setVersion(dMenu.getVersion());
                dailyMenus.set(dmFromModel.getDailyMenuDate().getDayOfMonth() - 1, dmFromModel);
            }
        }
        return dailyMenus;
    }


    public List<DailyMenu> dailyMenusMonthlyMonthGet(String month) throws ApiException {

        List<DailyMenu> dailyMenus = new ArrayList<>();
        FoodTypeConverter foodTypeConverter = new FoodTypeConverter();

        if (month.matches("[0-9][0-9]?-[0-9]{4}")) {
            String[] monthAndYear = month.split("-");
            String monthNumber = monthAndYear[0];
            String yearNumber = monthAndYear[1];
            if (Integer.parseInt(monthNumber) < 13 && Integer.parseInt(monthNumber) > 0 && Integer.parseInt(yearNumber) > 1900) {

                LocalDate localDate2 = new LocalDate().withYear(Integer.parseInt(yearNumber)).withMonthOfYear(Integer.parseInt(monthNumber)).withDayOfMonth(1);
                Calendar mycal = new GregorianCalendar(Integer.parseInt(yearNumber), Integer.parseInt(monthNumber) - 1, 1);
                int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
                for (int i = 0; i < daysInMonth; i++) {
                    DailyMenu dmFromModel2 = new DailyMenu();
                    LocalDate ld = localDate2.plusDays(i);
                    dmFromModel2.setDailyMenuDate(ld);
                    dailyMenus.add(dmFromModel2);
                }

                LocalDate start = localDate2.dayOfMonth().withMinimumValue();
                LocalDate end = localDate2.dayOfMonth().withMaximumValue();
                Iterable<org.bootcamp.yum.data.entity.DailyMenu> dm = dailyMenurepo.findByDailyMenuDateBetween(start, end);
                for (org.bootcamp.yum.data.entity.DailyMenu dMenu : dm) {

                    if (dMenu.getDailyMenuDate().getMonthOfYear() == Integer.parseInt(monthNumber) && (dMenu.getDailyMenuDate().getYear() == Integer.parseInt(yearNumber))) {
                        DailyMenu dmFromModel = new DailyMenu();

                        List<Food> dmFoods = dMenu.getFoods();
                        for (Food f : dmFoods) {
                            DailyMenuFoods dailyMenuFood = new DailyMenuFoods();
                            dailyMenuFood.setFoodId(f.getId());
                            dailyMenuFood.setFoodName(f.getName());
                            dailyMenuFood.setDescription(f.getDescription());
                            dailyMenuFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
                            dailyMenuFood.setIsOrdered(isOrdered(dailyMenuFood, dMenu.getDailyMenuDate()));
                            dmFromModel.addFoodsItem(dailyMenuFood);
                        }
                        dmFromModel.setDailyMenuId(dMenu.getDailyMenuId());
                        dmFromModel.setDailyMenuDate(dMenu.getDailyMenuDate());
                        dmFromModel.setVersion(dMenu.getVersion());
                        //dailyMenus.add(dmFromModel);
                        dailyMenus.set(dmFromModel.getDailyMenuDate().getDayOfMonth() - 1, dmFromModel);

                    }

                }
            } else {
//                throw new ApiException(404, "Bad Request");
                throw new ApiException(400, "Bad Request");
            }
        } else {
//            throw new ApiException(404, "Bad Request");
            throw new ApiException(400, "Bad Request");
        }
        return dailyMenus;
    }

    @Transactional
    public DailyMenu dailyMenusPost(DailyMenu dailyMenu) throws ApiException, ConcurrentCreationException {

        //try to create an empty menu
        if (dailyMenu == null) {
            System.out.println("Bad request, daily menu is empty");
            throw new ApiException(400, "Bad request, daily menu is empty");
            //try to create a menu that already exists
        } else if (dailyMenurepo.findByDailyMenuDate(dailyMenu.getDailyMenuDate()) != null) {
            org.bootcamp.yum.data.entity.DailyMenu dMenu = dailyMenurepo.findByDailyMenuDate(dailyMenu.getDailyMenuDate());
            DailyMenu modelMenu = dMenu.dailyMenuToDTO();
            throw new ConcurrentCreationException(409, "A menu was already created for that day.", modelMenu);
            //try to create a menu with no foods inside
        } else if (dailyMenu.getFoods().size() < 1) {
            throw new ApiException(400, "Bad request, no food items found in the menu");
            //check for the foods that exists
        } else if (!everyFoodExistsInDBPost(dailyMenu.getFoods())) {
            throw new ApiException(400, "Bad request, no such food items found in DB");
        } else {

            org.bootcamp.yum.data.entity.DailyMenu dailyMenuToInsertDb = new org.bootcamp.yum.data.entity.DailyMenu();
            dailyMenuToInsertDb.setDailyMenuDate(dailyMenu.getDailyMenuDate());

            List<Food> foodsToStore = new ArrayList<>();
            List<DailyMenuFoods> foodsRetrieved = dailyMenu.getFoods();
            for (DailyMenuFoods dmFoods : foodsRetrieved) {
                try {
                    Food copiedFood = foodrepo.findById(dmFoods.getFoodId());
                    foodsToStore.add(copiedFood);
                    dmFoods.setFoodName(copiedFood.getName());
                } catch (Exception e) {

                }
            }
            dailyMenu.setFoods(foodsRetrieved);
            dailyMenuToInsertDb.setFoods(foodsToStore);
            dailyMenurepo.save(dailyMenuToInsertDb);
            dailyMenu.setDailyMenuId(dailyMenuToInsertDb.getDailyMenuId());
            return dailyMenu;
        }
    }

    private Boolean isOrdered(DailyMenuFoods dailyMenuFood, LocalDate dmDate) {
        Boolean isOrdered = false;
        //match the daily menu food to be edited with foods inside food repo 
        if (dailyMenuFood != null) {
            System.out.println("***" + dailyMenuFood);
            org.bootcamp.yum.data.entity.Food askedFoodItem = foodrepo.findById(dailyMenuFood.getFoodId());
            if (askedFoodItem != null) {
                List<org.bootcamp.yum.data.entity.DailyMenu> dailyMenus = askedFoodItem.getDailyMenus();
                if (dailyMenus != null) {
                    for (org.bootcamp.yum.data.entity.DailyMenu dailyMenu : dailyMenus) {
                        List<DailyOrder> dailyOrders = dailyMenu.getDailyOrders();
                        if (!dailyOrders.isEmpty()) {
                            for (DailyOrder dailyOrder : dailyOrders) {
                                List<OrderItem> orderItems = dailyOrder.getOrderItems();
                                //checking for same food item on a certain date ( as we iterate to show daily Menu of the month ) 
                                if (dailyOrder.getDailyMenu().getDailyMenuDate().equals(dmDate)) {
                                    for (OrderItem orderItem : orderItems) {
                                        if (orderItem.getFood().getId() == askedFoodItem.getId()) {
                                            isOrdered = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return isOrdered;

    }

    //For POst
    private Boolean everyFoodExistsInDBPost(List<DailyMenuFoods> foods) {
        Boolean exists = true;

        for (DailyMenuFoods food : foods) {
            if (foodrepo.findByIdAndArchived(food.getFoodId(), false) == null) {
                exists = false;
            }
        }

        //the following check is for unique foods ( 2 checks in 1 method ) 
        Set<DailyMenuFoods> set = new HashSet<DailyMenuFoods>(foods);
        if (set.size() < foods.size()) {
            exists = false;
        }

        return exists;
    }

    private Boolean everyFoodExistsInDBPut(List<DailyMenuUpdateFoods> foods) {
        Boolean exists = true;

        for (DailyMenuUpdateFoods food : foods) {
            if (foodrepo.findById(food.getFoodId()) == null) {
                exists = false;
            }
        }

        //the following check is for unique foods ( 2 checks in 1 method ) 
        Set<DailyMenuUpdateFoods> set = new HashSet<DailyMenuUpdateFoods>(foods);
        if (set.size() < foods.size()) {
            exists = false;
        }

        return exists;
    }

}
