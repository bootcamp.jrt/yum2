/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.model;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.joda.time.LocalDate;

/**
 * DailyOrder
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T19:36:30.840+03:00")

public class DailyOrder   {
  @JsonProperty("day")
  private LocalDate day = null;

  @JsonProperty("email")
  private Boolean email = null;

  @JsonProperty("id")
  private Integer id = null;

  @JsonProperty("dmId")
  private Integer dmId = null;

  @JsonProperty("version")
  private Integer version = null;

  @JsonProperty("foods")
  private List<DailyOrderFoods> foods = new ArrayList<DailyOrderFoods>();

  public DailyOrder day(LocalDate day) {
    this.day = day;
    return this;
  }

   /**
   * Get day
   * @return day
  **/
  @ApiModelProperty(required = true, value = "")
  @NotNull
  public LocalDate getDay() {
    return day;
  }

  public void setDay(LocalDate day) {
    this.day = day;
  }

  public DailyOrder email(Boolean email) {
    this.email = email;
    return this;
  }

   /**
   * Get email
   * @return email
  **/
  @ApiModelProperty(value = "")
  public Boolean getEmail() {
    return email;
  }

  public void setEmail(Boolean email) {
    this.email = email;
  }

  public DailyOrder id(Integer id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public DailyOrder dmId(Integer dmId) {
    this.dmId = dmId;
    return this;
  }

   /**
   * Get dmId
   * @return dmId
  **/
  @ApiModelProperty(value = "")
  public Integer getDmId() {
    return dmId;
  }

  public void setDmId(Integer dmId) {
    this.dmId = dmId;
  }

  public DailyOrder version(Integer version) {
    this.version = version;
    return this;
  }

   /**
   * Get version
   * @return version
  **/
  @ApiModelProperty(value = "")
  public Integer getVersion() {
    return version;
  }

  public void setVersion(Integer version) {
    this.version = version;
  }

  public DailyOrder foods(List<DailyOrderFoods> foods) {
    this.foods = foods;
    return this;
  }

  public DailyOrder addFoodsItem(DailyOrderFoods foodsItem) {
    this.foods.add(foodsItem);
    return this;
  }

   /**
   * Get foods
   * @return foods
  **/
  @ApiModelProperty(value = "")
  public List<DailyOrderFoods> getFoods() {
    return foods;
  }

  public void setFoods(List<DailyOrderFoods> foods) {
    this.foods = foods;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DailyOrder dailyOrder = (DailyOrder) o;
    return Objects.equals(this.day, dailyOrder.day) &&
        Objects.equals(this.email, dailyOrder.email) &&
        Objects.equals(this.id, dailyOrder.id) &&
        Objects.equals(this.dmId, dailyOrder.dmId) &&
        Objects.equals(this.version, dailyOrder.version) &&
        Objects.equals(this.foods, dailyOrder.foods);
  }

  @Override
  public int hashCode() {
    return Objects.hash(day, email, id, dmId, version, foods);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class DailyOrder {\n");
    
    sb.append("    day: ").append(toIndentedString(day)).append("\n");
    sb.append("    email: ").append(toIndentedString(email)).append("\n");
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    dmId: ").append(toIndentedString(dmId)).append("\n");
    sb.append("    version: ").append(toIndentedString(version)).append("\n");
    sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

