/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import io.swagger.annotations.*;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.api.model.Login;
import org.bootcamp.yum.api.model.Body;
import org.bootcamp.yum.api.model.LoggedUserInfo;
import org.bootcamp.yum.api.model.Password;
import org.bootcamp.yum.api.model.Register;
import org.bootcamp.yum.api.model.TermsPolicy;
import org.bootcamp.yum.api.service.AuthorizeService;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-05T12:53:38.491+03:00")

@Controller
public class AuthApiController implements AuthApi {


    private AuthorizeService authService;

    @Autowired
    public AuthApiController(AuthorizeService authService) {
        this.authService = authService;
    }



    public ResponseEntity<Void> authChangepwdSecretPut(@ApiParam(value = "The user to update." ,required=true ) @RequestBody Password password,
        @ApiParam(value = "The secret send from email.",required=true ) @PathVariable("secret") String secret) throws ApiException {
        // do some magic!
        authService.authChangepwdSecretPut(password,secret);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    public ResponseEntity<LoggedUserInfo> authLoginPost
    (@ApiParam(value = "The email/password", required = true) @RequestBody Login body) throws ApiException {
        LoggedUserInfo userInfo = authService.authLoginPost(body);
            return new ResponseEntity<>(userInfo, HttpStatus.OK);
    }

    public ResponseEntity<Void> authForgotpwdPost
    (@ApiParam(value = "The user that wants to retrieve his password.", required = true) @RequestBody Body body) throws ApiException {
        authService.authForgotpwdPost(body);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    public ResponseEntity<Void> authRegisterPost
    (@ApiParam(value = "The user to be registered", required = true) @RequestBody Register body) throws ApiException {
        authService.authRegisterPost(body);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    
    @Override
    public ResponseEntity<TermsPolicy> authTermsGet() throws ApiException {
        // do some magic!
        TermsPolicy termsAndPolicy = authService.authTermsGet();
        return new ResponseEntity<>(termsAndPolicy, HttpStatus.OK);
    } 
}

