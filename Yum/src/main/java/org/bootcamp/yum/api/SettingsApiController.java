/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;


import io.swagger.annotations.*;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

import java.io.ByteArrayInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.OptimisticLockException;
import javax.validation.constraints.NotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;

import org.bootcamp.JwtCodec;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.api.service.SettingsService;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.api.model.UserGet;
import org.bootcamp.yum.api.model.UserPut;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Controller
public class SettingsApiController implements SettingsApi 
{
    private SettingsService settingsService;

    @Autowired
    public SettingsApiController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }
    
    @Autowired
    private UserRepository userrepo;

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<UserGet> settingsGet() throws ApiException 
    {
        UserGet userGet = settingsService.settingsGet();
        return new ResponseEntity<>(userGet, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<Void> settingsPut( 
    @ApiParam(value = "The user to update."  ) @RequestBody UserPut userToUpdate) throws ApiException , ConcModException, Exception 
    {
        try
        {
            settingsService.settingsPut(userToUpdate);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);            
        }
        catch(OptimisticLockException ex)
        {
            try 
            {
               UserGet user = settingsService.settingsGet();
               throw new ConcModException(user);
            } 
            catch (ApiException ex1) 
            {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }        
    }
        
    @PreAuthorize("hasAuthority('hungry')") 
    @Override
    public ResponseEntity<Void> settingsPicturePost(@ApiParam(value = "file detail") @RequestPart("file") MultipartFile file) {
        settingsService.postImage(file);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
    
    @PreAuthorize("hasAuthority('hungry')") 
    @Override
    public ResponseEntity<Void> settingsPictureDelete() {
        settingsService.deleteImage();
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Object> settingsPictureTokenGet( @NotNull @ApiParam(value = "The token of the current session.", required = true) 
        @RequestParam(value = "token", required = true) String token) throws ApiException {

        Claims claims;
        
        try{
           claims = JwtCodec.decode(token);  
        } catch(JwtException e){
             return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        
        long userId = Long.parseLong(claims.getSubject());
        
        // Get the id of the user and get the image(bytes) from the table users(Model/DB).
        User loggedUser = userrepo.findById((int) userId);
        
        byte[] bytes = loggedUser.getPicture();
        
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        
        return ResponseEntity.ok()
            .contentLength(bytes.length)
            .contentType(MediaType.parseMediaType("image/jpeg"))
            .body(new InputStreamResource(inputStream));
    } 
}
