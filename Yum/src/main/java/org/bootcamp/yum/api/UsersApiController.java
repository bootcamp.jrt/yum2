/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

import javax.validation.constraints.*;
import io.swagger.annotations.*;
import java.io.ByteArrayInputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.OptimisticLockException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.UserGetId;
import org.bootcamp.yum.api.model.UserIdPutByAdmin;
import org.bootcamp.yum.api.model.UserPut;
import org.bootcamp.yum.api.model.AllUsers;
import org.bootcamp.yum.api.model.Approval;
import org.bootcamp.yum.api.service.UserService;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.JwtCodec;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Controller
public class UsersApiController implements UsersApi {

    private UserService userService;

    @Autowired
    public UsersApiController(UserService userService) {
        this.userService = userService;
    }
    
    @Autowired
    private UserRepository userrepo;

    public boolean checkRole(String email) {
        if (email.equals("hungry") || email.equals("chef") || email.equals("admin")) {
            return true;
        } else {
            return false;
        }
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<AllUsers> usersGet(@NotNull @ApiParam(value = "Sort the list of users.", required = true, defaultValue = "date") @RequestParam(value = "sorting", required = true, defaultValue = "date") String sorting,
            @NotNull @ApiParam(value = "Show the users on the selected page.", required = true, defaultValue = "0") @RequestParam(value = "page", required = true, defaultValue = "0") Integer page,
            @NotNull @ApiParam(value = "Show the selected amount of users per page.", required = true, defaultValue = "10") @RequestParam(value = "pagesize", required = true, defaultValue = "10") Integer pagesize,
            @NotNull @ApiParam(value = "Select the ordering.", required = true, defaultValue = "asc") @RequestParam(value = "order", required = true, defaultValue = "asc") String order) throws ApiException {

        AllUsers allUsers = userService.usersGet(sorting, page, pagesize, order);
        return new ResponseEntity<>(allUsers, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdDelete(@ApiParam(value = "The id of the user to delete", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "Delete user's future orders.", defaultValue = "false") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force) throws ApiException {
        userService.usersIdDelete(id, force);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdForgotpwdPost(@ApiParam(value = "The id of the user.", required = true) @PathVariable("id") Integer id) throws ApiException {

        userService.usersIdForgotpwdPost(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<UserGetId> usersIdGet(@ApiParam(value = "The user's id that you want to search.", required = true) @PathVariable("id") Integer id) throws ApiException {

        UserGetId userModel = userService.usersIdGet(id);
        return new ResponseEntity<>(userModel, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<UserIdPutByAdmin> usersIdPut(@ApiParam(value = "The id of the food item to update", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "The user to update.") @RequestBody UserIdPutByAdmin userToUpdate) throws ApiException, Exception, ConcModException {

        try {
            userService.usersIdPut(id, userToUpdate);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            try {
                UserGetId userLatest = userService.usersIdGet(id);
                throw new ConcModException(409, "Concurrent modification error.", userLatest);
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersPost(@ApiParam(value = "The user to create.") @RequestBody UserPut user) throws ApiException, Exception {

        userService.usersPost(user);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdApprovePut(@ApiParam(value = "The id of the user.", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "User approval.")
            @RequestBody Approval approval,
            @ApiParam(value = "Delete user's future orders, and set user to dissaproved.", defaultValue = "false")
            @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force
    ) throws ApiException, ConcModException {
        try {
            userService.usersIdApprovePut(id, approval, force);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            try {
                UserGetId userLatest = userService.usersIdGet(id);
                throw new ConcModException(409, "Concurrent modification error.", userLatest);
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }

    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdPicturePost(@ApiParam(value = "The id of the user.", required = true) @PathVariable("id") Integer id,
            @ApiParam(value = "file detail") @RequestPart("file") MultipartFile upfile) throws ApiException {
        userService.postImageById(upfile,id);
        return new ResponseEntity<>(HttpStatus.OK);
    }    
    
    
    @Override
    public ResponseEntity<Object> usersIdPictureTokenGet(@ApiParam(value = "The requested user id.",required=true ) @PathVariable("id") Long id,
         @NotNull @ApiParam(value = "The admin's token.", required = true) @RequestParam(value = "token", required = true) String token) throws ApiException  {
        
        Claims claims;
        try{
            // We take a list of roles.
            claims = JwtCodec.decode(token);  
            
            // We iterate the list and check if exists the role='admin'.
            // If Yes we are ok, if No then exception
            List<String> roles = (List<String>)claims.get("roles");
            boolean isAdmin = false;
            for(String currentRole : roles){
                if(currentRole.equals("admin")){
                    isAdmin = true;
                }
            }   
            
            if(!isAdmin){
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }

        } catch(JwtException e){
             return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        // Get the id of the user and get the image(bytes) from the table users(Model/DB).
        User loggedUser = userrepo.findById(id.intValue());
        
        byte[] bytes = loggedUser.getPicture();
        
        ByteArrayInputStream inputStream = new ByteArrayInputStream(bytes);
        
        return ResponseEntity.ok()
            .contentLength(bytes.length)
            .contentType(MediaType.parseMediaType("image/jpeg"))
            .body(new InputStreamResource(inputStream));
    }
    

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdPictureDelete(@ApiParam(value = "The id of the user to delete", required = true)
            @PathVariable("id") Integer id) throws ApiException {
        userService.deleteImageById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
