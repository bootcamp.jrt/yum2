/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import static java.lang.Math.toIntExact;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcurrentCreationException;
import org.bootcamp.yum.exceptions.ConcurrentDeletionException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.DailyOrder;
import org.bootcamp.yum.api.model.DailyOrderFoods;
import org.bootcamp.yum.api.model.DailyOrderMonthSummary;
import org.bootcamp.yum.api.model.DailyOrderMonthSummaryFoods;
import org.bootcamp.yum.api.model.DailySummary;
import org.bootcamp.yum.api.model.DailySummaryFoods;
import org.bootcamp.yum.api.model.DailySummaryOrderedFoodList;
import org.bootcamp.yum.api.model.DailySummaryUserList;
import org.bootcamp.yum.api.model.Menus;
import org.bootcamp.yum.api.model.MenusInner;
import org.bootcamp.yum.api.model.MenusInnerFoods;
import org.bootcamp.yum.data.converter.FoodTypeConverter;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.GlobalSettings;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.GlobalSettingsRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class OrdersService {

    @Autowired
    private DailyOrderRepository dailyOrderrepo;
    @Autowired
    private DailyMenuRepository dailyMenurepo;
    @Autowired
    private UserRepository userrepo;
    @Autowired
    private FoodRepository foodrepo;
    @Autowired
    private GlobalSettingsRepository globalSettingsrepo;
    @Autowired
    private OrderItemRepository orderItemrepo;
    @Autowired
    private MailingService mailingService;

    @Transactional
    public DailyOrder ordersPost(DailyOrder dailyOrder) throws ApiException, ConcurrentCreationException {
        // The daily order that will be created
        org.bootcamp.yum.data.entity.DailyOrder myDailyOrder = new org.bootcamp.yum.data.entity.DailyOrder();

        // The actual logged in user
        User loggedUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));

        LocalDate orderDay = dailyOrder.getDay();

        DailyMenu aDailyMenu = dailyMenurepo.findByDailyMenuDate(orderDay);

        // Check if the Daily Menu exists.
        if (aDailyMenu == null) {
            throw new ApiException(404, "No daily menu for that day.");
        } else if (!quantitiesOk(dailyOrder)) {
            throw new ApiException(413, "Bad request, invalid order.");
        } else if (dailyOrder.getFoods().isEmpty()) {
            throw new ApiException(400, "Bad request, no foods in order.");
        } else {

            List<org.bootcamp.yum.data.entity.DailyOrder> dailyOrdersByUser = dailyOrderrepo.findByUserIdAndDailyMenuId(loggedUser.getId(), aDailyMenu.getDailyMenuId());
            Iterable<org.bootcamp.yum.data.entity.GlobalSettings> theGlobalSettings = globalSettingsrepo.findAll();

            // Check if the order dealine for that day has passed.
            LocalTime timeDeadline = theGlobalSettings.iterator().next().getTimeLastOrder();
            if (!org.bootcamp.yum.data.entity.DailyOrder.checkTimeOfOrder(timeDeadline, orderDay)) {
                throw new ApiException(400, "Deadline has passed.");
            }
            // Check if that user has already ordered for the day
            if (dailyOrdersByUser.size() > 0) {
                //Returning the latest dailymenu with the proper orders for the user.
                throw new ConcurrentCreationException(410, "You had already ordered for this day", getMenuFromDate(aDailyMenu));

            }

            myDailyOrder.setDailyMenu(aDailyMenu);
            // For mapping purposes
            myDailyOrder.setUser(loggedUser);
            // For DTO purposes
            myDailyOrder.setUserId(loggedUser.getId());

            // For each food in the arguments' list...
            for (DailyOrderFoods dof : dailyOrder.getFoods()) {
                org.bootcamp.yum.data.entity.OrderItem orderItem = new org.bootcamp.yum.data.entity.OrderItem();
                Food foodToEnterIntoOrder = foodrepo.findOne(dof.getFoodId());
                // ...Check if that food exists,
                if (foodToEnterIntoOrder == null) {
                    throw new ApiException(400, "Food not found.");
                }
                List<Food> foodsInsideDailyMenu = aDailyMenu.getFoods();
                boolean foodFound = false;
                // ...Check if the food in the DailyMenu's list
                for (Iterator<Food> iterator = foodsInsideDailyMenu.iterator(); iterator.hasNext();) {
                    Food foodInsideDailyMenu = iterator.next();
                    if (foodInsideDailyMenu.getId() == foodToEnterIntoOrder.getId()) {
                        orderItem.setFood(foodToEnterIntoOrder);
                        orderItem.setQuantity(dof.getQuantity());
                        orderItem.setDailyOrder(myDailyOrder);
                        foodFound = true;
                    }
                }
                // If any food in the API's arguments is not found in the DailyMenu return 400
                if (foodFound == false) {
                    throw new ApiException(400, "Food not in menu.");
                }
                myDailyOrder.addOrderItem(orderItem);
            }

            if (dailyOrder.getEmail()) {
                mailingService.sendConfirmOrderEmailToHungry(dailyOrder);
            }
            dailyOrderrepo.save(myDailyOrder);
            // Transform DailyOrder DAO to DTO
            DailyOrder dailyOrderDTO = myDailyOrder.dailyOrderDAO2DTO();
            // Email boolean does not persist, we take the value from input DTO
            dailyOrderDTO.setEmail(dailyOrder.getEmail());

            return dailyOrderDTO;
        }
    }

    public List<DailyOrderMonthSummary> ordersMonthlyGet() throws ApiException {
        LocalDate localDate = new LocalDate();
        List<DailyOrderMonthSummary> monthlyDailyOrders = new ArrayList<>();
        int iYear = localDate.getYear();
        int iMonth = localDate.getMonthOfYear();
        int iDay = 1;
        LocalDate localDate2 = new LocalDate().withYear(iYear).withMonthOfYear(iMonth).withDayOfMonth(1);
        Calendar mycal = new GregorianCalendar(iYear, iMonth - 1, iDay);
        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
        for (int i = 0; i < daysInMonth; i++) {
            LocalDate ld = localDate2.plusDays(i);
            DailyOrderMonthSummary monthlyDailyOrderFoods = new DailyOrderMonthSummary();
            monthlyDailyOrderFoods.setDay(ld);
            DailyMenu dailyMenu = dailyMenurepo.findByDailyMenuDate(ld);
            if (dailyMenu != null) {
                HashMap<Integer, DailyOrderMonthSummaryFoods> dofmap = new HashMap<>();
                for (Food food : dailyMenu.getFoods()) {
                    DailyOrderMonthSummaryFoods dof = new DailyOrderMonthSummaryFoods();
                    dof.setFood(food.foodToFoodItem());
                    dof.setQuantity(0);
                    dofmap.put(food.getId(), dof);
                }
                for (org.bootcamp.yum.data.entity.DailyOrder dailyOrder : dailyMenu.getDailyOrders()) {
                    for (OrderItem orderItem : dailyOrder.getOrderItems()) {
                        DailyOrderMonthSummaryFoods f = dofmap.get(orderItem.getMemberKey().getFoodId());
                        f.setQuantity(f.getQuantity() + orderItem.getQuantity());
                    }
                }
                List<DailyOrderMonthSummaryFoods> foods = new ArrayList<>();
                for (DailyOrderMonthSummaryFoods dof : dofmap.values()) {
                    if (dof.getQuantity() > 0) {
                        foods.add(dof);
                    }
                }
                monthlyDailyOrderFoods.setMenuid(dailyMenu.getDailyMenuId());
                monthlyDailyOrderFoods.setFoods(foods);
            }
            monthlyDailyOrders.add(monthlyDailyOrderFoods);
        }
        return monthlyDailyOrders;
    }

    public List<DailyOrderMonthSummary> ordersMonthlyMonthGet(String month) throws ApiException {
        List<DailyOrderMonthSummary> monthlyDailyOrders = new ArrayList<DailyOrderMonthSummary>();
        if (month.matches("[0-9][0-9]?-[0-9]{4}")) {
            String[] monthAndYear = month.split("-");
            String currentMonth = monthAndYear[0];
            String year = monthAndYear[1];
            if (Integer.parseInt(currentMonth) < 13 && Integer.parseInt(currentMonth) > 0 && Integer.parseInt(year) > 1900) {
                LocalDate localDate = new LocalDate().withYear(Integer.parseInt(year)).withMonthOfYear(Integer.parseInt(currentMonth));
                int iYear = localDate.getYear();
                int iMonth = localDate.getMonthOfYear();
                int iDay = 1;
                LocalDate localDate2 = new LocalDate().withYear(iYear).withMonthOfYear(iMonth).withDayOfMonth(1);
                Calendar mycal = new GregorianCalendar(iYear, iMonth - 1, iDay);
                int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH);
                for (int i = 0; i < daysInMonth; i++) {
                    LocalDate ld = localDate2.plusDays(i);
                    DailyOrderMonthSummary monthlyDailyOrderFoods = new DailyOrderMonthSummary();
                    monthlyDailyOrderFoods.setDay(ld);
                    DailyMenu dailyMenu = dailyMenurepo.findByDailyMenuDate(ld);
                    if (dailyMenu != null) {
                        HashMap<Integer, DailyOrderMonthSummaryFoods> dofmap = new HashMap<>();
                        for (Food food : dailyMenu.getFoods()) {
                            DailyOrderMonthSummaryFoods dof = new DailyOrderMonthSummaryFoods();
                            dof.setFood(food.foodToFoodItem());
                            dof.setQuantity(0);
                            dofmap.put(food.getId(), dof);
                        }
                        for (org.bootcamp.yum.data.entity.DailyOrder dailyOrder : dailyMenu.getDailyOrders()) {
                            for (OrderItem orderItem : dailyOrder.getOrderItems()) {
                                DailyOrderMonthSummaryFoods f = dofmap.get(orderItem.getMemberKey().getFoodId());
                                f.setQuantity(f.getQuantity() + orderItem.getQuantity());
                            }
                        }
                        List<DailyOrderMonthSummaryFoods> foods = new ArrayList<>();
                        for (DailyOrderMonthSummaryFoods dof : dofmap.values()) {
                            if (dof.getQuantity() > 0) {
                                foods.add(dof);
                            }
                        }
                        monthlyDailyOrderFoods.setMenuid(dailyMenu.getDailyMenuId());
                        monthlyDailyOrderFoods.setFoods(foods);
                    }
                    monthlyDailyOrders.add(monthlyDailyOrderFoods);
                }
            } else {
                throw new ApiException(400, "Wrong input.");
            }
        } else {
            throw new ApiException(400, "Wrong input.");
        }
        return monthlyDailyOrders;
    }

    public DailySummary ordersDailyDayGet(Integer day) throws ApiException {
        DailySummary dailySummaryToReturn = new DailySummary();
        List<DailySummaryOrderedFoodList> orderedFoods = new ArrayList<>();
        List<DailySummaryUserList> userOrders = new ArrayList<>();

        // Get all the DailyOrders associated with that DailyMenu id.
        List<org.bootcamp.yum.data.entity.DailyOrder> dailyOrdersOfThatMenu = (List<org.bootcamp.yum.data.entity.DailyOrder>) dailyOrderrepo.findByDailyMenuId(day);
        
        // Check if that DailyMenu ID exists 
        if (dailyOrdersOfThatMenu == null) {
            throw new ApiException(400, "Daily Menu not found.");
        }

        // Iterate through all the DailyOrders returned from the DB.
        for (Iterator<org.bootcamp.yum.data.entity.DailyOrder> iterator = dailyOrdersOfThatMenu.iterator(); iterator.hasNext();) {
            org.bootcamp.yum.data.entity.DailyOrder aDailyOrderOfThatMenu = iterator.next();
            
            // Here we put the date that exists in the menu
            dailySummaryToReturn.setDate(aDailyOrderOfThatMenu.getDailyMenu().getDailyMenuDate());
            
            // Get the details from the person that made the order.
            DailySummaryUserList aUserThatOrdered = new DailySummaryUserList();
            aUserThatOrdered.setFirstName(aDailyOrderOfThatMenu.getUser().getFirstName());
            aUserThatOrdered.setLastName(aDailyOrderOfThatMenu.getUser().getLastName());
            List<DailySummaryFoods> userOrderItems = new ArrayList<>();

            // Iterate through all the OrderItems in that Order.
            List<OrderItem> orderItemsFromOrder = aDailyOrderOfThatMenu.getOrderItems();
            for (Iterator<OrderItem> iterator2 = orderItemsFromOrder.iterator(); iterator2.hasNext();) {
                boolean foodAlreadyInList = false;
                OrderItem anOrderItemFromThatOrder = iterator2.next();

                // Put the pair (Food, Quantity) that is inside the OrderItem, to the person object.
                DailySummaryFoods userOrderItem = new DailySummaryFoods();
                userOrderItem.setFood(anOrderItemFromThatOrder.getFood().foodToFoodItem());
                userOrderItem.setQuantity(anOrderItemFromThatOrder.getQuantity());
                userOrderItems.add(userOrderItem);

                // Iterate through the list of ordered foods.
                int orderedFoodsIndex = 0;
                for (Iterator<DailySummaryOrderedFoodList> iterator3 = orderedFoods.iterator(); iterator3.hasNext();) {
                    DailySummaryOrderedFoodList anOrderedFoodAlreadyIn = iterator3.next();
                    // Check if that food is already in the list of ordered foods.
                    if (anOrderedFoodAlreadyIn.getFoods().getFoodName().equals(anOrderItemFromThatOrder.getFood().getName())) {
                        foodAlreadyInList = true;
                        break;
                    }
                    orderedFoodsIndex++;
                }
                // If the food is already in the list, update its quantity
                if (foodAlreadyInList) {
                    int updatedQuantity = orderedFoods.get(orderedFoodsIndex).getQuantity() + anOrderItemFromThatOrder.getQuantity();
                    orderedFoods.get(orderedFoodsIndex).setQuantity(updatedQuantity);
                } // If not, put it in the list and initialize the quantity.
                else {
                    DailySummaryOrderedFoodList dsoFoodToAdd = new DailySummaryOrderedFoodList();
                    dsoFoodToAdd.setFoods(anOrderItemFromThatOrder.getFood().foodToFoodItem());
                    dsoFoodToAdd.setQuantity(anOrderItemFromThatOrder.getQuantity());
                    orderedFoods.add(dsoFoodToAdd);
                }
            }
            aUserThatOrdered.setFoods(userOrderItems);
            userOrders.add(aUserThatOrdered);
        }
        dailySummaryToReturn.setOrderedFoodList(orderedFoods);
        dailySummaryToReturn.setUserList(userOrders);
        return dailySummaryToReturn;
    }

    public void ordersIdDelete(Integer id, DailyOrder dmVersion) throws ApiException, ConcurrentDeletionException {
        org.bootcamp.yum.data.entity.DailyOrder dailyOrderTaken = dailyOrderrepo.findById(id);
        org.bootcamp.yum.data.entity.DailyMenu dMenuOnThisDate = dailyMenurepo.findByDailyMenuDate(dmVersion.getDay());
        LocalDate today = LocalDate.now().plusDays(1);

        if (dailyOrderTaken == null && dMenuOnThisDate == null) {
            //order deleted earlier, daily menu as well. 
            //Catching this in front end, empties the daily menu ~ Stay Hungry
            throw new ApiException(404, "Order not found.");
        } else if (dailyOrderTaken == null && dMenuOnThisDate != null) {
            //order deleted earlier, daily menu exists and we need to send it back.           
            throw new ConcurrentDeletionException(410, "You cancelled this order earlier, and the menu has changed", getMenuFromDate(dMenuOnThisDate));


        } else if (dmVersion.getVersion() < dailyOrderTaken.getVersion()) {

            //customer is watching an older version, we must send him the correct one.           
            throw new ConcurrentDeletionException(410, "Daily order was dirty, but now updated", getMenuFromDate(dMenuOnThisDate));
        } else if (dailyOrderTaken.getDailyMenu().getDailyMenuDate().isBefore(today)) {
            throw new ApiException(400, "Bad request to delete.");
        } else {
            dailyOrderrepo.delete(dailyOrderTaken);
        }
    }

    public DailyOrder ordersIdGet(Integer id) throws ApiException {
        org.bootcamp.yum.data.entity.DailyOrder dailyOrderTaken = dailyOrderrepo.findById(id);
        if (dailyOrderTaken == null) {
            throw new ApiException(400, "Order not found.");
        } else {
            DailyOrder dailyOrder = new DailyOrder();
            dailyOrder.setDay(dailyOrderTaken.getDailyMenu().getDailyMenuDate());
            List<OrderItem> orderItemList = dailyOrderTaken.getOrderItems();

            for (OrderItem orderItem : orderItemList) {
                DailyOrderFoods food = new DailyOrderFoods();
                food.setFoodId(orderItem.getFood().getId());
                food.setQuantity(orderItem.getQuantity());
                dailyOrder.addFoodsItem(food);
            }
            dailyOrder.setVersion(dailyOrderTaken.getVersion());
            return dailyOrder;
        }
    }

    @Transactional
    public DailyOrder ordersIdPut(Integer id, DailyOrder dailyOrderToUpdate) throws ApiException, ConcModException {
        // The actual logged in user
        User loggedUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));

        org.bootcamp.yum.data.entity.DailyOrder dailyOrderToBeUpdated = dailyOrderrepo.findOne(id);
        if (dailyOrderToBeUpdated == null) {
            throw new ApiException(404, "Order not found.");
        } else if (!quantitiesOk(dailyOrderToUpdate)) {
            throw new ApiException(413, "Bad request, invalid order.");
        }
        List<OrderItem> orderItemsToBeUpdated = (List<OrderItem>) dailyOrderToBeUpdated.getOrderItems();
        dailyOrderToBeUpdated.getOrderItems();

        // Variables to check if the deadline has passed.
        GlobalSettings theGlobalSettings = globalSettingsrepo.findOne(1);
        LocalTime timeDeadline = theGlobalSettings.getTimeLastOrder();

        // Variables to check the lists of foods.
        LocalDate orderDay = dailyOrderToUpdate.getDay();
        DailyMenu dailyMenuOfThatDay = dailyMenurepo.findByDailyMenuDate(orderDay);
        if (dailyMenuOfThatDay == null) {
            //return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            throw new ApiException(404, "Daily Menu not found.");
        }
        List<DailyOrderFoods> foodsInsideArgument = dailyOrderToUpdate.getFoods();
        List<Food> foodsInsideDailyMenu = dailyMenuOfThatDay.getFoods();

        // Check if the Daily Order exists.
        // Check if the Daily Menu exists.
        if ((dailyOrderToUpdate.getVersion() < dailyOrderToBeUpdated.getVersion())) {
            org.bootcamp.yum.data.entity.DailyOrder latestDailyOrderToBeUpdated = dailyOrderrepo.findOne(id);
            DailyOrder newestDailyOrder = new DailyOrder();
            latestDailyOrderToBeUpdated.getOrderItems();
            List<DailyOrderFoods> dailyOrderFoodss = new ArrayList<>();
            // Iterate all the order items and add them to the return object
            for (OrderItem anOrderItem : orderItemsToBeUpdated) {
                DailyOrderFoods aDailyOrderFood = new DailyOrderFoods();
                aDailyOrderFood.setFoodId(anOrderItem.getFood().getId());
                aDailyOrderFood.setQuantity(anOrderItem.getQuantity());
                dailyOrderFoodss.add(aDailyOrderFood);
            }
            newestDailyOrder.setFoods(dailyOrderFoodss);
            newestDailyOrder.setDay(latestDailyOrderToBeUpdated.getDailyMenu().getDailyMenuDate());
            newestDailyOrder.setVersion(latestDailyOrderToBeUpdated.getVersion());

            throw new ConcModException(409, "Concurrent modification error.", newestDailyOrder);
        } // Check if the order dealine for that day has passed.
        else if (!org.bootcamp.yum.data.entity.DailyOrder.checkTimeOfOrder(timeDeadline, dailyOrderToUpdate.getDay())) {
            throw new ApiException(400, "Deadline has passed.");
        } // Check if all foods in input are available in the daily menu  
        else {
            // For every OrderItem that already exists in the order
            for (Iterator<OrderItem> iterator = orderItemsToBeUpdated.iterator(); iterator.hasNext();) {
                boolean orderItemFound = false;
                OrderItem anOrderItemToBeUpdated = iterator.next();
                // For every item in the API's input...
                for (Iterator<DailyOrderFoods> iterator2 = foodsInsideArgument.iterator(); iterator2.hasNext();) {
                    DailyOrderFoods aFoodInsideArgument = iterator2.next();
                    Food foodToEnterIntoOrder = foodrepo.findOne(aFoodInsideArgument.getFoodId());
                    // If the food is already in the order adjust the quantity
                    if (foodToEnterIntoOrder.getId() == anOrderItemToBeUpdated.getFood().getId()) {
                        anOrderItemToBeUpdated.setQuantity(aFoodInsideArgument.getQuantity());
                        orderItemFound = true;
                        iterator2.remove();
                        break;
                    }
                }
                // If the order item is not matched with a food delete it.
                if (orderItemFound == false) {
                    iterator.remove();
                    orderItemrepo.delete(anOrderItemToBeUpdated);
                }
            }
            // For every food in the order list that wasn't matched add it to the order.
            for (Iterator<DailyOrderFoods> iterator3 = foodsInsideArgument.iterator(); iterator3.hasNext();) {
                boolean foodFound = false;
                DailyOrderFoods aFoodThatRemained = iterator3.next();
                Food foodToEnterIntoOrder = foodrepo.findOne(aFoodThatRemained.getFoodId());
                // ...Check if that food exists,
                if (foodToEnterIntoOrder == null) {
                    throw new ApiException(400, "Food not found.");
                    //return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                }
                // Check if the food is in the DailyMenu's list
                for (Iterator<Food> iterator4 = foodsInsideDailyMenu.iterator(); iterator4.hasNext();) {
                    Food afoodInsideDailyMenu = iterator4.next();
                    // If it is, add it to the updated order.
                    if (afoodInsideDailyMenu.getId() == foodToEnterIntoOrder.getId()) {
                        org.bootcamp.yum.data.entity.OrderItem orderItem = new org.bootcamp.yum.data.entity.OrderItem();
                        orderItem.setFood(foodToEnterIntoOrder);
                        orderItem.setQuantity(aFoodThatRemained.getQuantity());
                        orderItem.setDailyOrder(dailyOrderToBeUpdated);
                        dailyOrderToBeUpdated.addOrderItem(orderItem);
                        foodFound = true;
                        break;
                    }
                }
                // If any food in the API's arguments is not found in the DailyMenu return 400.
                if (foodFound == false) {
                    throw new ApiException(400, "Food not inside the menu.");
                }
            }

            // For mapping purposes
            dailyOrderToBeUpdated.setUser(loggedUser);
            // Trick JPA in order to update version
            dailyOrderToBeUpdated.setUserId(loggedUser.getId());
            // @Version Independently changes the DB version
            // We add the +1 in order have the right value in the DTO.
            dailyOrderToBeUpdated.setVersion(dailyOrderToUpdate.getVersion() + 1);
            dailyOrderToBeUpdated.setDailyMenu(dailyMenuOfThatDay);
            dailyOrderrepo.save(dailyOrderToBeUpdated);

            //We need to return only the version and the id, but let's simply return the DTO
            DailyOrder dailyOrderUpdatedDTO = dailyOrderToBeUpdated.dailyOrderDAO2DTO();

            if (dailyOrderToUpdate.getEmail()) {

                mailingService.sendConfirmOrderEmailToHungry(dailyOrderUpdatedDTO);
            }

            dailyOrderUpdatedDTO.setEmail(dailyOrderToUpdate.getEmail());
            return dailyOrderUpdatedDTO;
        }
    }

    private boolean quantitiesOk(DailyOrder dailyOrder) {
        boolean quantityStatus = true;
        for (DailyOrderFoods f : dailyOrder.getFoods()) {      
            if (!f.getQuantity().toString().matches("^[0-9]{0,3}$")) {
                quantityStatus = false;
            }
        }

        return quantityStatus;
    }

    private Menus getMenuFromDate(org.bootcamp.yum.data.entity.DailyMenu dm) {

        User currentUser = userrepo.findById(toIntExact((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal()));
        Menus menus = new Menus();
        MenusInner menu = new MenusInner();
        //menu.setFoods(dMenu.getFoods());
        List<Food> food = dm.getFoods();
        for (Food f : food) {
            FoodTypeConverter foodTypeConverter = new FoodTypeConverter();
            org.bootcamp.yum.api.model.Food modelFood = new org.bootcamp.yum.api.model.Food();
            modelFood.setFoodName(f.getName());
            modelFood.setFoodId(f.getId());
            modelFood.setDescription(f.getDescription());
            modelFood.setFoodType(foodTypeConverter.convertToDatabaseColumn(f.getType()));
            modelFood.setPrice(f.getPrice().doubleValue());
            MenusInnerFoods mif = new MenusInnerFoods();
            mif.setFood(modelFood);
            List<OrderItem> orderItems = f.getOrderItems();
            if (menu.getOrderId() == null) {
                menu.setOrderId(0);
            }
            if (orderItems != null) {
                for (OrderItem oItem : orderItems) {
                    if (oItem.getMemberKey().getFoodId() == f.getId()) {
                        org.bootcamp.yum.data.entity.DailyOrder dOrder = oItem.getDailyOrder();
                        if (dOrder.getUser().getId() == currentUser.getId() && dOrder.getDailyMenu() == dm) {
                            mif.setQuantity(oItem.getQuantity());

                            menu.setOrderId(oItem.getMemberKey().getDailyOrderId());

                        }
                    }
                }
            }
            // modelFood.setQuantity(0);
            menu.addFoodsItem(mif);
        }
        menu.setDailyMenuDate(dm.getDailyMenuDate());
        menu.setDailyMenuId(dm.getDailyMenuId());
        menu.setVersion(dm.getVersion());
        if (dm != null) {
            menus.add(menu);
        } else {
            menus.add(new MenusInner());
        }
        return menus;
    }
}
