/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp.yum.api;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.OptimisticLockException;

import io.swagger.annotations.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;

import org.bootcamp.yum.api.service.GlobalSettingsService;
import org.bootcamp.yum.exceptions.ApiException;
import org.bootcamp.yum.exceptions.ConcModException;
import org.bootcamp.yum.api.model.GlobalSettings;


@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-19T16:54:41.298+03:00")

@Controller
public class GlobalsettingsApiController implements GlobalsettingsApi {

    private GlobalSettingsService globalSettingsService;

    @Autowired
    public GlobalsettingsApiController(GlobalSettingsService globalSettingsService) {
        this.globalSettingsService = globalSettingsService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<GlobalSettings> globalsettingsGet() throws ApiException
    {
        GlobalSettings globalSettings = globalSettingsService.globalsettingsGet();
        return new ResponseEntity<>(globalSettings, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<GlobalSettings> globalsettingsPut(@ApiParam(value = "The updated settings.") @RequestBody GlobalSettings settings) throws ApiException, ConcModException
    {
        try 
        {
            globalSettingsService.globalsettingsPut(settings);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } 
        catch (OptimisticLockException ex) 
        {
            try 
            {
                GlobalSettings globalSettingsLatest = globalSettingsService.globalsettingsGet();
                throw new ConcModException(409, "Concurrent modification error.", globalSettingsLatest);
            } 
            catch (ApiException ex1) 
            {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }
}
