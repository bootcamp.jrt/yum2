/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */

package org.bootcamp;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import org.bootcamp.yum.data.converter.PasswordProtection;
import org.bootcamp.yum.data.converter.UserTypeConverter;
import org.bootcamp.yum.data.entity.GlobalSettings;
import org.bootcamp.yum.data.repository.GlobalSettingsRepository;
import org.bootcamp.yum.data.repository.UserRepository;


@Transactional
@Component
public class CmdLineApp implements CommandLineRunner {

    @Autowired
    private UserRepository userrepo;

    @Autowired
    private GlobalSettingsRepository globalSettingsrepo;

    @Override
    public void run(String... args) throws Exception {
        
           //if no first user- super admin found , create one
           if (userrepo.findAll().isEmpty()) {
//           if (false) {
            org.bootcamp.yum.data.entity.User firstUser = new org.bootcamp.yum.data.entity.User();

            UserTypeConverter userTypeConverter = new UserTypeConverter();

            firstUser.setEmail("admin@yum.com");
            firstUser.setFirstName("admin");
            firstUser.setLastName("admin");

            PasswordProtection passwordProtection = new PasswordProtection();
            String encryptedPassword = passwordProtection.encrypt("123456");
            firstUser.setPassword(encryptedPassword);
            firstUser.setRole(userTypeConverter.convertToEntityAttribute("admin"));

            LocalDate curr = new LocalDate();
            firstUser.setRegistrationDate(curr);
            firstUser.setVersion(0);
            firstUser.setApproved(true);

            userrepo.save(firstUser);

        }
   
        if (globalSettingsrepo.findOne(1)==null) {
   
            GlobalSettings globalSettings = new GlobalSettings();
            
            globalSettings.setGlobalSettingsId(1);
            globalSettings.setCurrency("&euro;");        
            globalSettings.setTimeLastOrder(LocalTime.parse("13:00:00"));    
            globalSettingsrepo.save(globalSettings);
        }

    }

}
