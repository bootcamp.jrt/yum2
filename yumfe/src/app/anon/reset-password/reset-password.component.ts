import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

import { AuthenticationService } from '../../shared/authentication.service';
import * as remote from '../../remote';

@Component({
  moduleId: module.id,
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {

  credentials: FormGroup;
  loading = false;
  private sub: any;
  private secretString: string;

  constructor(
    public router: ActivatedRoute,
    public route: Router,
    public authenticationService: AuthenticationService,
    public authApiService: remote.AuthApi,
    public snackBar: MdSnackBar,
    public fb: FormBuilder) { }

  ngOnInit() {
    this.sub = this.router.params.subscribe(params => {

      if (params) {
        this.secretString = params.secret;
      }
    });

    this.credentials = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      rePassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]]
    }, { validator: passwordMatcher });
  }

  changePassword(credentials: FormGroup) {
    this.loading = true;
    let password: remote.Password = {
      password: credentials.value.password
    }

    this.authApiService.authChangepwdSecretPut(password, this.secretString).subscribe(result => {
      this.loading = false;
      this.snackBar.open("Changed password successfully", 'OK', { duration: 5000, extraClasses: ['snackBarSuccess', 'snackBartext'] });
      this.route.navigate(['login']);
    }, error => {
      this.loading = false;

      if (error.status == 400) {
        var error = JSON.parse(error._body);
        this.snackBar.open(error.message, 'OK', { extraClasses: ['snackBarError', 'snackBartext'] });
        this.route.navigate(['login']);
      }
      else {
        this.snackBar.open('Something went wrong internally', 'OK', { extraClasses: ['snackBarError'] });
        this.route.navigate(['login']);
      }
    });
  }
}

// Struct that helps password matching validation
export const passwordMatcher = (control): { [key: string]: boolean } => {
  const password = control.get('password');
  const confirm = control.get('rePassword');
  if (!password || !confirm) {
    return null;
  }
  return password.value === confirm.value ? null : { nomatch: true };
};
