import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MdDialogRef, MdDialog } from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as remote from '../../remote';

@Component({
  moduleId: module.id,
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.scss']
})
export class ForgotpwdComponent implements OnInit {

  // Reactive form object that will become the post DTO
  body: FormGroup;
  loading = false;
  error = '';

  constructor(
    public router: Router,
    public forgotpwdService: remote.AuthApi,
    public authenticationService: AuthenticationService,
    public snackBar: MdSnackBar) { }

  ngOnInit() {

    this.body = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.minLength(2), Validators.email]),
    });
  }

  resetPassword({ value }: { value: remote.Body }) {
    this.loading = true;
    value.email.trim();
    this.forgotpwdService.authForgotpwdPost(value).subscribe(d => {
      this.loading = false;
      this.router.navigate(['/login']);
      this.openSnackBar("Email sent", 1);
    }, error => {
      this.loading = false;
      if (error.status === 404) {
        this.openSnackBar("Such user was not found", 3);
      } else if (error.status === 406) {
        this.openSnackBar("User is not approved", 3);
      } else if (error.status === 400) {
        this.openSnackBar("Invalid email", 3);
      }
      else {
        this.openSnackBar("Something went internally wrong", 3);
      }
    });
  }

  openSnackBar(msg: string, nmb: number) {

    if (nmb == 1) {
      this.snackBar.open(msg, 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb == 2) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    } else if (nmb == 3) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarError', 'snackBartext'] });
    } else {
      this.snackBar.open(msg, 'Ok', {});
    }
  }
}
