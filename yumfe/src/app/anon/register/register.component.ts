import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';

import { AuthenticationService } from '../../shared/authentication.service';
import * as remote from '../../remote';
import { TosService } from "../../shared/tos.service";

@Component({
  moduleId: module.id,
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})

export class RegisterComponent implements OnInit {

  user: remote.Register = {};
  userGroup: FormGroup;
  createUserDisable = false;
  error = '';

  constructor(
    public router: Router,
    public registerService: remote.AuthApi,
    public fb: FormBuilder,
    public snackBar: MdSnackBar,
    public tos: TosService) {
  }

  ngOnInit() {
    this.userGroup = this.fb.group({
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.email, , Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      rePassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
    }, { validator: passwordMatcher })
  }

  register({ value }: { value: remote.Register }) {
    this.createUserDisable = true;
    value.email = value.email.trim();
    value.firstName = value.firstName.trim();
    value.lastName = value.lastName.trim();
    this.registerService.authRegisterPost(value).subscribe(result => {

      this.createUserDisable = false;
      this.openSuccessSnackBar();
      this.router.navigate(['login']);
    }, error => {
      this.createUserDisable = false;
      this.onRegisterErrorHandling(error);
    });
  }

  onRegisterErrorHandling(error) {
    this.openErrorSnackBar(error);
    this.createUserDisable = false;
  }

  openSuccessSnackBar() {
    this.snackBar.open('Account creation successful', 'OK', { duration: 5000, extraClasses: ['snackBarSuccess'] });
  }

  openErrorSnackBar(msgType: any) {
    var error = JSON.parse(msgType._body);
    if (msgType.status === 400) {
      this.snackBar.open(error.message, 'OK', { extraClasses: ['snackBarError'] });
    }
    // Catch the really obscure errors
    else {
      this.snackBar.open('Something went wrong internally', 'OK', { extraClasses: ['snackBarError'] });
    }
  }
}
// Struct that helps password matching validation
export const passwordMatcher = (control): { [key: string]: boolean } => {
  const password = control.get('password');
  const confirm = control.get('rePassword');
  if (!password || !confirm) {
    return null;
  }
  return password.value === confirm.value ? null : { nomatch: true };
};
