import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MdDialogRef, MdDialog } from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

import { AuthenticationService } from '../../shared/authentication.service';
import * as remote from '../../remote';

@Component({
  moduleId: module.id,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {


  // The DTO with all the page users and the total number of users

  // Reactive form object that will become the post DTO
  credentials: FormGroup;

  model: remote.Login;
  loading = false;
  error = '';

  constructor(
    public router: Router,
    public authenticationService: AuthenticationService,
    public snackBar: MdSnackBar) { }

  ngOnInit() {

    this.credentials = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.minLength(2)]),
      password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }

  // Create a new user
  onSubmit({ value }: { value: remote.Login }) {
    this.loading = true;
    this.authenticationService.login(value.email.trim(), value.password)
      .subscribe(result => {

        this.router.navigate(['/']);

      }, error => {
        this.loading = false;
        if (error.status === 400) {
          var error = JSON.parse(error._body);
          this.openSnackBar(error.message, 3);
        } else if (error.status == 404) {
          this.openSnackBar("Such user not found", 3);
        } else if (error.status == 405) {
          this.openSnackBar("User not approved", 3);
        } else {
          this.snackBar.open('Something went wrong internally', 'OK', { extraClasses: ['snackBarError'] });
        }

      });
  }

  forgotPassword() {
    this.router.navigate(['forgotpwd']);
  }


  // Handles the success and error snackbars
  openSnackBar(msg: string, nmb: number) {

    if (nmb == 1) {
      this.snackBar.open(msg, 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb == 2) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    } else if (nmb == 3) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarError', 'snackBartext'] });
    } else {
      this.snackBar.open(msg, 'Ok', {});
    }
  }

}
