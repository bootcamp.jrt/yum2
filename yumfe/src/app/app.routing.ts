
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './anon/login/login.component';
import { RegisterComponent } from './anon/register/register.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';
import { ForgotpwdComponent } from './anon/forgotpwd/forgotpwd.component';
import { SettingsComponent } from './shared/settings/settings.component';
import { ResetPasswordComponent } from '../app/anon/reset-password/reset-password.component';
import { AppGuard, CanLoginGuard, IsLoggedInGuard } from "app/app.guard";

const appRoutes: Routes = [
  { path: '', component:NotFoundComponent, pathMatch: 'full', canActivate: [AppGuard] },
  { path: 'login', component: LoginComponent  , canActivate: [CanLoginGuard] },
  { path: 'register', component: RegisterComponent },
  { path: 'forgotpwd', component: ForgotpwdComponent },
  { path: 'changepassword/:secret', component: ResetPasswordComponent },
  { path: 'settings', component: SettingsComponent, canActivate: [IsLoggedInGuard] },
  { path: '**', component: NotFoundComponent }, //always last
];

export const AppRouting = RouterModule.forRoot(appRoutes);
