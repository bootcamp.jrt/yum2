import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { DailyOrderFormComponent } from '../hungry/home/daily-order-form/daily-order-form.component';
import { HistoryComponent } from './history/history.component';
import { LoggedComponent } from '../shared/logged/logged.component';
import { IsLoggedInGuard } from "app/app.guard";

const hungryRoutes: Routes = [
  {
    path: 'hungry',
    component: LoggedComponent,
    canActivate: [IsLoggedInGuard],
    children: [
      { path: '', component: HomeComponent },
      { path: 'history/:year/:month', component: HistoryComponent },
      { path: ':year/:week', component: HomeComponent },
      { path: 'history', component: HistoryComponent },
    ]
  }
];

export const HungryRouting = RouterModule.forChild(hungryRoutes);
