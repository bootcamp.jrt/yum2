import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.service';
import { GlobalSettingsService } from '../../shared/globalsettings.service';
import { DailyOrderHistoryComponentComponent } from './daily-order-history-component/daily-order-history-component.component';
import * as remote from '../../remote';

@Component({
  selector: 'app-hungry-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {

  dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  daysAfterMonday: Array<number> = [];

  monthlyHistory: remote.Menus;
  totalPrice: Array<number> = [];
  orderAvailability: Array<boolean> = [];
  monthlyTotal: number = 0;

  foodMap: Map<string, number>;
  monthDays: number;
  currMonthNumber: number;
  currYear: number;
  monthStart: number;
  private sub: any;
  loading = true;

  constructor(
    public hungryService: remote.HungryApi,
    public authenticationService: AuthenticationService,
    public router: ActivatedRoute,
    public route: Router,
    public globalSettingsService: GlobalSettingsService) { }


//on init check for parameters. If no parameters are passed,
//redirect to the current month.
  ngOnInit() {

    this.sub = this.router.params.subscribe(params => {

      if (params.year && params.month) {

        this.currYear = parseInt(params.year);
        this.currMonthNumber = parseInt(params.month);

        if (this.currMonthNumber === this.getMonthNumber() && this.currYear === this.getYearNumber()) {
          this.route.navigate(['/hungry/history']);
        }
        else {
          this.hungryService.menusMonthlyMonthOfYearGet(this.currMonthNumber + "-" + this.currYear).subscribe(dailymenus => {
            this.monthlyHistory = dailymenus;
            this.monthDays = this.daysInMonth();
            this.monthStartCalculate(this.currYear, this.currMonthNumber);
            this.monthlyHistory = this.removeWeekendsFromHistory();
            this.calculateTotalPrice();
            this.calculateOrderAvaliability();
            this.calculateMonthlyTotal();
            this.loading = false;
          }, error => {
            this.loading = false;
          });
        }

      } else {

        this.currMonthNumber = this.getMonthNumber();
        this.currYear = this.getYearNumber();

        this.hungryService.menusMonthlyGet().subscribe(dailymenus => {
          this.monthlyHistory = dailymenus;
          this.monthDays = this.daysInMonth();
          this.monthStartCalculate(this.currYear, this.currMonthNumber);
          this.monthlyHistory = this.removeWeekendsFromHistory();
          this.calculateTotalPrice();
          this.calculateOrderAvaliability();
          this.calculateMonthlyTotal();
          this.loading = false;
        }, error => this.loading = false);
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

//find on which day starts the asked month. This is used to calculate the number
//of empty divs in the grid list
  monthStartCalculate(year: number, month: number) {
    this.daysAfterMonday = [];
    let d = new Date(year, month - 1, 1);
    let size = 0;
    if (d.getDay() === 2) {
      size = 1;
    } else if (d.getDay() === 3) {
      size = 2;
    } else if (d.getDay() === 4) {
      size = 3;
    } else if (d.getDay() === 5) {
      size = 4;
    } else {
      size = 0;
    }
    for (let i = 0; i < size; i++) {
      this.daysAfterMonday.push(1);
    }
  }

//calculate the total amount of money that were spent during each day in the asked month.
  calculateTotalPrice() {
    this.totalPrice = [];
    let found: boolean = false;
    for (let i = 0; i < this.monthlyHistory.length; i++) {
      let total: number = 0;
      for (let j = 0; j < this.monthlyHistory[i].foods.length; j++) {
        if (this.monthlyHistory[i].foods[j].quantity > 0) {
          total = total + (this.monthlyHistory[i].foods[j].quantity * this.monthlyHistory[i].foods[j].food.price);
        }
      }
      if (total === 0) {
        this.totalPrice.push(0);
      } else {
        this.totalPrice.push(total);
      }
    }
  }

//past dates are not available for ordering
  calculateOrderAvaliability() {
    let d = new Date();
    this.orderAvailability = [];
    for (let i = 0; i < this.monthlyHistory.length; i++) {
      let d2 = new Date(this.monthlyHistory[i].dailyMenuDate);
      if (d > d2) {
        this.orderAvailability.push(false);
      } else {
        this.orderAvailability.push(true);
      }
    }
  }

//calculate the total amount of money that were spent during the asked month.
  calculateMonthlyTotal() {
    this.monthlyTotal = 0;
    for (let i = 0; i < this.totalPrice.length; i++) {
      this.monthlyTotal = this.monthlyTotal + this.totalPrice[i];
    }
  }

//the back end API returns a menu for each day of the month.
//We remove the weekends from our grid list.
  removeWeekendsFromHistory(): remote.Menus {
    let menu = this.monthlyHistory;
    let index = 0;
    for (let i = this.monthlyHistory.length - 1; i >= 0; i--) {
      let d = new Date(this.currYear, this.currMonthNumber - 1, i + 1);
      if (d.getDay() === 6 || d.getDay() === 0) {
        menu.splice(i, 1);
      }
    }
    return menu;
  }

//returns the number of days in the asked month
  daysInMonth(): number {
    return new Date(this.currYear, this.currMonthNumber, 0).getDate();
  }

//returns the month number
  getMonthNumber(): number {
    let date = new Date();
    return date.getMonth() + 1;
  }

//returns the year number
  getYearNumber(): number {
    let date = new Date();
    return date.getFullYear();
  }

//returns the previous year
  getPreviousYear(): number {
    if (this.currMonthNumber === 1)
      return this.currYear - 1;
    return this.currYear;
  }

//returns the next year
  getNextYear(): number {
    if (this.currMonthNumber === 12)
      return this.currYear + 1;
    return this.currYear;
  }

  //returns the previous month
  getPreviousMonth(): number {
    if (this.currMonthNumber === 1)
      return 12;
    return this.currMonthNumber - 1;
  }

//returns the next month
  getNextMonth(): number {
    if (this.currMonthNumber === 12)
      return 1;
    return this.currMonthNumber + 1;
  }
}
