import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyOrderHistoryComponentComponent } from './daily-order-history-component.component';

describe('DailyOrderHistoryComponentComponent', () => {
  let component: DailyOrderHistoryComponentComponent;
  let fixture: ComponentFixture<DailyOrderHistoryComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyOrderHistoryComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyOrderHistoryComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
