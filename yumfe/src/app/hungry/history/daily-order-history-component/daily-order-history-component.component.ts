import { Component, OnInit, Input } from '@angular/core';
import * as remote from '../../../remote';
import { Router } from '@angular/router';
import { GlobalSettingsService } from '../../../shared/globalsettings.service';


@Component({
  selector: 'app-daily-order-history-component',
  templateUrl: './daily-order-history-component.component.html',
  styleUrls: ['./daily-order-history-component.component.scss']
})
export class DailyOrderHistoryComponentComponent implements OnInit {

  @Input() dailyHistory: remote.MenusInner;
  @Input() orderAvail: boolean;
  @Input() totalPr: number;

  constructor(
    public router: Router,
    public hungryService: remote.HungryApi,
    public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() {
  }

//get year
//this is used to navigate to /hungry/home
  getYear(): number{
     let date = new Date(this.dailyHistory.dailyMenuDate);
     return date.getFullYear();
   }

//get number of week
//this is used to navigate to /hungry/home
   getWeek(): number{
    let date = new Date(this.dailyHistory.dailyMenuDate);
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    let week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
      - 3 + (week1.getDay() + 6) % 7) / 7);
   }

//navigate to correct link
   navigate(){
     if(this.dailyHistory.dailyMenuId!=null){
       this.router.navigate(['/hungry', this.getYear(), this.getWeek()]);
     }
   }
}
