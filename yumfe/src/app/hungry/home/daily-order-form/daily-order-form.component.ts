import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { HomeComponent } from '../home.component';
import { DailyMenuByCategory } from '../../home/dailyMenuByCategory';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../shared/authentication.service';
import { ActivatedRoute } from '@angular/router';
import * as models from '../../../remote/model/models';
import * as remote from '../../../remote';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { MdDialogRef, MdDialog } from '@angular/material';
import { GlobalSettingsService } from '../../../shared/globalsettings.service';

@Component({
  selector: 'app-daily-order-form',
  templateUrl: 'daily-order-form.component.html',
  styleUrls: ['daily-order-form.component.scss']
})
export class DailyOrderFormComponent implements OnInit {

  //input from parent containing foods categorized by foodType
  @Input() dailyMenuFoodsByCategory: DailyMenuByCategory;
  @Output() changedTotalPrice = new EventEmitter();
  //flags for defining statuses and button
  //state of daily orders
  modifyFlag: boolean = true;
  deleteFlag: boolean = false;
  latestVersion: number = 0; //global value holding the latest version of menu from Db
  disableSubmit = false;
  checkBoxValue: boolean;
  foodPriceMap: Map<number, models.Food>;//
  dialogRef: MdDialogRef<any>;
  loading: boolean = false;

  addExtraClass: boolean = false;
  time: Date;

  constructor(
    public route: Router,
    public formB: FormBuilder,
    public hungryService: remote.HungryApi,
    public authenticationService: AuthenticationService,
    public router: ActivatedRoute,
    public snackBar: MdSnackBar,
    public dialog: MdDialog,
    public globalSettingsService: GlobalSettingsService

  ) {
    this.globalSettingsService.getDeadLine().subscribe(time => this.time = time);
  }
  doLogin(event) {

  }

  ngOnInit() {
    //if there is an order, we load the version of that menu from db
    if (this.dailyMenuFoodsByCategory.orderId != 0) {
      this.setLatestVersionOfDailyOrder();
    }
    //populate the price map from component's values
    this.foodPriceMap = new Map<number, models.Food>();
    for (let item of this.dailyMenuFoodsByCategory.foods) {
      this.foodPriceMap.set(item.food.foodId, item.food);
    }
  }
  //returns the value of an ordered OR an about-to-order menu
  calculateTotal(): number {
    this.dailyMenuFoodsByCategory.total = 0;
    for (const food of this.dailyMenuFoodsByCategory.foods) {
      this.dailyMenuFoodsByCategory.foodsMap.get(food.food.foodType).set(food.quantity, food);
      this.dailyMenuFoodsByCategory.total += food.food.price * food.quantity;
    }
    return this.dailyMenuFoodsByCategory.total;
  }

  //below are the methods that are invoked by the buttons
  orderDailyMenu() {
    if (this.calculateTotal() != 0) {
      this.open(DialogExecuteOrderComponent);
    } else {
      this.openSnackBar("Choose something to order", 3);
    }
  }

  modifyDailyMenu() {
    this.disableSubmit = true;
    this.toggleModifyFlag();
    this.refreshAfterConcurrency();
  }

  submitChanges() {
    this.disableSubmit = true;
    this.loading = true;

    if (this.calculateTotal() == 0) {
      this.deleteOrder();
      return;
    }
    this.open(DialogExecuteOrderComponent);
  }

  goBack() {
    this.disableSubmit = false;
    this.toggleModifyFlag();
  }

  deleteOrder() {

    this.disableSubmit = true;
    this.loading = true;
    const dailyOrderToDelete: models.DailyOrder = {
      day: this.dailyMenuFoodsByCategory.dailyMenuDate,
      email: true,
      id: 5,
      dmId: this.dailyMenuFoodsByCategory.dailyMenuId,
      version: this.latestVersion,
      foods: this.dailyMenuFoodsByCategory.foods
    };

    this.hungryService.ordersIdDelete(this.dailyMenuFoodsByCategory.orderId, dailyOrderToDelete).subscribe(result => {
      this.dailyMenuFoodsByCategory.total = 0;
      for (const food of this.dailyMenuFoodsByCategory.foods) {
        food.quantity = null;
      }
      this.dailyMenuFoodsByCategory.orderId = 0;
      this.deleteFlag = false;
      this.openSnackBar("Order successfully deleted", 1);
      this.disableSubmit = false;
      this.loading = false;
      this.modifyFlag = true;

    }, error => {
      if (error.status == 410) {
        let concurrentItem: models.DailyOrder = (JSON.parse(error._body));
        const returnedMenu: models.MenusInner = {
          dailyMenuDate: concurrentItem[0].dailyMenuDate,
          dailyMenuId: concurrentItem[0].dailyMenuId,
          orderId: concurrentItem[0].orderId,
          foods: concurrentItem[0].foods
        };
        this.getDailyMenuByCategory(returnedMenu);
      } else if (error.status == 500) {

        this.openSnackBar("Something went internally wrong", 3);
      } else {

        this.refreshAfterConcurrency();

      }
      this.openSnackBar("Seems you had deleted this order.", 2);
      this.changedTotalPrice.emit();
      this.disableSubmit = false;
      this.loading = false;
    },
      () => {
        this.changedTotalPrice.emit();
      });
  }


  modifyMe() {
    this.disableSubmit = true;
    this.loading = true;
    const dailyOrderToUpdate: models.DailyOrder = {
      day: this.dailyMenuFoodsByCategory.dailyMenuDate,
      email: this.checkBoxValue,
      id: 5,
      dmId: this.dailyMenuFoodsByCategory.dailyMenuId,
      version: this.latestVersion,
      foods: this.getOrderedFoods()
    };
    if (dailyOrderToUpdate.foods == null) return;

    this.hungryService.ordersIdPut(this.dailyMenuFoodsByCategory.orderId, dailyOrderToUpdate).subscribe(result => {
      this.setLatestVersionOfDailyOrder();
      this.openSnackBar("Order successfully modified", 1);
      this.disableSubmit = false;
      this.loading = false;
      this.toggleModifyFlag();
    }, error => {
      if (error.status == 409 || error.status == 404) {
        let concurrentItem: models.DailyOrder = (JSON.parse(error._body));
        this.refreshAfterConcurrency();
        this.openSnackBar("Order info refreshed, concurrency occured", 2);
      } else if (error.status == 500) {
        this.openSnackBar("Something went internally wrong", 3);
      } else if (error.status == 413) {
        this.openSnackBar("Quantity should be up to 3 digits", 3);
      }

      this.disableSubmit = false;
      this.loading = false;
    },
      () => {
        this.changedTotalPrice.emit();
      }
    );

  }
  //method that updates the version of the menu
  setLatestVersionOfDailyOrder() {
    this.hungryService.ordersIdGet(this.dailyMenuFoodsByCategory.orderId).subscribe(result => {
      this.latestVersion = result.version;
    }, error => { });
  }
  //opens the modal for the order menu
  open(thatDialog) {


    this.dialogRef = this.dialog.open(thatDialog);
    this.dialogRef.componentInstance.total = this.calculateTotal();
    this.dialogRef.componentInstance.date = new Date(this.dailyMenuFoodsByCategory.dailyMenuDate);
    let currentUser = this.authenticationService.getLoggedInUser();
    this.dialogRef.componentInstance.user = currentUser.firstName + " " + currentUser.lastName;
    this.dialogRef.componentInstance.foodOrdered = this.getOrderedFoods();
    this.dialogRef.componentInstance.totalPrice = this.dailyMenuFoodsByCategory.total;
    this.dialogRef.componentInstance.foodsPrice = this.foodPriceMap;

    this.dialogRef.afterClosed().subscribe(result => {
      if (result === 'submit' || result === 'submitWithEmail') {
        if (this.dailyMenuFoodsByCategory.orderId !== 0) {
          this.checkBoxValue = result === 'submitWithEmail';
          this.modifyMe();
        } else {
          this.loading = true;
          this.checkBoxValue = result === 'submitWithEmail';
          this.disableSubmit = true;
          let foodsToOrder = new Array<models.DailyOrderFoods>();

          for (let item of this.dailyMenuFoodsByCategory.foods) {
            if (item.quantity) {
              const innerFood: models.DailyOrderFoods = {
                foodId: item.food.foodId,
                quantity: item.quantity
              }
              foodsToOrder.push(innerFood);
            }
          }
          const dailyOrder: models.DailyOrder = {
            day: this.dailyMenuFoodsByCategory.dailyMenuDate,

            email: this.checkBoxValue,
            id: currentUser.userId,
            dmId: 0,
            version: 0,
            foods: foodsToOrder
          };

          this.hungryService.ordersPost(dailyOrder).subscribe(result => {
            this.dailyMenuFoodsByCategory.orderId = result.id;
            this.modifyFlag = true;
            this.openSnackBar("Orders successfully placed", 1);
            this.setLatestVersionOfDailyOrder();
            this.disableSubmit = false;
            this.loading = false;
          }, error => {
            let concurrentItem: models.DailyOrder = (JSON.parse(error._body));
            if (error.status == 410) {
              const returnedMenu: models.MenusInner = {
                dailyMenuDate: concurrentItem[0].dailyMenuDate,
                dailyMenuId: concurrentItem[0].dailyMenuId,
                orderId: concurrentItem[0].orderId,
                foods: concurrentItem[0].foods
              };
              this.getDailyMenuByCategory(returnedMenu);
              this.setLatestVersionOfDailyOrder();
              this.modifyFlag = true;
              this.openSnackBar("You have already ordered for this day.", 2);
              this.changedTotalPrice.emit();
            } else if (error.status === 413) {
              this.openSnackBar("Quantity should be up to 3 digits", 3);
            } else if (error.status === 400) {
              this.openSnackBar("Deadline for ordering has changed, refresh your page", 3);
            } else if (error.status == 500) {
              this.openSnackBar("Something went internally wrong", 3);
            }
            else {
              this.refreshAfterConcurrency();
            }

            this.disableSubmit = false;
            this.loading = false;

          },
            () => {
              this.changedTotalPrice.emit();
            }
          );
        }

      } else if (result === 'back') {
        this.dialogRef = null;
        this.disableSubmit = false;
        this.loading = false;
      }
      this.dialogRef = null;
      this.disableSubmit = false;
      this.loading = false;
    });
  }

  openSnackBar(msg: string, nmb: number) {

    if (nmb == 1) {
      this.snackBar.open(msg, 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb == 2) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    } else if (nmb == 3) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarError', 'snackBartext'] });
    } else {
      this.snackBar.open(msg, 'Ok', {});
    }
  }

  // method to pass ordered foods inside the modal window
  getOrderedFoods(): Array<models.DailyOrderFoods> {
    let checkQuantities = 0;

    const foodsToUpdate = new Array<models.DailyOrderFoods>();
    for (let item of this.dailyMenuFoodsByCategory.foods) {
      if (item.quantity) {

        const innerFood: models.DailyOrderFoods = {
          foodId: item.food.foodId,
          quantity: item.quantity,
        };
        checkQuantities += item.quantity;
        foodsToUpdate.push(innerFood);

      }
    }

    if (checkQuantities === 0 && this.deleteFlag) {
      this.deleteOrder();
      return;
    }
    return foodsToUpdate;
  }

  //brings back the daily menu of the day
  refreshAfterConcurrency() {

    this.hungryService.menusDayDayDateGet(this.dailyMenuFoodsByCategory.dailyMenuDate + "").subscribe(result => {
      this.getDailyMenuByCategory(result[0]);
      this.setLatestVersionOfDailyOrder();
      this.disableSubmit = false;
    }, error => {
      this.disableSubmit = false;
    }
    );


  }

  //categorizes again the menu received from the database
  getDailyMenuByCategory(dailymenu: remote.MenusInner) {

    const freshDailyMenuByCategory: DailyMenuByCategory = {

      total: 0,
      dailyMenuId: dailymenu.dailyMenuId,
      dailyMenuDate: dailymenu.dailyMenuDate,
      orderId: dailymenu.orderId,
      foods: dailymenu.foods,
      foodsMap: new Map<string, Map<number, models.MenusInnerFoods>>()
    };

    freshDailyMenuByCategory.foodsMap.set('Main dish', new Map<number, models.MenusInnerFoods>());
    freshDailyMenuByCategory.foodsMap.set('Salad', new Map<number, models.MenusInnerFoods>());
    freshDailyMenuByCategory.foodsMap.set('Drink', new Map<number, models.MenusInnerFoods>());

    for (const food of dailymenu.foods) {

      freshDailyMenuByCategory.foodsMap.get(food.food.foodType).set(food.food.foodId, food);
      freshDailyMenuByCategory.total += food.food.price * food.quantity;
    }
    if (this.dailyMenuFoodsByCategory.total != freshDailyMenuByCategory.total) {
      this.openSnackBar("Seems you had modified this order earlier", 2);
      this.changedTotalPrice.emit();
    }
    if(this.dailyMenuFoodsByCategory.foods.length!=freshDailyMenuByCategory.foods.length){
      this.openSnackBar("Seems a chef had modified this menu earlier", 2);
    }
    this.dailyMenuFoodsByCategory = freshDailyMenuByCategory;

  }

  //if pass menus, they should not be able to be modified
  calculateFinalState() {
    const d1 = new Date();
    const d2 = new Date(this.dailyMenuFoodsByCategory.dailyMenuDate);
    const d3 = d1.getDate();
    const d4 = new Date(new Date().setDate(d2.getDate() - 1)).getDate();

    if ((d1 > d2) || ((d3 === d4) && (d1.getHours() > new Date(this.time).getHours()))) {
      return true;
    } else if ((d3 === d4) && (d1.getHours() === new Date(this.time).getHours()) && (d1.getMinutes() > new Date(this.time).getMinutes())) {
      return true;
    }
    return false;
  }

  toggleModifyFlag() {


    this.modifyFlag = !this.modifyFlag;
    this.deleteFlag = !this.deleteFlag;

  }

  isEnabled(): boolean {
    if (this.dailyMenuFoodsByCategory.orderId == 0 || !this.modifyFlag) return true;
    else return false;
  }

}

@Component({
  selector: 'app-daily-order-confirm-dialog',
  templateUrl: 'daily-order-confirm-dialog.html',
  styleUrls: ['daily-order-form.component.scss']
})
export class DialogExecuteOrderComponent implements OnInit {

  public user: string;
  public total: number;
  public date: Date;
  public foodOrdered: Array<models.DailyOrderFoods>;
  public totalPrice: number;
  public totalQuantity: number = 0;
  public checkBox: boolean = false;
  public foodsPrice: Map<number, models.Food>;

  constructor(public dialogRef: MdDialogRef<any>,
    public globalSettingsService: GlobalSettingsService) { }

  isEmailChecked(): string {
    return this.checkBox ? 'submitWithEmail' : 'submit';
  }

  ngOnInit() {
    for (let item of this.foodOrdered) {
      if (item.quantity !== 0) {
        this.totalQuantity += item.quantity;
      }
    }
  }

}





