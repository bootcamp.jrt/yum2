import { Component, OnInit, OnDestroy, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { GlobalSettingsService } from '../../shared/globalsettings.service';
import { AuthenticationService } from '../../shared/authentication.service';
import { DailyMenuByCategory } from '../home/dailyMenuByCategory';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import * as models from '../../remote/model/models';
import * as remote from '../../remote';

@Component({
  selector: 'app-hungry-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

  weeklyDailyMenus: remote.Menus;
  dailyMenuFoodsByCategory: Array<DailyMenuByCategory> = [];

  currWeekNumber: number;
  currYear: number;
  weeklyTotal: number = 0;
  private sub: any;
  loading = true;
  notes;

  constructor(
    public hungryService: remote.HungryApi,
    public authenticationService: AuthenticationService,
    public router: ActivatedRoute,
    public route: Router,
    public snackBar: MdSnackBar,
    public globalSettingsService: GlobalSettingsService
  ) {
    this.notes = globalSettingsService.getNotes();
  }

  ngOnInit() {
    this.sub = this.router.params.subscribe(params => {

      if (params.year && params.week) {

        this.currYear = parseInt(params.year);
        this.currWeekNumber = parseInt(params.week); // (+) converts string 'id' to a number

        if (this.currWeekNumber === this.getWeekNumber() && this.currYear === this.getYearNumber()) {
          this.route.navigate(['/hungry']);
        }
        else {
          this.hungryService.menusWeeklyWeekOfYearGet(this.currWeekNumber + "-" + this.currYear).subscribe(dailymenus => {
            this.weeklyDailyMenus = dailymenus;
            this.dailyMenuFoodsByCategory = this.getDailyMenuByCategory(dailymenus);
            this.loading = false;

          }, error => {
            if (error.status == 409) {
              this.currYear = this.currYear + 1;
              this.currWeekNumber = 1;
              this.route.navigate(['/hungry', this.currYear, this.currWeekNumber]);
              this.loading = false;
            } else if (error.status == 500) {
              this.openSnackBar("Something went internally wrong", 3);
            }
          });
        }

      } else {

        this.currWeekNumber = this.getWeekNumber();
        this.currYear = this.getYearNumber();

        this.hungryService.menusWeeklyGet().subscribe(dailymenus => {
          this.weeklyDailyMenus = dailymenus;
          this.dailyMenuFoodsByCategory = this.getDailyMenuByCategory(dailymenus);
          this.loading = false;

        }, error => {
          this.loading = false;
          if (error.status == 500) {
            this.openSnackBar("Something went internally wrong", 3);
          }
        });
      }
    });

  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  refreshWeeklyMenus() {

    this.hungryService.menusWeeklyGet().subscribe(dailymenus => {
      this.weeklyDailyMenus = dailymenus;
      this.dailyMenuFoodsByCategory = this.getDailyMenuByCategory(dailymenus);
    }, error => {
      if (error.status == 500) {
        this.openSnackBar("Something went internally wrong", 3);
      }
    });


  }

  getWeekNumber(): number {
    let date = new Date();
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    let week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
      - 3 + (week1.getDay() + 6) % 7) / 7);
  }

  getWeekNumberForYear(year: number): number {
    let date = new Date(year + "-12-31");
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    let week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    let ret = 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
      - 3 + (week1.getDay() + 6) % 7) / 7);
    // if 31dec is in week 1 of following year, we know that this year has only 52 weeks
    if (ret == 1) ret = 52;
    return ret;
  }

  getYearNumber(): number {
    let date = new Date();

    return date.getFullYear();
  }

  getPreviousYear(): number {
    if (this.currWeekNumber === 1)
      return this.currYear - 1;
    return this.currYear;
  }

  getNextYear(): number {
    let maxWeek = this.getWeekNumberForYear(this.currYear);
    if (this.currWeekNumber === maxWeek)
      return this.currYear + 1;
    return this.currYear;
  }
  getPreviousWeek(): number {
    if (this.currWeekNumber === 1) {
      let maxWeek = this.getWeekNumberForYear(this.currYear - 1);
      return maxWeek;
    }

    return this.currWeekNumber - 1;
  }

  getNextWeek(): number {
    let maxWeek = this.getWeekNumberForYear(this.currYear);
    if (this.currWeekNumber === maxWeek)
      return 1;
    return this.currWeekNumber + 1;
  }

  handleChangeOfDailyOrderPrice() {

    this.hungryService.menusWeeklyWeekOfYearGet(this.currWeekNumber + "-" + this.currYear).subscribe(dailymenus => {
      this.weeklyDailyMenus = dailymenus;
      this.dailyMenuFoodsByCategory = this.getDailyMenuByCategory(dailymenus);
      this.loading = false;

    }, error => {
      if (error.status === 409) {
        this.currYear = this.currYear + 1;
        this.currWeekNumber = 1;
        this.route.navigate(['/hungry', this.currYear, this.currWeekNumber]);
        this.loading = false;
      } else if (error.status === 500) {
        this.openSnackBar("Something went internally wrong", 3);
      }
    });


  }

  openSnackBar(msg: string, nmb: number) {

    if (nmb == 1) {
      this.snackBar.open(msg, 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb == 2) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    } else if (nmb == 3) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarError', 'snackBartext'] });
    } else {
      this.snackBar.open(msg, 'Ok', {});
    }
  }

  getDailyMenuByCategory(dailymenus: remote.Menus): Array<DailyMenuByCategory> {

    const newDailyMenuFoodsByCategory: Array<DailyMenuByCategory> = [];
    for (const dmenu of dailymenus) {

      const myMenu: DailyMenuByCategory = {

        total: 0,
        dailyMenuId: dmenu.dailyMenuId,
        dailyMenuDate: dmenu.dailyMenuDate,
        orderId: dmenu.orderId,
        foods: dmenu.foods,
        foodsMap: new Map<string, Map<number, models.MenusInnerFoods>>()
      };

      myMenu.foodsMap.set('Main dish', new Map<number, models.MenusInnerFoods>());
      myMenu.foodsMap.set('Salad', new Map<number, models.MenusInnerFoods>());
      myMenu.foodsMap.set('Drink', new Map<number, models.MenusInnerFoods>());

      for (const food of dmenu.foods) {

        myMenu.foodsMap.get(food.food.foodType).set(food.food.foodId, food);
        myMenu.total += food.food.price * food.quantity;

      }
      newDailyMenuFoodsByCategory.push(myMenu);

    };
    this.weeklyTotal = 0;
    for (let item of newDailyMenuFoodsByCategory) {
      this.weeklyTotal += item.total;
    }

    return newDailyMenuFoodsByCategory;

  }
}

