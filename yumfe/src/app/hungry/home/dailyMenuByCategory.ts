import * as models from '../../remote/model/models';

export interface DailyMenuByCategory extends models.MenusInner {

  total: number;
  foodsMap: Map<string, Map<number, models.MenusInnerFoods>>;

}
