import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HistoryComponent } from './history/history.component';
import { HungryRouting } from './hungry.routing';
import { HungryApi } from '../remote';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { DailyOrderFormComponent,DialogExecuteOrderComponent } from './home/daily-order-form/daily-order-form.component';
import { BrowserModule } from '@angular/platform-browser';
import { MapToIterable } from '../shared/mapToIterable';
import { FormsModule }   from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DailyOrderHistoryComponentComponent  } from './history/daily-order-history-component/daily-order-history-component.component';
import { HungryNavBarComponent } from './hungry-nav-bar/hungry-nav-bar.component';



@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    HungryRouting,
    SharedModule,
    RouterModule,
    ReactiveFormsModule,
    HttpModule,
    FormsModule

  ],
  declarations: [HomeComponent, HistoryComponent,  DailyOrderFormComponent, DailyOrderHistoryComponentComponent,DialogExecuteOrderComponent, MapToIterable, HungryNavBarComponent],
  entryComponents: [
    DialogExecuteOrderComponent
  ],
  providers: [HungryApi]
})
export class HungryModule { }
