import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-hungry-nav-bar',
  templateUrl: './hungry-nav-bar.component.html',
  styleUrls: ['./hungry-nav-bar.component.scss']
})
export class HungryNavBarComponent implements OnInit {

  constructor(
    public router: ActivatedRoute,
    public route: Router) { }

  ngOnInit() {


  }
  toHomePage(){
    this.route.navigate(['/hungry']);
  }
  toHistoryPage(){
    this.route.navigate(['/hungry/history']);
  }



}
