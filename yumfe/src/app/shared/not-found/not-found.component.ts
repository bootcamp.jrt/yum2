import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';



@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  constructor(
    public router: Router,
    public authenticationService: AuthenticationService) {}

  ngOnInit() {
  }

  goBack() {
    if (this.authenticationService.isLogged()) {
      this.router.navigate(['/' ]);
    }
    else {
      this.router.navigate(['/login' ]);
    }
  }

}
