import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoggedComponent } from './logged/logged.component';
import { RouterModule } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Configuration } from '../remote/configuration';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FooterComponent } from './footer/footer.component';
import {
  MdButtonModule, MdCheckboxModule, MdCardModule, MdInputModule,
  MdRadioModule, MdSelectModule, MdIconModule, MdSlideToggleModule,
  MdGridListModule, MdDialogModule, MdDialog, MdProgressSpinnerModule,
  MdSnackBarModule, MdButtonToggleModule, MdMenuModule, MdTabsModule
} from '@angular/material';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ProfileComponent } from './profile/profile.component';
import { PaginationComponent } from './pagination/pagination.component';
import { ApproveComponent } from './approve/approve.component';
import { DialogCantDeleteComponent } from './approve/approve.component';
import { DialogOrdersDelDisappComponent } from './approve/approve.component';
import { SettingsComponent } from './settings/settings.component';
import { TermsOfServiceComponent, TosService, TabsOverview } from './tos.service';
import { HeaderComponent } from './header/header.component';
import { FileUploadModule } from 'ng2-file-upload';




@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    MdSelectModule,
    MdInputModule,
    MdButtonModule,
    MdButtonToggleModule,
    MdSlideToggleModule,
    MdCardModule,
    MdProgressSpinnerModule,
    MdSnackBarModule,
    MdMenuModule,
    MdIconModule,
    MdTabsModule,
    FlexLayoutModule,
    FileUploadModule
  ],
  declarations: [
    NotFoundComponent,
    LoggedComponent,
    ProfileComponent,
    PaginationComponent,
    ApproveComponent,
    DialogCantDeleteComponent,
    DialogOrdersDelDisappComponent,
    TermsOfServiceComponent,
    FooterComponent,
    HeaderComponent,
    SettingsComponent,
    TabsOverview
  ],
  providers: [AuthenticationService, Configuration, MdDialog, TosService],
  exports: [
    MdButtonModule,
    MdCheckboxModule,
    MdCardModule,
    MdInputModule,
    BrowserAnimationsModule,
    MdRadioModule,
    MdSelectModule,
    MdIconModule,
    MdSlideToggleModule,
    MdGridListModule,
    MdDialogModule,
    MdProgressSpinnerModule,
    MdSnackBarModule,
    FormsModule,
    ReactiveFormsModule,
    ProfileComponent,
    FlexLayoutModule,
    MaterialModule,
    PaginationComponent,
    ApproveComponent,
    FileUploadModule
  ],
  entryComponents: [
    DialogCantDeleteComponent,
    DialogOrdersDelDisappComponent,
    TermsOfServiceComponent
  ]
})
export class SharedModule { }
