import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthenticationService } from '../authentication.service';
import { BASE_PATH } from '../../remote/variables';

import { CurrentUser } from '../header/current-user'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  currentUser: CurrentUser = null;
  currentPage;
  picUrl;

  constructor(
    public authenticationService: AuthenticationService,
    public router: Router,
    public actRouter: ActivatedRoute,
    @Inject(BASE_PATH) public basePath: string,
  ) { }

  ngOnInit() {
        this.currentUser = this.authenticationService.getLoggedInUser();

        this.getCurrentPage();

        this.authenticationService.getLoggedUserObservable()
        .subscribe(response =>{

          this.currentUser.firstName = response.user.firstName;
          this.currentUser.lastName = response.user.lastName;

          if (response.pictureUrl === "")
          {
            this.currentUser.hasPicture = 0;
          }
          else
          {
            this.currentUser.hasPicture = 1;
            this.picUrl = response.pictureUrl;
          }
        } , error =>  {} );


  }

  // Displaying the right arrow when opening and closing the menu
  arrow = 'keyboard_arrow_down';
  menuOpened  () {
    this.arrow = 'keyboard_arrow_up';
  };
  menuClosed  () {
    this.arrow = 'keyboard_arrow_down';
  };

  getCurrentPage  () {
    let curPage = this.router.url;
    curPage = curPage.substring(1, ).toUpperCase();
    this.currentPage = curPage;
  };


}
