export class CurrentUser {


  firstName: string;
  lastName: string;
  userId: number;
  role: string;
  token: string;
  hasPicture: number;

}
