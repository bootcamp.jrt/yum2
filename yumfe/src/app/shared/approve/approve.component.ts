import { Component, OnInit, Input } from '@angular/core';
import {MdSnackBar} from '@angular/material';
import { MdDialogRef, MdDialog } from '@angular/material';
import * as remote from '../../remote';

@Component({
  selector: 'app-approve',
  templateUrl: './approve.component.html',
  styleUrls: ['./approve.component.scss']
})

export class ApproveComponent implements OnInit {

  @Input() inputUser: remote.UserExtended;
  dialogRef: MdDialogRef<any>;

  constructor(public adminApiService: remote.AdminApi, public dialog: MdDialog, public snackBar: MdSnackBar) {  }

  ngOnInit() {

  }

  // This Method handles the APPROVED status.
  // The method has the bool value BEFORE the click happens.
  // That means we must inverse the value.
  onSlideChange(force: boolean) {
    // The API wants a Model of type approval instead of a simple boolean.
    const approvalBoolModel: remote.Approval = {};
    approvalBoolModel.appr = !this.inputUser.user.approved;
    this.adminApiService.usersIdApprovePut(this.inputUser.id, approvalBoolModel, force)
    .subscribe(response =>{
      this.inputUser.user.approved = approvalBoolModel.appr;
      this.openSnackBar(this.inputUser.user.approved.toString());
     },
    error => this.onSlideHandleError(error.status) );
  }

  onSlideHandleError(code) {
    if (code === 405) {
      this.open(DialogOrdersDelDisappComponent);
      this.inputUser.user.approved = !this.inputUser.user.approved;
    }
    // Although we make a check and will not display that option.
    // Could also be implemented with snackbar
    else if (code === 400){
      this.open(DialogCantDeleteComponent);
      this.inputUser.user.approved = !this.inputUser.user.approved;
    }
  }

  // Method to open dialogs
  open(thatDialog) {
    this.dialogRef = this.dialog.open(thatDialog);

    // Execute actions from dialog button
    this.dialogRef.afterClosed().subscribe(result => {
      if (result === 'disapprove') {
        this.onSlideChange(false);
      }
      else if (result === 'delOrders') {
        this.onSlideChange(true);
      }
      this.dialogRef = null;
    });
  }

 openSnackBar  (msgType: string) {
    if (msgType==='true') {
          this.snackBar.open('User approved successfully', 'Ok', {duration: 2000, extraClasses: ['snackBarSuccess'] });
    }
    else if (msgType==='false') {
          this.snackBar.open('User disapproved successfully', 'Ok', {duration: 2000, extraClasses: ['snackBarSuccess'] });
    }
  };

}

@Component({
  template: `
  <p>User has placed orders in the future.</p>
  <p>If you disapprove him the pending orders will be deleted.</p>
  <button type="button"class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" (click)="dialogRef.close('delOrders')">Delete</button>
  <button type="button"class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" (click)="dialogRef.close('cancel')">Cancel</button>`
})
export class DialogOrdersDelDisappComponent {
  constructor(public dialogRef: MdDialogRef<any>) { }
}

@Component({
  template: `
  <p>User cannot be disapproved.</p>
  <button type="button"class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" (click)="dialogRef.close()">Ok</button>`
})
export class DialogCantDeleteComponent {
  constructor(public dialogRef: MdDialogRef<any>) { }
}

