import { Pipe, PipeTransform } from '@angular/core';

/**
 * Map to Iteratble Pipe
 *
 * It accepts Objects and [Maps](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map)

 *
 */

@Pipe({ name: 'mapToIterable' })
export class MapToIterable implements PipeTransform {
  transform(value) {
    const result = [];

      value.forEach((value: any, key: any) => {
          result.push(value);
      });

    return result;
  }
}
