import { Injectable, Inject } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import { BASE_PATH } from '../remote/variables';


import * as remote from '../remote';

const currUser = 'currentUser2';

@Injectable()
export class AuthenticationService {

  private displayedUser : DisplayedUser = { user: {} };
  private loggedUserSubject = new BehaviorSubject<DisplayedUser>(this.displayedUser);
  user;
  constructor(
    private authService: remote.AuthApi,
    private conf: remote.Configuration,
    public router: Router,
    @Inject(BASE_PATH) public basePath: string,
  ) { }

  getLoggedUserObservable(): Observable<DisplayedUser> {
    return this.loggedUserSubject.asObservable();
  }


  login(username: string, password: string): Observable<boolean> {

    let creds: remote.Login = { email: username, password: password };
    return this.authService.authLoginPost(creds).map(loggedUserInfo => {
      // login successful if there's a jwt token in the response
      if (loggedUserInfo.token) {
        // store username and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem(currUser, JSON.stringify({
          firstName: loggedUserInfo.firstName,
          lastName: loggedUserInfo.lastName,
          userId: loggedUserInfo.userId,
          token: loggedUserInfo.token,
          role: loggedUserInfo.role,
          hasPicture: loggedUserInfo.hasPicture
        }));

        // Initialize the display user that will be handled by the subject
        this.displayedUser.user = loggedUserInfo;
        if (loggedUserInfo.hasPicture === 1) {
          this.displayedUser.pictureUrl = this.basePath + '/settings/picture/token?token=' + this.getToken() +  "&ts="+(new Date().getTime());
        }
        else {
          this.displayedUser.pictureUrl = "";
        }

        this.conf.apiKey = "Bearer " + loggedUserInfo.token;
        // return true to indicate successful login
        return true;
      } else {
        // return false to indicate failed login
        return false;
      }
    })
  }

  getToken(): String {
    var currentUser = JSON.parse(localStorage.getItem(currUser));
    var token = currentUser && currentUser.token;
    return token ? token : "";
  }

  isLogged(): boolean {
    let ls = localStorage.getItem(currUser);
    if (ls === null) {
      this.user = undefined;
      this.conf.apiKey = undefined;
      return false;
    }
    this.user = JSON.parse(ls);
    return ( (this.user !== undefined) && this.user.token !== undefined);
  }

  getLoggedInRole(): String {
    if (!this.isLogged()) return null;
    return this.user ? this.user.role : null;
  }
  getLoggedInUser() {
    return this.isLogged() ? this.user : undefined;
  }

  bootstrapUser(): void {
    let token = this.getToken();
    if (token !== "") {
      this.conf.apiKey = "Bearer " + token;
    }
    var currentUser = JSON.parse(localStorage.getItem(currUser));
    if (currentUser !== null ) this.prepareUrl(currentUser);
  }

  logout(): void {
    // clear token remove user from local storage to log user out...
    localStorage.removeItem(currUser);
    this.conf.apiKey = undefined;
    // ...and return to the login page
    this.router.navigate(['/login']);
  }

  updateUserDetails(firstName: string, lastName: string) {
    let cUser = JSON.parse(localStorage.currentUser2); // Get user object from storage
    cUser.firstName = firstName;
    cUser.lastName = lastName;
    localStorage.setItem(currUser, JSON.stringify(cUser));  //put the object back

    this.displayedUser.user.firstName = firstName;
    this.displayedUser.user.lastName = lastName;
    this.loggedUserSubject.next(this.displayedUser);

  }


  prepareUrl(cUser) {
    if (cUser.hasPicture === 1) // hasPicture is TRUE
    {
      this.displayedUser.pictureUrl = this.basePath + '/settings/picture/token?token=' + this.getToken() +  "&ts="+(new Date().getTime());
    }
    else
    {
      this.displayedUser.pictureUrl = "";
    }
    this.displayedUser.user = cUser;
    this.loggedUserSubject.next(this.displayedUser);

  }

  updateHasPicture(state): void {
    let cUser = JSON.parse(localStorage.currentUser2); // Get user object from storage
    cUser.hasPicture = state;
    localStorage.setItem(currUser, JSON.stringify(cUser));  //put the object back

    this.prepareUrl(cUser);



  }

}

export interface DisplayedUser {
    user?: remote.LoggedUserInfo;
    pictureUrl?: string;

}
