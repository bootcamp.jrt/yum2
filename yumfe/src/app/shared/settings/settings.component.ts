import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';
import { Router } from '@angular/router';

import { AuthenticationService } from '../authentication.service';
import * as remote from '../../remote';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})

export class SettingsComponent implements OnInit {

  // User date from the Auth service
  currentUser;
  // The GET method returns this user DTO
  userToUpdate: remote.UserGet = {};
  // The profile component takes this DTO as input
  userUpdate: remote.UserIdPutByAdmin = { user: {} };
  // The user type DTO that the PUT method takes as an argument
  userUpdateFinal: remote.UserPut = {};

  // Variables that synchronize the viewing of various components
  loading = true;
  disableSubmit = true;
  disablePwdSubmit = false;
  //formChanged = false;
  showSmallSpinner = false;

  // Heading variable
  greeting = '';
  // Second form group
  pwdGroup: FormGroup;

  constructor(
    public authenticationService: AuthenticationService,
    public hungryApiService: remote.HungryApi,
    public fb: FormBuilder,
    public snackBar: MdSnackBar,
    public routerRedirect: Router) { }

  ngOnInit() {
    this.currentUser = this.authenticationService.getLoggedInUser();
    // We make an additional api call to get the user data, in order to ensure
    // that we have the latest version
    this.getUserToUpdate();

    this.setupPwdForm();
  }

  // Get the latest version of the user details
  getUserToUpdate  () {
    this.hungryApiService.settingsGet()
      .subscribe(user => {
        this.userToUpdate = user;
        this.fromGetToInput();
        this.setRoleHeading();
        this.loading = false;
      });
  };

  // Transform the DTO that the Form returns to the DTO that the PUT API wants
  fromFormToPut  () {
    this.userUpdateFinal.version = this.userUpdate.version;
    this.userUpdateFinal.firstName = this.userUpdate.user.firstName;
    this.userUpdateFinal.lastName = this.userUpdate.user.lastName;
    this.userUpdateFinal.email = this.userUpdate.user.email;
    this.userUpdateFinal.role = null;
  };

  // Transform the DTO that GET returns to the DTO the profile component wants
  fromGetToInput  () {
    this.userUpdate = { user: {}};
    this.userUpdate.version = this.userToUpdate.version;
    this.userUpdate.user.firstName = this.userToUpdate.firstName;
    this.userUpdate.user.lastName = this.userToUpdate.lastName;
    this.userUpdate.user.lastName = this.userToUpdate.lastName;
    this.userUpdate.user.email = this.userToUpdate.email;
    this.userUpdate.user.role = null;
    this.userUpdate.user.version = this.userToUpdate.version;
    this.userUpdate.user.hasPicture = this.userToUpdate.hasPicture;

  };

  // Method that set the heading string based on role
  setRoleHeading  () {
    switch (this.userToUpdate.role) {
      case 'admin':
        this.greeting = 'You are an Administrator';
        break;
      case 'chef':
        this.greeting = 'You are a Chef';
        break;
      default:
        this.greeting = 'You are a Hungry user';
    }
  };

  // Method called to submit the changes to the user
  onSubmit  () {
    this.disableSubmit = true;
    this.disablePwdSubmit = true;
    this.showSmallSpinner = true;
    this.fromFormToPut();
    console.log(this.userUpdateFinal);
    this.hungryApiService.settingsPut(this.userUpdateFinal)
      .subscribe(response => { this.updateUserHandle(); this.authenticationService.updateUserDetails(this.userUpdateFinal.firstName, this.userUpdateFinal.lastName); }
      , error => this.onSubmitErrorHandling(error));
  };

  // Actions to execute after the successful update of the user
  updateUserHandle  () {
    this.showSmallSpinner = false;
    this.disableSubmit = false;
    this.disablePwdSubmit = false;
    this.openSnackBar('putok');


    // After a succesful update, return to the Home page
    this.routerRedirect.navigate(['/']);
  };

  // Handle errors when updating a user
  onSubmitErrorHandling (error) {
    this.showSmallSpinner = false;
    this.disableSubmit = false;
    this.disablePwdSubmit = false;
    this.openSnackBar(error);
    if (error.status === 409) {
      var latestUserDTO = JSON.parse(error._body);
      this.userUpdate.version = latestUserDTO.version;
      this.userUpdate.user.version = latestUserDTO.version;
      this.userUpdate.user.firstName = latestUserDTO.firstName;
      this.userUpdate.user.lastName = latestUserDTO.lastName;
      this.userUpdate.user.email = latestUserDTO.email;
      this.userUpdate.user.hasPicture = latestUserDTO.hasPicture;
    }
  };

  openSnackBar (msgType: any) {
    // Success snackbar
    if (msgType === 'putok') {
      this.snackBar.open('User was updated successfully', 'Ok', { duration: 2000, extraClasses: ['snackBarSuccess'] });
    }
    // Error snackbars
    else {
      var error = JSON.parse(msgType._body);
      if (msgType.status === 400 || msgType.status === 404) {
        this.snackBar.open(error.message, 'Ok', { extraClasses: ['snackBarError'] });
      }
      // The concurrent modification returns a DTO instead of a message
      else if (msgType.status === 409) {
        this.snackBar.open('User already updated. The latest version is returned', 'Ok', { extraClasses: ['snackBarError'] });
      }
      // Catch all internal server errors
      else if (msgType.status === 500) {
        this.snackBar.open('Something went wrong internally', 'Ok', { extraClasses: ['snackBarError'] });
      }
      // Catch the really obscure errors
      else {
        this.snackBar.open('Unknown error', 'Ok', { extraClasses: ['snackBarError'] });
      }
    }
  };

  // Set up the second form of the settings screen
  setupPwdForm  () {
    this.pwdGroup = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(6)]],
      rePassword: ['', [Validators.required, Validators.minLength(6)]]
    }, { validator: passwordMatcher });

    this.pwdGroup.valueChanges
      .subscribe(data => {


        console.log(this.userUpdateFinal.password);
        if (this.pwdGroup.valid) {
          if (data.password !== null)
          {
            this.userUpdateFinal.password = data.password;
          }
          this.disablePwdSubmit = false;
        }
        // If the password form is not valid disable the submit button
        else {
          this.disablePwdSubmit = true;
        }
      });
  };

  // Emit handler that waits for the form data to be transmited
  // When the form is valid, it activates the submit button
  handleUpdateData () {
    this.disableSubmit = false;
  };

  // Emit-handler that keeps the submit disabled while the form is not valid
  handleformState (state) {
    if (state === false) {
      this.disableSubmit = true;
    }
  };

  // Emit-handler that reload the user data when the picture is changed
  handlePicChanged() {
    // Get the latest version of the user
    this.getUserToUpdate();
  };

} // End of class

// Struct that helps password matching validation
export const passwordMatcher = (control): { [key: string]: boolean } => {
  const password = control.get('password');
  const confirm = control.get('rePassword');
  if (!password || !confirm) {
    return null;
  }
  if (password === null && confirm === null) {
    return null;
  }
  return password.value === confirm.value ? null : { nomatch: true };
};

