import { Component, OnInit, Input, EventEmitter, Output, Inject } from '@angular/core';
import { AuthenticationService } from '../../shared/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/switchMap';

import { FileUploader } from 'ng2-file-upload';
import { BASE_PATH } from '../../remote/variables';
import * as remote from '../../remote';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

  private _userUpdateData: remote.UserIdPutByAdmin = { user: {} };

  @Input()
  set userUpdateData(data: remote.UserIdPutByAdmin) {
    if (this._userUpdateData.user.hasPicture === 1) {
      this.refreshImg();
    }
    this._userUpdateData = data;
    this.setupForm();
  };

  get userUpdateData() {
    return this._userUpdateData;
  }

  // The user DTO does not pass the id, but we need it to retrieve the picture
  @Input() id;
  // This boolean will designate whether we enter the profile component from the admin page or the user settings
  @Input() cameFromAdmin;
  @Output() userUpdateDataChange = new EventEmitter<remote.UserIdPutByAdmin>();
  @Output() formStateOutput = new EventEmitter<boolean>();
  // Event signaling that the picture has changed
  @Output() picChanged = new EventEmitter<boolean>();

  profileGroup: FormGroup;
  myToken;
  uploader;
  uploadWheelShow = false;

  // Populate the role select
  roles = [
    { value: 'hungry', viewValue: 'Hungry' },
    { value: 'chef', viewValue: 'Chef' },
    { value: 'admin', viewValue: 'Administrator' }
  ];

  constructor(
    public router: ActivatedRoute,
    public adminApiService: remote.AdminApi,
    public hungryApiService: remote.HungryApi,
    public snackBar: MdSnackBar,
    public fb: FormBuilder,
    @Inject(BASE_PATH) public basePath: string,
    public authService: AuthenticationService) { }

  ngOnInit() {
    this.setupForm();

    // Decide which API will be called when uploading
    this.getApiSuffix();
    // Construct full Url for API call
    this.picUrl = this.completeGetUrl();

    // Call the uploader will the necessary parameters
    this.uploader = new FileUploader(
      {
        url: this.basePath + this.apiUrlSuffix,
        autoUpload: true,
        removeAfterUpload: true,
        authToken: "Bearer " + this.authService.getToken()
      });

    /*** Methods to be invoked on uploader events (success, error, etc...) ***/

    this.uploader.onSuccessItem = () => {
      // display success snack bar 'Profile picture was set succesfully.'
      this.openSnackBar('Profile picture was set succesfully.', 1);
      this.userUpdateData.user.hasPicture = 1;
      this.picChanged.emit();
      // If the upload happened from the settings page update the logged user
      if (!this.cameFromAdmin) {
        this.authService.updateHasPicture(1);
      }
      this.uploadWheelShow = false;
    }

    this.uploader.onAfterAddingFile = () => {
        this.uploadWheelShow = true;
    }

    this.uploader.onErrorItem = () => {
      this.snackBar.open('An unexpected error occured, please try again later.', 'OK', {extraClasses: ['snackBarError', 'snackBartext'] });
      this.uploadWheelShow = false;
    }

    this.myToken = this.authService.getToken();
  }

  /********  Url for picture GET  *******/

  picUrl; // Full picture url
  apiUrlSuffix; // Which Picture API to call

  // Method that decides which API will be called when uploading
  getApiSuffix() {
    if (!this.cameFromAdmin) {
      this.apiUrlSuffix = '/settings/picture';
    }
    else {
      this.apiUrlSuffix = '/users/' + this.id + '/picture';
    }
  };

  // Method that constructs full Url for API call
  private completeGetUrl(): string {
    return this.basePath + this.apiUrlSuffix + "/token?token=" + this.authService.getToken() + "&ts=" + (new Date().getTime());
  }

  // Method that forces refresh of the picture
  public refreshImg() {
    this.picUrl = this.completeGetUrl();
  }

  // Populate the form builder object and subscribe to form changes
  setupForm() {
    this.profileGroup = this.fb.group({
      role: [this.userUpdateData.user.role,],
      firstName: [this.userUpdateData.user.firstName, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      lastName: [this.userUpdateData.user.lastName, [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      email: [this.userUpdateData.user.email, [Validators.required, Validators.email]]
    });
    this.profileGroup.valueChanges
      .subscribe(data => {
        if (this.profileGroup.valid) {
          this.userUpdateData.user.firstName = data.firstName;
          this.userUpdateData.user.lastName = data.lastName;
          this.userUpdateData.user.email = data.email;
          this.userUpdateData.user.role = data.role;
          this.userUpdateDataChange.emit(this.userUpdateData);
        }
        // Keep the submit button outside the component disabled
        // as long as the form is invalid
        else {
          this.formStateOutput.emit(false);
        }
      });
  };

  deletePic() {
    if (this.cameFromAdmin) // Deletion from users component
    {
      this.adminApiService.usersIdPictureDelete(this.id)
        .subscribe(response => {
          this.handleDeletePic();
        },
        error => { });
    }
    else // Deletion from settingscomponent
    {
      this.hungryApiService.settingsPictureDelete()
        .subscribe(response => {
          this.handleDeletePic();
        },
        error => { });
    }
  }

  handleDeletePic() {
    this.openSnackBar('Profile picture was delete succesfully.', 1);
    // Notify the components that the user has not a picture anymore
    this.userUpdateData.user.hasPicture = 0;
    this.authService.updateHasPicture(0);
    // Also, you need to reload the user data
    this.picChanged.emit();
  }

  openSnackBar(msg: string, nmb: number) {
    if (nmb === 1) {
      this.snackBar.open(msg, 'Ok', {duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb === 2) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarError', 'snackBartext'] });
    }
  }

} // End class
