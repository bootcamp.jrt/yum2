import { Injectable, Inject, Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { MdDialogRef, MdDialog } from '@angular/material';
import { MD_DIALOG_DATA } from '@angular/material';
import { MdTabsModule } from '@angular/material';
import * as remote from '../remote';
import { GlobalSettingsService } from '../shared/globalsettings.service';

@Injectable()
export class TosService {

  termsPolicy: remote.TermsPolicy = {};
  dialogRef: MdDialogRef<any>;
  idxTerms: IdxTerms;

  constructor(
    private authService: remote.AuthApi,
    public dialog: MdDialog
  ) { }

  getTerms(selectedTab: number) {
    this.authService.authTermsGet().subscribe(data => {
      this.termsPolicy = data;
      this.open(TermsOfServiceComponent, this.termsPolicy, selectedTab);
    }, error => {
    })
  }

  open(dialog, msg, seltab) {
    this.idxTerms = {
      content: msg,
      idx: seltab
    };
    this.dialogRef = this.dialog.open(dialog, { data: this.idxTerms, width: '900px', height: '600px' });
    this.dialogRef.afterClosed().subscribe(result => {
      this.dialogRef = null;
    })
  }


}

@Component({
  template: `
  <tabs-overview [firstTab]="data.content.terms" [secondTab]="data.content.policy" [index]="data.idx"></tabs-overview>
  <br><br>
  <button type="button" md-raised-button color="accent" (click)="dialogRef.close('cancel')">Ok</button>`,
})
export class TermsOfServiceComponent {
  constructor( @Inject(MD_DIALOG_DATA) public data: any, public dialogRef: MdDialogRef<any>) { }
}

@Component({
  selector: 'tabs-overview',
  template: `<md-tab-group selectedIndex="{{index}}">
  <md-tab label="Terms and Conditions"><span [innerHTML]="firstTab"></span></md-tab>
  <md-tab label="Privacy policy"><span [innerHTML]="secondTab"></span></md-tab>
</md-tab-group>`,
})
export class TabsOverview {
  @Input() firstTab;
  @Input() secondTab;
  @Input() index;
}

export interface IdxTerms {
  content: remote.TermsPolicy;
  idx: number;
}

