import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HungryApi } from './remote'
import { AuthApi } from './remote';
import { AdminApi } from './remote';
import { ChefApi } from './remote';
import { GlobalSettingsService } from 'app/shared/globalsettings.service';
import 'hammerjs';
import { SharedModule } from './shared/shared.module';
import { HungryModule } from './hungry/hungry.module';
import { ChefModule } from './chef/chef.module';
import { AdminModule } from './admin/admin.module';
import { AppRouting } from './app.routing';
import { AppComponent } from './app.component';
import { LoginComponent } from './anon/login/login.component';
import { RegisterComponent } from './anon/register/register.component';
import { ForgotpwdComponent } from 'app/anon/forgotpwd/forgotpwd.component';
import { ResetPasswordComponent } from '../app/anon/reset-password/reset-password.component';
import { SettingsComponent } from 'app/admin/settings/settings.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppGuard, CanLoginGuard, IsLoggedInGuard, CanActivateChefGuard, CanActivateAdminGuard } from "app/app.guard";
import { ReactiveFormsModule } from '@angular/forms';
import { BASE_PATH } from './remote/variables';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotpwdComponent,
    ResetPasswordComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HungryModule,
    AdminModule,
    ChefModule,
    AppRouting,
    SharedModule,
    FlexLayoutModule,
    ReactiveFormsModule
    // FileUploadModule
  ],
  providers: [
    AuthApi,
    AdminApi,
    ChefApi,
    HungryApi,
    GlobalSettingsService,
    AppGuard,
    CanLoginGuard,
    IsLoggedInGuard,
    CanActivateChefGuard,
    CanActivateAdminGuard,
    { provide: BASE_PATH, useValue: "http://localhost:8082/api" }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
