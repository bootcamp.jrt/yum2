/**
 * Yum Food Orders
 * **Yum application, order food daily from the best chef in town**  This API is used by the angular.io client, and is not meant to be used otherwise.  Find source code of this API [here](http://gitlab/)  Copyright (C) 2017 JR Technologies. 
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

import { Inject, Injectable, Optional }                      from '@angular/core';
import { Http, Headers, URLSearchParams }                    from '@angular/http';
import { RequestMethod, RequestOptions, RequestOptionsArgs } from '@angular/http';
import { Response, ResponseContentType }                     from '@angular/http';

import { Observable }                                        from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import * as models                                           from '../model/models';
import { BASE_PATH, COLLECTION_FORMATS }                     from '../variables';
import { Configuration }                                     from '../configuration';

/* tslint:disable:no-unused-variable member-ordering */


@Injectable()
export class AdminApi {
    protected basePath = 'http://localhost:8082/api';
    public defaultHeaders: Headers = new Headers();
    public configuration: Configuration = new Configuration();

    constructor(protected http: Http, @Optional()@Inject(BASE_PATH) basePath: string, @Optional() configuration: Configuration) {
        if (basePath) {
            this.basePath = basePath;
        }
        if (configuration) {
            this.configuration = configuration;
        }
    }


    /**
     * Changes the global settings.
     * Changes the global settings.
     * @param settings The updated settings.
     */
    public globalsettingsPut(settings?: models.GlobalSettings, extraHttpRequestParams?: any): Observable<{}> {
        return this.globalsettingsPutWithHttpInfo(settings, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Upload user picture. - multipart/form-data
     * 
     * @param upfile The file to upload.
     */
    public settingsPicturePost(upfile: any, extraHttpRequestParams?: any): Observable<{}> {
        return this.settingsPicturePostWithHttpInfo(upfile, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Gets a list of all the users
     * Gets the account information of all the users.
     * @param sorting Sort the list of users.
     * @param page Show the users on the selected page.
     * @param pagesize Show the selected amount of users per page.
     * @param order Select the ordering.
     */
    public usersGet(sorting: string, page: number, pagesize: number, order: string, extraHttpRequestParams?: any): Observable<models.AllUsers> {
        return this.usersGetWithHttpInfo(sorting, page, pagesize, order, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Select user approval.
     * Approves or not a specific user.
     * @param id The id of the user.
     * @param approval User approval.
     * @param force Delete user&#39;s future orders, and set user to dissaproved.
     */
    public usersIdApprovePut(id: number, approval?: models.Approval, force?: boolean, extraHttpRequestParams?: any): Observable<{}> {
        return this.usersIdApprovePutWithHttpInfo(id, approval, force, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Deletes a user.
     * deletes a user in the database
     * @param id The id of the user to delete
     * @param force Delete user&#39;s future orders.
     */
    public usersIdDelete(id: number, force?: boolean, extraHttpRequestParams?: any): Observable<{}> {
        return this.usersIdDeleteWithHttpInfo(id, force, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Resets password.
     * Resets the password of a specific user.
     * @param id The id of the user.
     */
    public usersIdForgotpwdPost(id: number, extraHttpRequestParams?: any): Observable<{}> {
        return this.usersIdForgotpwdPostWithHttpInfo(id, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Gets the specific user
     * Gets the specific user by the id.
     * @param id The user&#39;s id that you want to search.
     */
    public usersIdGet(id: number, extraHttpRequestParams?: any): Observable<models.UserGetId> {
        return this.usersIdGetWithHttpInfo(id, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Deletes a user.
     * deletes a user in the database
     * @param id The id of the user to delete
     */
    public usersIdPictureDelete(id: number, extraHttpRequestParams?: any): Observable<{}> {
        return this.usersIdPictureDeleteWithHttpInfo(id, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Upload user picture. - multipart/form-data
     * 
     * @param id The id of the user.
     * @param upfile The file to upload.
     */
    public usersIdPicturePost(id: number, upfile: any, extraHttpRequestParams?: any): Observable<{}> {
        return this.usersIdPicturePostWithHttpInfo(id, upfile, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Admin get a picture of a single user.
     * 
     * @param id The id of the user.
     * @param token The token to validate the user.
     */
    public usersIdPictureTokenGet(id: number, token: string, extraHttpRequestParams?: any): Observable<{}> {
        return this.usersIdPictureTokenGetWithHttpInfo(id, token, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Changes the settings of a user.
     * Changes the account settings of the current user.
     * @param id The id of the food item to update
     * @param userToUpdate The user to update.
     */
    public usersIdPut(id: number, userToUpdate?: models.UserIdPutByAdmin, extraHttpRequestParams?: any): Observable<{}> {
        return this.usersIdPutWithHttpInfo(id, userToUpdate, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Creates a new user
     * creates a new user in the database
     * @param user The user to create.
     */
    public usersPost(user?: models.UserPut, extraHttpRequestParams?: any): Observable<models.UserPut> {
        return this.usersPostWithHttpInfo(user, extraHttpRequestParams)
            .map((response: Response) => {
                if (response.status === 204) {
                    return undefined;
                } else {
                    return response.json();
                }
            });
    }

    /**
     * Changes the global settings.
     * Changes the global settings.
     * @param settings The updated settings.
     */
    public globalsettingsPutWithHttpInfo(settings?: models.GlobalSettings, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/globalsettings`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        headers.set('Content-Type', 'application/json');

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Put,
            headers: headers,
            body: settings == null ? '' : JSON.stringify(settings), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Upload user picture. - multipart/form-data
     * 
     * @param upfile The file to upload.
     */
    public settingsPicturePostWithHttpInfo(upfile: any, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/settings/picture`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        let formParams = new URLSearchParams();

        // verify required parameter 'upfile' is not null or undefined
        if (upfile === null || upfile === undefined) {
            throw new Error('Required parameter upfile was null or undefined when calling settingsPicturePost.');
        }
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        headers.set('Content-Type', 'application/x-www-form-urlencoded');

        if (upfile !== undefined) {
            formParams.set('upfile', <any>upfile);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Post,
            headers: headers,
            body: formParams.toString(),
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Gets a list of all the users
     * Gets the account information of all the users.
     * @param sorting Sort the list of users.
     * @param page Show the users on the selected page.
     * @param pagesize Show the selected amount of users per page.
     * @param order Select the ordering.
     */
    public usersGetWithHttpInfo(sorting: string, page: number, pagesize: number, order: string, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'sorting' is not null or undefined
        if (sorting === null || sorting === undefined) {
            throw new Error('Required parameter sorting was null or undefined when calling usersGet.');
        }
        // verify required parameter 'page' is not null or undefined
        if (page === null || page === undefined) {
            throw new Error('Required parameter page was null or undefined when calling usersGet.');
        }
        // verify required parameter 'pagesize' is not null or undefined
        if (pagesize === null || pagesize === undefined) {
            throw new Error('Required parameter pagesize was null or undefined when calling usersGet.');
        }
        // verify required parameter 'order' is not null or undefined
        if (order === null || order === undefined) {
            throw new Error('Required parameter order was null or undefined when calling usersGet.');
        }
        if (sorting !== undefined) {        
            queryParameters.set('sorting', <any>sorting);
        }

        if (page !== undefined) {
            queryParameters.set('page', <any>page);
        }

        if (pagesize !== undefined) {
            queryParameters.set('pagesize', <any>pagesize);
        }

        if (order !== undefined) {
            queryParameters.set('order', <any>order);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Select user approval.
     * Approves or not a specific user.
     * @param id The id of the user.
     * @param approval User approval.
     * @param force Delete user&#39;s future orders, and set user to dissaproved.
     */
    public usersIdApprovePutWithHttpInfo(id: number, approval?: models.Approval, force?: boolean, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}/approve`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdApprovePut.');
        }
        if (force !== undefined) {
            queryParameters.set('force', <any>force);
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        headers.set('Content-Type', 'application/json');

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Put,
            headers: headers,
            body: approval == null ? '' : JSON.stringify(approval), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Deletes a user.
     * deletes a user in the database
     * @param id The id of the user to delete
     * @param force Delete user&#39;s future orders.
     */
    public usersIdDeleteWithHttpInfo(id: number, force?: boolean, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdDelete.');
        }
        if (force !== undefined) {
         
                queryParameters.set('force', <any>force);
            
        }

        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Delete,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Resets password.
     * Resets the password of a specific user.
     * @param id The id of the user.
     */
    public usersIdForgotpwdPostWithHttpInfo(id: number, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}/forgotpwd`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdForgotpwdPost.');
        }
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Post,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Gets the specific user
     * Gets the specific user by the id.
     * @param id The user&#39;s id that you want to search.
     */
    public usersIdGetWithHttpInfo(id: number, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdGet.');
        }
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Deletes a user.
     * deletes a user in the database
     * @param id The id of the user to delete
     */
    public usersIdPictureDeleteWithHttpInfo(id: number, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}/picture`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdPictureDelete.');
        }
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Delete,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Upload user picture. - multipart/form-data
     * 
     * @param id The id of the user.
     * @param upfile The file to upload.
     */
    public usersIdPicturePostWithHttpInfo(id: number, upfile: any, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}/picture`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        let formParams = new URLSearchParams();

        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdPicturePost.');
        }
        // verify required parameter 'upfile' is not null or undefined
        if (upfile === null || upfile === undefined) {
            throw new Error('Required parameter upfile was null or undefined when calling usersIdPicturePost.');
        }
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        headers.set('Content-Type', 'application/x-www-form-urlencoded');

        if (upfile !== undefined) {
            formParams.set('upfile', <any>upfile);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Post,
            headers: headers,
            body: formParams.toString(),
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Admin get a picture of a single user.
     * 
     * @param id The id of the user.
     * @param token The token to validate the user.
     */
    public usersIdPictureTokenGetWithHttpInfo(id: number, token: string, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}/picture/${token}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdPictureTokenGet.');
        }
        // verify required parameter 'token' is not null or undefined
        if (token === null || token === undefined) {
            throw new Error('Required parameter token was null or undefined when calling usersIdPictureTokenGet.');
        }
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Get,
            headers: headers,
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Changes the settings of a user.
     * Changes the account settings of the current user.
     * @param id The id of the food item to update
     * @param userToUpdate The user to update.
     */
    public usersIdPutWithHttpInfo(id: number, userToUpdate?: models.UserIdPutByAdmin, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users/${id}`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // verify required parameter 'id' is not null or undefined
        if (id === null || id === undefined) {
            throw new Error('Required parameter id was null or undefined when calling usersIdPut.');
        }
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        headers.set('Content-Type', 'application/json');

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Put,
            headers: headers,
            body: userToUpdate == null ? '' : JSON.stringify(userToUpdate), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

    /**
     * Creates a new user
     * creates a new user in the database
     * @param user The user to create.
     */
    public usersPostWithHttpInfo(user?: models.UserPut, extraHttpRequestParams?: any): Observable<Response> {
        const path = this.basePath + `/users`;

        let queryParameters = new URLSearchParams();
        let headers = new Headers(this.defaultHeaders.toJSON()); // https://github.com/angular/angular/issues/6845
        // to determine the Content-Type header
        let consumes: string[] = [
        ];

        // to determine the Accept header
        let produces: string[] = [
            'application/json'
        ];

        // authentication (Bearer) required
        if (this.configuration.apiKey) {
            headers.set('Authorization', this.configuration.apiKey);
        }

        headers.set('Content-Type', 'application/json');

        let requestOptions: RequestOptionsArgs = new RequestOptions({
            method: RequestMethod.Post,
            headers: headers,
            body: user == null ? '' : JSON.stringify(user), // https://github.com/angular/angular/issues/10612
            search: queryParameters
        });

        // https://github.com/swagger-api/swagger-codegen/issues/4037
        if (extraHttpRequestParams) {
            requestOptions = (<any>Object).assign(requestOptions, extraHttpRequestParams);
        }

        return this.http.request(path, requestOptions);
    }

}
