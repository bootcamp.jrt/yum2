import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chef-nav-bar',
  templateUrl: './chef-nav-bar.component.html',
  styleUrls: ['./chef-nav-bar.component.scss']
})
export class ChefNavBarComponent implements OnInit {

  constructor(public route: Router) { }

  ngOnInit() {
  }

}
