import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.service';
import { ActivatedRoute } from '@angular/router';
import { HistoryComponent } from '../../hungry/history/history.component';
import * as remote from '../../remote';


@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.scss']
})
export class MenusComponent implements OnInit {

  dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  daysAfterMonday: Array<number> = [];
  foodsWithVersion: remote.Foods;
  currMonthNumber: number;
  currYear: number;
  monthlyHistory: Array<remote.DailyMenu>;
  private sub: any;
  loading = true;



  constructor(
    public chefService: remote.ChefApi,
    public authenticationService: AuthenticationService,
    public router: ActivatedRoute,
    public route: Router) {
     }

  ngOnInit() {
    this.refreshAll();
  }

//get the list of foods from the database
  getFoods() {
    this.chefService.foodsGet(false, 0, 2000, 'all', 'none').subscribe(getFoods => {
      this.foodsWithVersion = getFoods;
    }, error => {});
  }

//find on which day starts the asked month. This is used to calculate the number
//of empty divs in the grid list
  monthStartCalculate() {
    this.daysAfterMonday = [];
    let d = new Date(this.currYear, this.currMonthNumber - 1, 1);
    let size = 0;
    if (d.getDay() === 2) {
      size = 1;
    } else if (d.getDay() === 3) {
      size = 2;
    } else if (d.getDay() === 4) {
      size = 3;
    } else if (d.getDay() === 5) {
      size = 4;
    } else {
      size = 0;
    }
    for (let i = 0; i < size; i++) {
      this.daysAfterMonday.push(1);
    }
  }

//the back end API returns a menu for each day of the month.
//We remove the weekends from our grid list.
  removeWeekendsFromHistory(): Array<remote.DailyMenu> {
    let menu = this.monthlyHistory;
    let index = 0;
    for (let i = this.monthlyHistory.length - 1; i >= 0; i--) {
      let d = new Date(this.currYear, this.currMonthNumber - 1, i + 1);
      if (d.getDay() === 6 || d.getDay() === 0) {
        menu.splice(i, 1);
      }
    }
    return menu;
  }

//returns the month number
  getMonthNumber(): number {
    let date = new Date();
    return date.getMonth() + 1;
  }

//returns the year number
  getYearNumber(): number {
    let date = new Date();

    return date.getFullYear();
  }

//returns the previous year
  getPreviousYear(): number {
    if (this.currMonthNumber === 1)
      return this.currYear - 1;
    return this.currYear;
  }

//returns the next year
  getNextYear(): number {
    if (this.currMonthNumber === 12)
      return this.currYear + 1;
    return this.currYear;
  }

  //returns the previous month
  getPreviousMonth(): number {
    if (this.currMonthNumber === 1)
      return 12;
    return this.currMonthNumber - 1;
  }

//returns the next month
  getNextMonth(): number {
    if (this.currMonthNumber === 12)
      return 1;
    return this.currMonthNumber + 1;
  }

//check for parameters. If no parameters are passed,
//redirect to the current month.
  refreshAll() {
    this.sub = this.router.params.subscribe(params => {

      if (params.year && params.month) {

        this.currYear = parseInt(params.year);
        this.currMonthNumber = parseInt(params.month);

        if (this.currMonthNumber === this.getMonthNumber() && this.currYear === this.getYearNumber()) {
          this.route.navigate(['/chef/menus']);
        }
        else {
          this.chefService.dailyMenusMonthlyMonthGet(this.currMonthNumber + "-" + this.currYear).subscribe(dailymenus => {
            this.monthlyHistory = dailymenus;
            this.monthStartCalculate();
            this.removeWeekendsFromHistory();
            this.loading = false;
          }, error => {
            this.loading = false;
          });
        }

      } else {

        this.currMonthNumber = this.getMonthNumber();
        this.currYear = this.getYearNumber();

        this.chefService.dailyMenusMonthlyGet().subscribe(dailymenus => {
          this.monthlyHistory = dailymenus;
          this.monthStartCalculate();
          this.removeWeekendsFromHistory();
          this.loading = false;
        }, error => this.loading = false);

      }

    });
    this.getFoods();
  }

}
