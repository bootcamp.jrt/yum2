import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../shared/authentication.service';
import { ActivatedRoute } from '@angular/router';
import { HistoryComponent } from '../../../hungry/history/history.component';
import { MdSnackBar } from '@angular/material';
import * as remote from '../../../remote';
import { GlobalSettingsService } from "app/shared/globalsettings.service";

@Component({
  selector: 'app-daily-menu-component',
  templateUrl: './daily-menu-component.component.html',
  styleUrls: ['./daily-menu-component.component.scss']
})
export class DailyMenuComponentComponent implements OnInit {

  @Input() dailyHistory: remote.DailyMenu;
  @Input() foodsWithVersion: remote.Foods;
  @Output() refresh  = new EventEmitter();


  foodsToShow: Array<remote.FoodsFoodList>;

//the foods the user has selected from the dropdown list
  selectedFoods: Array<string>;

  orderedFoods: Array<string> = [];
  selectedValue: string;
  initialized: boolean = false;

//3 hashmaps that help during tthe display
  foodsMap = new Map<string, number>();
  idToFood = new Map<number, remote.Food>();
  foodToIcon = new Map<string, string>();

  foodItem: remote.FoodsFoodList = {};
  foodOfFoodItem: remote.Food = {};

//is true if no menu exists for the current day
  emptyMenu: boolean;

//the values needed for the API call during a POST
  newMenu: remote.DailyMenu = { dailyMenuId: 0 };
  newFoods: Array<remote.DailyMenuFoods> = [];
  newFood: remote.DailyMenuFoods = {};

//the values needed for the API call during a PUT
  updateMenu: remote.DailyMenuUpdate = {};
  updateFoods: Array<remote.DailyMenuUpdateFoods> = [];
  updateFood: remote.DailyMenuUpdateFoods = {};

  loading: boolean = false;
  time: Date;

  constructor(
    public chefService: remote.ChefApi,
    public authenticationService: AuthenticationService,
    public router: ActivatedRoute,
    public route: Router, public snackBar: MdSnackBar,
    public globalSettingsService: GlobalSettingsService) {
      this.globalSettingsService.getDeadLine().subscribe(time => this.time = time);
     }

  ngOnInit() {
    this.onStart();
  }

  onStart() {
    this.getSelectedFoods();
    this.getInitialFoods();
    this.checkIfMenuIsEMpty();
    this.addToOrderFoods();
  }

//check if a menu exists for the current day
  checkIfMenuIsEMpty() {
    if (this.dailyHistory.foods.length === 0) {
      this.emptyMenu = true;
    } else {
      this.emptyMenu = false;
    }
  }

//on init the the list of foods from DB
  getInitialFoods() {
    this.chefService.foodsGet(false, 0, 2000, 'all', 'none').subscribe(getFoodsFromDB => {
      this.foodsWithVersion = getFoodsFromDB;
      this.populateFoodsMap();
      this.populateIdToFood();
      if (this.initialized === false) {
        this.foodsToShowInitialize();
      }
    }, error => {
    });
  }

//this function is called when the chef clicks on the dropdown list.
//if the food list has not changed, the server returns a 304 and we use
//the previously populated food list
  getFoods() {
    this.loading = true;
    this.chefService.foodsGet(false, 0, 2000, 'all', 'none', this.foodsWithVersion.version).subscribe(getFoodsFromDB => {
      this.foodsWithVersion = {};
      this.foodsWithVersion = getFoodsFromDB;
      this.populateFoodsMap();
      this.populateIdToFood();
      this.foodsToShowInitialize();
      this.loading = false;
    }, error => {
      this.loading = false;
    });
  }

//if the chef clicks on a food from the list,
//the same food should not display again in that list
  onFoodSelection(newFood: string) {
    this.selectedFoods.push(newFood);
    this.foodsToShowAddUpdate(newFood);
  }

//if the chef removes a food from the menu, that food should
//be shown in the dropdown list
  onDelete(foodToDelete: string) {
    let foods = this.selectedFoods;
    for (let i = this.selectedFoods.length - 1; i >= 0; i--) {
      if (this.selectedFoods[i] === foodToDelete) {
        foods.splice(i, 1);
        this.selectedFoods = foods;
        this.foodsToShowDeleteUpdate(foodToDelete);
      }
    }
    this.selectedValue = null;
  }

  checkIfFoodAlreadyInList(food: remote.Food): boolean {
    for (let i = 0; i < this.dailyHistory.foods.length; i++) {
      if (food.foodName === this.dailyHistory.foods[i].foodName) {
        return true;
      }
    }
    return false;
  }

  foodsToShowInitialize() {
    let shorterList = [];
    for (const food of this.foodsWithVersion.foodList) {
      shorterList.push(food);
    }
    for (let i = this.foodsWithVersion.foodList.length - 1; i >= 0; i--) {
      for (let j = 0; j < this.dailyHistory.foods.length; j++) {
        if (this.foodsWithVersion.foodList[i].food.foodName === this.dailyHistory.foods[j].foodName) {
          shorterList.splice(i, 1);
        }
      }
    }
    this.foodsToShow = shorterList;
    this.initialized = true;
  }

  foodsToShowAddUpdate(newFood: string) {
    let shorterList = this.foodsToShow;
    for (let i = this.foodsToShow.length - 1; i >= 0; i--) {
      if (this.foodsToShow[i].food.foodName === newFood) {
        shorterList.splice(i, 1);
        this.foodsToShow = shorterList;
        break;
      }
    }
  }

  foodsToShowDeleteUpdate(deletedFood: string) {
    this.foodOfFoodItem.foodName = deletedFood;
    this.foodItem.food = this.foodOfFoodItem;
    this.foodsToShow.push(this.foodItem);
    this.foodItem = {};
    this.foodOfFoodItem = {};
  }

  getSelectedFoods() {
    this.selectedFoods = [];
    for (const food of this.dailyHistory.foods) {
      this.selectedFoods.push(food.foodName);
    }
  }

  populateFoodsMap() {
    for (let i = 0; i < this.foodsWithVersion.foodList.length; i++) {
      this.foodsMap.set(this.foodsWithVersion.foodList[i].food.foodName, this.foodsWithVersion.foodList[i].food.foodId);
      this.foodToIcon.set(this.foodsWithVersion.foodList[i].food.foodName, this.foodsWithVersion.foodList[i].food.foodType);
    }
  }

  populateIdToFood() {
    for (let i = 0; i < this.foodsWithVersion.foodList.length; i++) {
      this.idToFood.set(this.foodsWithVersion.foodList[i].food.foodId, this.foodsWithVersion.foodList[i].food);
    }
  }

/************************************** */
//snackbars
  openSuccessSnackBar() {
    this.openSnackBar('Menu was updated successfully',1);
  };

  openErrorSnackBar(code) {
    if (code === 400) {
      this.openSnackBar('Bad request. Try again',2);
    } else if (code === 409) {
      this.openSnackBar('A menu was already created for that day.',3);
    }
  }

  openPutErrorSnackBar(code) {
    if (code.status === 400) {
      this.openSnackBar('Bad request in put. Try again.',4);
    } else if (code.status === 409) {
      this.openSnackBar('Concurrent modification error. Try again.',5);
    } else if (code.status === 410) {
      if (JSON.parse(code._body).dailyMenuId === 0) {
       this.openSnackBar('You deleted this menu earlier. Please recreate it again if this is what was intended.',6);
      } else {
        this.openSnackBar('You deleted this menu earlier, and recreated it. Here is the new one.',7);
      }
    }
  }

  openSnackBar(msg: string, nmb: number) {

    if (nmb == 1) {
      this.snackBar.open(msg, 'Ok', {duration: 2000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb == 2) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarError', 'snackBartext'] });
    }else if (nmb == 3) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    }else if (nmb == 4) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarError', 'snackBartext'] });
    }else if (nmb == 5) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    }else if (nmb == 6) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    }else if (nmb == 7) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    }

  }
  /*************************************************************************************** */

//when chef clicks on submit a POST or PUT request should be initialized
  submitMenu() {
    if (this.emptyMenu === true) {
      this.menuPost();
    } else {
      this.menuPut();
    }
  }

//update the lists after a PUT
  updateHistory(response) {
    this.dailyHistory.version = response.version;
    this.dailyHistory.foods = [];
    for (const food of response.foods) {
      this.dailyHistory.foods.push(this.idToFood.get(food.foodId));
    }
    if (this.emptyMenu === true) {
      this.dailyHistory.foods = [];
      this.dailyHistory.dailyMenuId = null;
    }
  }

//update the lists after a POST
  updateHistoryFromPost(menu) {
    this.dailyHistory.foods = [];
    for (const food of menu.foods) {
      this.dailyHistory.foods.push(this.idToFood.get(food.foodId));
    }
  }

  menuPost() {
    this.loading = true;
    this.newMenu.dailyMenuDate = this.dailyHistory.dailyMenuDate;
    for (let i = 0; i < this.selectedFoods.length; i++) {
      this.newFood.foodId = this.foodsMap.get(this.selectedFoods[i]);
      this.newFoods.push(this.newFood);
      this.newFood = {};
    }
    this.newMenu.foods = this.newFoods;
    this.newFoods = [];
    this.chefService.dailyMenusPost(this.newMenu).subscribe(response => {
      this.openSuccessSnackBar();

      this.updateHistoryFromPost(this.newMenu);
      this.dailyHistory.version = 0;
      this.dailyHistory.dailyMenuId = response.dailyMenuId;
      this.newMenu = { dailyMenuId: 0 };


      this.getSelectedFoods();
      this.emptyMenu = false;
      this.loading = false;
    }, error => {
      this.openErrorSnackBar(error.status);
      this.loading = false;
      if (error.status === 409) {
        this.emptyMenu = false;
        this.dailyHistory.dailyMenuId = JSON.parse(error._body).dailyMenuId;
        this.updateHistory(JSON.parse(error._body));
        this.getSelectedFoods();
      }else if(error.status === 500){
        this.refresh.emit();
      }
    });
  }

  menuPut() {
    this.loading = true;
    if (this.selectedFoods.length === 0) {
      this.emptyMenu = true;
    }
    for (let i = 0; i < this.selectedFoods.length; i++) {
      this.updateFood.foodId = this.foodsMap.get(this.selectedFoods[i]);
      this.updateFoods.push(this.updateFood);
      this.updateFood = {};
    }
    this.updateMenu.foods = this.updateFoods;
    this.updateMenu.version = this.dailyHistory.version;
    this.updateMenu.dailyMenuDate = this.dailyHistory.dailyMenuDate;
    this.updateFoods = [];


    this.chefService.dailyMenusIdPut(this.dailyHistory.dailyMenuId, this.updateMenu).subscribe(response => {
      this.updateHistory(response);
      this.updateMenu = {};
      this.openSuccessSnackBar();
      this.loading = false;
    }, error => {
      this.loading = false;
      this.openPutErrorSnackBar(error);
      if (error.status === 409) {
        this.updateHistory(JSON.parse(error._body));
        this.getSelectedFoods();
      } else if (error.status === 410) {
        if (JSON.parse(error._body).dailyMenuId === 0) {
          this.updateHistory(JSON.parse(error._body));
          this.getSelectedFoods();
          this.emptyMenu = true;
        } else {
          this.updateHistory(JSON.parse(error._body));
          this.getSelectedFoods();
        }
      }else if(error.status === 500){
         this.refresh.emit();
      }
    });
  }

  checkForFoodListUpdate(): boolean {
    if (this.dailyHistory.foods.length !== this.selectedFoods.length) {
      return true;
    } else {
      for (const food of this.dailyHistory.foods) {
        if (!this.selectedFoods.includes(food.foodName)) {
          return true;
        }
      }
      return false;
    }
  }

//if chef clicks cancel, everything should return to the initial state
  onCancel() {
    this.getSelectedFoods();
    this.foodsToShowInitialize();
    this.selectedValue = null;
  }

//in the past: false
  isEditable(): boolean {
    let d = new Date();
    let d2 = new Date(this.dailyHistory.dailyMenuDate);
    if (d2 > d) {
      return true;
    } else {
      return false;
    }
  }

  addToOrderFoods() {
    this.orderedFoods = [];
    for (const food of this.dailyHistory.foods) {
      if (food.isOrdered === true) {
        this.orderedFoods.push(food.foodName);
      }
    }
  }

  checkIfOrdered(food: string): boolean {
    for (const menuFood of this.orderedFoods) {
      if (menuFood === food) {
        return true;
      }
    }
    return false;
  }

  calculateFinalState() {
    const d1 = new Date();
    const d2 = new Date(this.dailyHistory.dailyMenuDate);
    const d3 = d1.getDate();
    const d4 = new Date(new Date().setDate(d2.getDate() - 1)).getDate();

    if ((d1 > d2) || ((d3 === d4) && (d1.getHours() > new Date(this.time).getHours()))) {
      return true;
    }else if((d3 === d4) && (d1.getHours() === new Date(this.time).getHours()) && (d1.getMinutes() > new Date(this.time).getMinutes())){
        return true;
    }
    return false;
  }

}
