import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import * as remote from '../../../remote';
import { MdSnackBarModule } from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { ViewContainerRef } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

import { DeleteFoodConfirmComponent } from '../delete-food-confirm/delete-food-confirm.component';
import { EditFoodConfirmComponent } from '../edit-food-confirm/edit-food-confirm.component';
import { GlobalSettingsService } from '../../../shared/globalsettings.service';

@Component({
  selector: 'app-chef-home-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss']
})
export class FoodComponent implements OnInit {

  @Input() parentFoodA;

  @Output() needRefresh: EventEmitter<boolean> = new EventEmitter<boolean>();

  public localEdit: boolean;

  public foodEdit: remote.FoodEdit = {};
  public isNotArchived = false;
  public foodOrdered = false;

  continueFoodEditing = true;
  deleteFoodConfirm = false;

  isSalad = false;
  isDrink = false;
  isMeal = false;

  constructor(
    public router: Router,
    public chefService: remote.ChefApi,
    public snackBar: MdSnackBar,
    public viewContainerRef: ViewContainerRef,
    public dialog: MdDialog,
    public globalSettingsService: GlobalSettingsService
  ) { }

  ngOnInit() {

    this.localEdit = false;

    if (this.parentFoodA.foodType === 'Salad') {
      this.isSalad = true;
      this.isDrink = false;
      this.isMeal = false;
    } else if (this.parentFoodA.foodType === 'Drink') {
      this.isSalad = false;
      this.isDrink = true;
      this.isMeal = false;
    } else {
      this.isSalad = false;
      this.isDrink = false;
      this.isMeal = true;
    }

    this.get_foods_ById(this.parentFoodA.foodId);
  }

  editLocalEdit() {
    this.chefService.foodsFoodIdGet(this.parentFoodA.foodId, true).subscribe(
      response => {
        this.continueFoodEditing = true;
      },
      error => {
        this.openErrorSnackBar(error.status);
        this.continueFoodEditing = false;
        this.needRefresh.emit(true);
      },
      () => {
        if (this.continueFoodEditing === true) {
          if (this.localEdit === true) {
            this.localEdit = false;
          } else {
            this.localEdit = true;
          }
        }
      }
    );
  }

  catchEditDone(event) {

    this.localEdit = event.editFlag;

    // If the user doesn't make changes we don't refresh the foods.
    if (event.makeChanges !== false) {
      this.needRefresh.emit(true);
    }
  }

  deleteFood() {

    this.chefService.foodsFoodIdGet(this.parentFoodA.foodId, true).subscribe(
      response => { },
      error => {
        this.openErrorSnackBar(error.status);
        this.continueFoodEditing = false;
        this.needRefresh.emit(true);
      },
      () => {
        const dialogRef = this.dialog.open(DeleteFoodConfirmComponent, { height: '150px', width: '230px', disableClose: true });
        dialogRef.afterClosed().subscribe(result => {
          if (result === 'true') {
            this.deleteFoodConfirm = true;
            this.delete_foods(0, this.parentFoodA.foodId);
          } else {
            this.deleteFoodConfirm = false;
          }
        });
      }
    );
  }

  openErrorSnackBar(code) {
    if (code === 400) {
      this.openSnackBar('Bad request. Try again',2);
    } else {
      this.openSnackBar('Request could not be completed.',2);
    }
  }

  openSuccessDeleteSnackBar() {
    this.openSnackBar('Food was deleted successfully',1);
  };

  openSnackBar(msg: string, nmb: number) {

    if (nmb === 1) {
      this.snackBar.open(msg, 'Ok', {duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb === 2) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarError', 'snackBartext'] });
    }
  }

  /*
   * Get a food by id.
   * Returns a FoodEdit object which is a { Food , boolean}.
   */
  get_foods_ById(foodId) {
    this.chefService.foodsFoodIdGet(foodId, true).subscribe(response => {
      this.foodOrdered = response.editable;
    },
      error => {
        if (error.status === 404) {
          this.isNotArchived = true;
        }
      });
  }

  /*
   * Delete a foodItem from the DB.
   */
  delete_foods(archived, foodId) {
    this.chefService.foodsFoodIdDelete(archived, foodId).subscribe(
      response => {
        this.openSuccessDeleteSnackBar();
        this.deleteFoodConfirm = false;
        this.needRefresh.emit(true);
      },
      error => {
        if (error.status === 400) {
          this.openErrorSnackBar(error.status);
          this.needRefresh.emit(true);
        } else if (error.status === 500) {
          this.openErrorSnackBar(error.status);
          this.needRefresh.emit(true);
          this.deleteFoodConfirm = true;
        }
      });
  }
}
