import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { GlobalSettingsService } from '../../../shared/globalsettings.service';

@Component({
  selector: 'app-create-food-confirm',
  templateUrl: './create-food-confirm.component.html',
  styleUrls: ['./create-food-confirm.component.scss']
})
export class CreateFoodConfirmComponent implements OnInit {

  public localFoodItem;

  constructor(
    public dialogRef: MdDialogRef<CreateFoodConfirmComponent>,
    public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() { }

  pressConfirm() {
    this.dialogRef.close('true');
  }

  pressCancel() {
    this.dialogRef.close('false');
  }
}
