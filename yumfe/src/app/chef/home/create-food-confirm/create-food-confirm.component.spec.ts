import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateFoodConfirmComponent } from './create-food-confirm.component';

describe('CreateFoodConfirmComponent', () => {
  let component: CreateFoodConfirmComponent;
  let fixture: ComponentFixture<CreateFoodConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFoodConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateFoodConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
