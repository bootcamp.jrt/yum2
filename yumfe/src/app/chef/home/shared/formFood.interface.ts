export interface FormFood {
  foodName: string;
  foodType: string;
  description: string;
  price: number;
}
