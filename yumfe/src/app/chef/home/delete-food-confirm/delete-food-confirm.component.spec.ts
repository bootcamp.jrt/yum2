import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteFoodConfirmComponent } from './delete-food-confirm.component';

describe('DeleteFoodConfirmComponent', () => {
  let component: DeleteFoodConfirmComponent;
  let fixture: ComponentFixture<DeleteFoodConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeleteFoodConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeleteFoodConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
