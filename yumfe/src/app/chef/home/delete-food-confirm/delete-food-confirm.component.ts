import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-delete-food-confirm',
  templateUrl: './delete-food-confirm.component.html',
  styleUrls: ['./delete-food-confirm.component.scss']
})
export class DeleteFoodConfirmComponent implements OnInit {

  constructor(public dialogRef: MdDialogRef<DeleteFoodConfirmComponent>) { }

  ngOnInit() { }

  pressYes() {
    this.dialogRef.close('true');
  }

  pressNo() {
    this.dialogRef.close('false');
  }
}
