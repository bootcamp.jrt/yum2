import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-edit-food-confirm',
  templateUrl: './edit-food-confirm.component.html',
  styleUrls: ['./edit-food-confirm.component.scss']
})
export class EditFoodConfirmComponent implements OnInit {

  constructor(public dialogRef: MdDialogRef<EditFoodConfirmComponent>) { }

  ngOnInit() {
  }

  pressYes() {
    this.dialogRef.close('true');
  }

  pressNo() {
    this.dialogRef.close('false');
  }
}
