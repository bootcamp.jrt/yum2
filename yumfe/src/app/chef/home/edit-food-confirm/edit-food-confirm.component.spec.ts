import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditFoodConfirmComponent } from './edit-food-confirm.component';

describe('EditFoodConfirmComponent', () => {
  let component: EditFoodConfirmComponent;
  let fixture: ComponentFixture<EditFoodConfirmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditFoodConfirmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditFoodConfirmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
