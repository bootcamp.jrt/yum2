import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import * as remote from '../../remote';
import { FoodComponent } from './food/food.component';
import { FoodEditComponent } from './food-edit/food-edit.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormFood } from '../home/shared/formFood.interface';
import { MdSnackBarModule } from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';
import { ViewContainerRef } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { CreateFoodConfirmComponent } from './create-food-confirm/create-food-confirm.component';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  private foodItem: remote.FoodItem = {}; // POST()
  foodsList; // GET()

  formFood: FormGroup;
  paginationIsReady = false;
  loading = true;
  archived: boolean;

  charsRem: number = 140;

  foodsTypeList = [
    { value: 'Main dish', viewValue: 'Main meal' },
    { value: 'Salad', viewValue: 'Salad' },
    { value: 'Drink', viewValue: 'Drink' },
    { value: 'all', viewValue: 'All' }
  ];

  itemsPerPage = [
    { value: '10', viewValue: '10 items' },
    { value: '20', viewValue: '20 items' },
    { value: '50', viewValue: '50 items' },
    { value: '100', viewValue: '100 items' }
  ];

  orderingMode = [
    { value: 'none', viewValue: 'Newest food' },
    // { value: 'mostpopular', viewValue: 'Most popular food' },
    // { value: 'leastpopular', viewValue: 'Least popular food' },
    { value: 'higherprice', viewValue: 'Higher price' },
    { value: 'lowerprice', viewValue: 'Lower price' },
  ];

  foodsTypeListA = [
    { value: 'Main dish', viewValue: 'Main meal' },
    { value: 'Salad', viewValue: 'Salad' },
    { value: 'Drink', viewValue: 'Drink' }
  ];

  currentSearchState = {
    currArchived: false,
    currPage: 0,
    currPagesize: 10,
    currType: 'all',
    currOrdering: 'none',
    currVersion: null
  };

  numOfUsers = 0;
  // paramPageNo = 0;
  // paramPageSize = 0;

  pagesize: string;
  type: string;
  ordering: string;

  createNewFood = false;

  isEmptyList = false;

  /* *********************************************************************************************************************** */
  constructor(
    public router: Router,
    public chefService: remote.ChefApi,
    public snackBar: MdSnackBar,
    public viewContainerRef: ViewContainerRef,
    public dialog: MdDialog
  ) { }

  ngOnInit() {
    this.formFood = new FormGroup({
      foodName: new FormControl('', Validators.required),
      foodType: new FormControl(''),
      description: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required)
    });

    this.pagesize = '10';
    this.type = 'all';
    this.ordering = 'none';

    // Call the default GET().
    this.get_foods();
  }


  /*
   * Posts a food item to the DB.
   */
  post_foods() {

    this.foodItem.foodName = this.formFood.value.foodName;
    this.foodItem.foodType = this.formFood.value.foodType;
    this.foodItem.description = this.formFood.value.description;
    this.foodItem.price = this.formFood.value.price;
    this.foodItem.version = 0;

    this.createNewFood = true;

    this.chefService.foodsPost(this.foodItem).subscribe(
      response => {
      this.openSuccessSnackBar();
      this.get_foods();
    },
    error => {
      this.openErrorSnackBar(error.status);
      this.createNewFood = false;
    },
    () => {
      this.paginationIsReady = false;
      this.createNewFood = false;
      this.charsRem = 140;
      this.formFood.reset();
    });
  }

  /* *********************************************************************************************************************** */

  /*
   * Get returns a list of all foods.
   */
  get_foods() {
    this.chefService.foodsGet(this.currentSearchState.currArchived, this.currentSearchState.currPage, this.currentSearchState.currPagesize,
      this.currentSearchState.currType, this.currentSearchState.currOrdering, this.currentSearchState.currVersion).subscribe(getFoods => {
        this.foodsList = getFoods.foodList;
        this.numOfUsers = getFoods.totalNumber;
        this.loading = false;
        this.checkListSize();
      }, error => this.loading = false,
      () => {
        this.paginationIsReady = true;
      });
  }

  // Handle pagination bar logic
  changePageHandler(newCurrentPage) {
    this.currentSearchState.currPage = newCurrentPage;

    // Check what is the max number of pages that you can handle with the new page size
    // Handle corner cases
    let maxPageNo = Math.floor(this.numOfUsers / this.currentSearchState.currPagesize);

    if ((maxPageNo % this.currentSearchState.currPagesize) === 0) {
      maxPageNo--;
    } else if ((maxPageNo % this.currentSearchState.currPagesize) > 0) {
      maxPageNo++;
    }

    if (maxPageNo < 0) {
      maxPageNo = 0;
    }

    // // With params overcome error message that "value has changed after was read"
    let paramPageNo = 0;
    let paramPageSize = 0;

    if (newCurrentPage > maxPageNo) {
      paramPageNo = maxPageNo;
    } else {
      paramPageNo = newCurrentPage;
    }

    // Handle very large pages
    if (this.currentSearchState.currPagesize > this.numOfUsers) {
      paramPageSize = this.numOfUsers;
    } else {
      paramPageSize = this.currentSearchState.currPagesize;
    }

    this.get_foods();
  }

  onKeyPress(event: KeyboardEvent): void {
    const patern = /Backspace/;
  if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 65 && event.keyCode <= 90) || event.key === 'Backspace' || event.keyCode === 0 || event.keyCode === 32){
    if (this.charsRem < 140 && event.key === 'Backspace') {
      this.charsRem++;
    } else if (this.charsRem === 140 && event.key === 'Backspace') {
      this.charsRem = 140;
    } else if (this.charsRem === 0) {
      if (!patern.test(event.key)) {
        event.preventDefault();
      } else {
        this.charsRem++;
      }
    } else {
      this.charsRem--;
    }
  }else{
    event.preventDefault();
  }
  }

/* *********************************************************************************************************************** */

  onBlurFoodName() {
    if (this.formFood.value.foodName === null || this.formFood.value.foodName === '') { } else {
      this.chefService.foodsFindByNameNameGet(this.formFood.value.foodName).subscribe(response => {
        this.snackBar.open('Food already exists! Please insert another name.', 'OK', {extraClasses: ['snackBarError', 'snackBartext'] });
        this.formFood.reset();
      }, error => {});
    }
  }

  onBlurPrice() {
    if (this.formFood.value.price !== undefined) {
      const pattern = new RegExp('^[+]?([0-9]+(?:[\.|\,][0-9]*)?|\.[0-9]+)$');
      const result = pattern.test(this.formFood.value.price);

      if (result === false) {
        this.snackBar.open('Invalid number! Please try again.', 'OK', {extraClasses: ['snackBarError', 'snackBartext'] });
        this.formFood.reset();
      }
    }
  }

  onChangeSelectPageSize(event) {
    this.currentSearchState.currPagesize = Number(event);
    this.paginationIsReady = false;
    this.get_foods();
  }

  onChangeSelectType(event) {
    this.currentSearchState.currType = event;
    this.paginationIsReady = false;
    this.get_foods();
  }

  onChangeSelectOrdering(event) {
    this.currentSearchState.currOrdering = event;
    this.get_foods();
  }

  onChangeSelectArchived(event) {
    this.currentSearchState.currArchived = event;
    this.get_foods();
  }

  catchNeedRefresh(event) {
    this.paginationIsReady = false;
    this.get_foods();
  }

/* *********************************************************************************************************************** */

  openSuccessSnackBar() {
    this.openSnackBar('Food was created successfully', 1);
  };

  openErrorSnackBar(code) {
    if (code === 400) {
      this.openSnackBar('Bad request. Try again', 2);
    }
  }

  openSnackBar(msg: string, nmb: number) {
    if (nmb === 1) {
      this.snackBar.open(msg, 'Ok', {duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb === 2) {
      this.snackBar.open(msg, 'Ok', {extraClasses: ['snackBarError', 'snackBartext'] });
    }
  }

  checkListSize() {
    if (this.numOfUsers === 0) {
      this.isEmptyList = true;
    } else {
      this.isEmptyList = false;
    }
  }
}

