import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import * as remote from '../../../remote';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormFood } from '../../home/shared/formFood.interface';
import { ViewContainerRef } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { EditFoodConfirmComponent } from '../edit-food-confirm/edit-food-confirm.component';
import { MdSnackBarModule } from '@angular/material';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-chef-home-food-edit',
  templateUrl: './food-edit.component.html',
  styleUrls: ['./food-edit.component.scss']
})
export class FoodEditComponent implements OnInit {

  private foodItem: remote.FoodItem = {}; // POST()
  private foodEdit: remote.FoodEdit = {}; // GET()/{id}
  private orderedFood;

  formFood: FormGroup;

  @Input()
  parentFoodB;

  @Output()
  editDone: EventEmitter<ComplexEdit> = new EventEmitter<ComplexEdit>();

  foodsTypeListA = [
    { value: 'Main dish', viewValue: 'Main meal' },
    { value: 'Salad', viewValue: 'Salad' },
    { value: 'Drink', viewValue: 'Drink' }
  ];

  constructor(
    public chefService: remote.ChefApi,
    public viewContainerRef: ViewContainerRef,
    public dialog: MdDialog,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {
    this.formFood = new FormGroup({
      foodName: new FormControl(''),
      foodType: new FormControl(''),
      description: new FormControl(''),
      price: new FormControl('')
    });

    this.formFood.patchValue({
      foodName: this.parentFoodB.foodName.trim(),
      foodType: this.parentFoodB.foodType,
      description: this.parentFoodB.description.trim(),
      price: this.parentFoodB.price
    });

    this.chefService.foodsFoodIdGet(this.parentFoodB.foodId, true).subscribe(
      getFood => {
        this.foodEdit = getFood;
        this.checkOrderedFoods();
      },
      error => {

      });
  }

  // ---------------------------------------------------------------------

  onBlurFoodName() {
    if (this.formFood.value.foodName.trim() === null || this.formFood.value.foodName.trim() === '') { } else {
      this.chefService.foodsFindByNameNameGet(this.formFood.value.foodName.trim()).subscribe(response => {
        this.snackBar.open('Food already exists! Please insert another name.', 'OK', { extraClasses: ['snackBarError', 'snackBartext'] });

        this.formFood.patchValue({
          foodName: this.parentFoodB.foodName.trim(),
          foodType: this.parentFoodB.foodType,
          description: this.parentFoodB.description.trim(),
          price: this.parentFoodB.price
        });
      }, error => { });
    }
  }

  onBlurPrice() {
    if (this.formFood.value.price !== undefined) {
      const pattern = new RegExp('^[+]?([0-9]+(?:[\.|\,][0-9]*)?|\.[0-9]+)$');
      const result = pattern.test(this.formFood.value.price);

      if (result === false) {
        this.snackBar.open('Invalid number! Please try again.', 'OK', { extraClasses: ['snackBarError', 'snackBartext'] });

        this.formFood.patchValue({
          foodName: this.parentFoodB.foodName.trim(),
          foodType: this.parentFoodB.foodType,
          description: this.parentFoodB.description.trim(),
          price: this.parentFoodB.price
        });
      }
    }
  }

  checkOrderedFoods() {

    // After the call of the GET /foods/{foodId} with params {foodId} + true.
    // The response body returns a FoodItem and a boolean variable with name 'editable',
    // which indicates if the food has ordered or not.
    if (this.foodEdit.editable === false) {

      this.orderedFood = true;

      const dialogRef = this.dialog.open(EditFoodConfirmComponent, { height: '300px', width: '400px', disableClose: true });

      dialogRef.afterClosed().subscribe(result => {

        if (result === 'true') {
          this.foodItem.foodName = this.formFood.value.foodName.trim();
          this.foodItem.foodType = this.formFood.value.foodType;
          this.foodItem.price = this.formFood.value.price;

          this.formFood.controls['foodName'].disable();
          this.formFood.controls['foodType'].disable();
          this.formFood.controls['price'].disable();
        } else {
          const passAnswer: ComplexEdit = { editFlag: false, makeChanges: false };
          this.editDone.emit(passAnswer);
        }
      });

    } else {
      this.orderedFood = false;
    }
  }

  pressCancel() {
    const passAnswer: ComplexEdit = { editFlag: false, makeChanges: false };
    this.editDone.emit(passAnswer);
  }


  completeEdit() {
    if (this.orderedFood === false) {

      // Step-1: First we check if the user has modify the current value of any field.
      if (this.parentFoodB.foodName.trim() === this.formFood.value.foodName.trim() && this.parentFoodB.foodType === this.formFood.value.foodType &&
        this.parentFoodB.description.trim() === this.formFood.value.description.trim() && this.parentFoodB.price === this.formFood.value.price) {

        const passAnswer: ComplexEdit = { editFlag: false, makeChanges: false };
        this.editDone.emit(passAnswer);

      } else {

        this.foodItem.foodName = this.formFood.value.foodName.trim();
        this.foodItem.foodType = this.formFood.value.foodType;
        this.foodItem.description = this.formFood.value.description.trim();
        this.foodItem.price = this.formFood.value.price;
        this.foodItem.version = this.foodEdit.food.version;
        this.putFoods();

      }

    } else {
      this.foodItem.description = this.formFood.value.description.trim();
      this.foodItem.version = this.foodEdit.food.version;
      this.putCloneFood();
    }

  }

  /*
   * Update a food using the id. The value false indicates tha the food isn't archived.
   */
  putFoods() {
    this.chefService.foodsFoodIdPut(this.parentFoodB.foodId, false, this.foodItem).subscribe(
      response => {
        const passAnswer: ComplexEdit = { editFlag: false, makeChanges: true };
        this.editDone.emit(passAnswer);
      },
      error => {
      }
    );
  }

  /*
   * Update a food using the id. The value true indicates tha the food is archived.
   * If the food is archived the only field that you can update is the description field.
   */
  putCloneFood() {
    this.chefService.foodsFoodIdPut(this.parentFoodB.foodId, true, this.foodItem).subscribe(
      response => {
        const passAnswer: ComplexEdit = { editFlag: false, makeChanges: true };
        this.editDone.emit(passAnswer);
      },
      error => {
      }
    );
  }

}

interface ComplexEdit {
  editFlag: boolean;
  makeChanges: boolean;
}
