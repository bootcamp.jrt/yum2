import { Component, OnInit, Input } from '@angular/core';
import * as remote from '../../../remote';
import { Router } from '@angular/router';
import { GlobalSettingsService } from '../../../shared/globalsettings.service';

@Component({
  selector: 'app-userorder',
  templateUrl: './userorder.component.html',
  styleUrls: ['./userorder.component.scss']
})
export class UserorderComponent implements OnInit {

  @Input() dailyUserSummary: remote.DailySummaryUserList;
  @Input() totalPr: number;

  constructor(
    public chefService: remote.ChefApi,
    public route: Router,
    public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() {
  }

}
