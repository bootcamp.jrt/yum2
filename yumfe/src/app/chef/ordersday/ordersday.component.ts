import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as remote from '../../remote';
import { GlobalSettingsService } from '../../shared/globalsettings.service';


@Component({
  selector: 'app-ordersday',
  templateUrl: './ordersday.component.html',
  styleUrls: ['./ordersday.component.scss']
})
export class OrdersdayComponent implements OnInit {


  public dailySummary: remote.DailySummary;

  totalPrice: number;
  totalPriceUser: Array<number> = [];

  // it is actually an id and not a date
  id: number;

  public sub: any;
  loading = true;

  regularDistribution = 100 / 8;
  columns = ['Name', 'Meal', 'Qty', 'Salad', 'Qty', 'Drink', 'Qty', 'Total'];

  constructor(
    public chefService: remote.ChefApi,
    public router: ActivatedRoute,
    public route: Router,
    public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() {
    this.sub = this.router.params.subscribe(params => {
      if (params.day) {
        this.id = parseInt(params.day, 10); // (+) converts string 'id' to a number
        this.chefService.ordersDailyDayGet(this.id).subscribe(sumOfDay => {
          this.loading = false;
          this.dailySummary = sumOfDay;
          this.calculateTotalPrice();
          this.calculatePrice();
        }, error => {}); this.loading = false;
      } else {
        this.loading = false;
      }
    });


  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  calculateTotalPrice() {
    this.totalPrice = 0;
    let total: number = 0;
    for (let i = 0; i < this.dailySummary.orderedFoodList.length; i++) {
      total = total + (this.dailySummary.orderedFoodList[i].quantity) * (this.dailySummary.orderedFoodList[i].foods.price);
    } this.totalPrice = total;
  }

  calculatePrice() {
    let total: number;
    for (let i = 0; i < this.dailySummary.userList.length; i++) {
      total = 0;
      for (let j = 0; j < this.dailySummary.userList[i].foods.length; j++) {
        total = total + (this.dailySummary.userList[i].foods[j].quantity) * (this.dailySummary.userList[i].foods[j].food.price);
      }
      this.totalPriceUser[i] = total;
    }
  }

  public print = (): void => {
    let printContents, popupWin;
    printContents = document.getElementById('container-to-print').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=1260px');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Daily Order Summary</title>
          <style>

          .no-print {
            display: none;
          }
          app-user-order {
            display: block;
          }
          </style>
        </head>
        <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

}


