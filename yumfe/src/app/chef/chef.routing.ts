import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ChefApi } from '../remote';
import { LoggedComponent } from '../shared/logged/logged.component';
import { OrdersComponent } from 'app/chef/orders/orders.component';
import { MenusComponent } from 'app/chef/menus/menus.component';
import { OrdersdayComponent } from 'app/chef/ordersday/ordersday.component';
import { CanActivateChefGuard } from "app/app.guard";


const chefRoutes: Routes = [
  { path: 'chef',
    component: LoggedComponent,
    canActivateChild: [CanActivateChefGuard],
    children: [
      { path: '', component: HomeComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'orders/daily/:day', component: OrdersdayComponent},// the order matters because otherwise it will not work
      { path: 'orders/:year/:month', component: OrdersComponent },
      { path: 'menus', component: MenusComponent },
      { path: 'menus/:year/:month', component: MenusComponent },
    ]
  }
];

export const ChefRouting = RouterModule.forChild(chefRoutes);
