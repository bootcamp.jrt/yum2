import { Component, OnInit, Input } from '@angular/core';
import * as remote from '../../../remote';
import { Router } from '@angular/router';
import { GlobalSettingsService } from '../../../shared/globalsettings.service';

@Component({
  selector: 'app-ordertotal',
  templateUrl: './ordertotal.component.html',
  styleUrls: ['./ordertotal.component.scss']
})
export class OrdertotalComponent implements OnInit {

  @Input() dailyOrderMonthSummary: remote.DailyOrderMonthSummary;
  @Input() totalPr: number;

  constructor(
    public chefService: remote.ChefApi,
    public route: Router,
    public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() {
  }



}
