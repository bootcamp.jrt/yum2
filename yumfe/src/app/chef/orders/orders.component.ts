import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import * as remote from '../../remote';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  public dailyOrderMonthSummarys: Array<remote.DailyOrderMonthSummary> = [];

  dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
  daysAfterMonday: Array<number> = [];

  totalPrice: Array<number> = [];
  orderAvailability: Array<boolean> = [];

  foodMap: Map<string, number>;
  monthDays: number;

  regularDistribution = 100 / 5;

  currMonthNumber: number;
  currYear: number;
  private sub: any;
  loading = true;


  constructor(
    public chefService: remote.ChefApi,
    public router: ActivatedRoute,
    public route: Router) { }

  ngOnInit() {

    this.sub = this.router.params.subscribe(params => {
      if (params.year && params.month) {

        this.currYear = parseInt(params.year);
        this.currMonthNumber = parseInt(params.month); // (+) converts string 'id' to a number

        if (this.currMonthNumber === this.getMonthNumber() && this.currYear === this.getYearNumber()) {
          this.route.navigate(['/chef/orders']);
        } else {
          this.chefService.ordersMonthlyMonthGet(this.currMonthNumber + "-" + this.currYear).subscribe(dailyOrderMonthly => {
            this.dailyOrderMonthSummarys = dailyOrderMonthly;
            this.monthDays = this.daysInMonth(this.currMonthNumber, this.currYear);
            this.monthStartCalculate(this.currYear, this.currMonthNumber);
            this.dailyOrderMonthSummarys = this.removeWeekendsFromHistory();
            this.calculateTotalPrice();
            this.calculateOrderAvaliability();
            this.loading = false;
          }, error => {
            this.loading = false;
            if (error.status === 409) {
              this.currYear = this.currYear + 1;
              this.currMonthNumber = 1;
              this.route.navigate(['/chef/orders', this.currYear, this.currMonthNumber]);
            }
          });
        }

      } else {

        this.currMonthNumber = this.getMonthNumber();
        this.currYear = this.getYearNumber();

        this.chefService.ordersMonthlyGet().subscribe(dailyOrderMonthly => {
          this.dailyOrderMonthSummarys = dailyOrderMonthly;
          this.monthDays = this.daysInMonth(this.currMonthNumber, this.currYear);
          this.monthStartCalculate(this.currYear, this.currMonthNumber);
          this.dailyOrderMonthSummarys = this.removeWeekendsFromHistory();
          this.calculateTotalPrice();
          this.calculateOrderAvaliability();
          this.loading = false;
        }, error => this.loading = false);

      }

    });
  }

  monthStartCalculate(year: number, month: number) {
    this.daysAfterMonday = [];
    let d = new Date(year, month - 1, 1);
    let size = 0;
    if (d.getDay() === 2) {
      size = 1;
    } else if (d.getDay() === 3) {
      size = 2;
    } else if (d.getDay() === 4) {
      size = 3;
    } else if (d.getDay() === 5) {
      size = 4;
    } else {
      size = 0;
    }
    for (let i = 0; i < size; i++) {
      this.daysAfterMonday.push(1);
    }
  }

  calculateTotalPrice() {
    this.totalPrice = [];
    let found: boolean = false;
    for (let i = 0; i < this.dailyOrderMonthSummarys.length; i++) {
      let total: number = 0;
      for (let j = 0; j < this.dailyOrderMonthSummarys[i].foods.length; j++) {
        if (this.dailyOrderMonthSummarys[i].foods[j].quantity > 0) {
          total = total + (this.dailyOrderMonthSummarys[i].foods[j].quantity * this.dailyOrderMonthSummarys[i].foods[j].food.price);
        }
      }
      if (total === 0) {
        this.totalPrice.push(0);
      } else {
        this.totalPrice.push(total);
      }
    }
  }

  calculateOrderAvaliability() {
    let d = new Date();
    this.orderAvailability = [];
    for (let i = 0; i < this.dailyOrderMonthSummarys.length; i++) {
      let d2 = new Date(this.dailyOrderMonthSummarys[i].day);
      if (d > d2) {
        this.orderAvailability.push(false);
      } else {
        this.orderAvailability.push(true);
      }
    }
  }




  removeWeekendsFromHistory(): Array<remote.DailyOrderMonthSummary> {
    let menu = this.dailyOrderMonthSummarys;
    let index = 0;
    for (let i = this.dailyOrderMonthSummarys.length - 1; i >= 0; i--) {
      let d = new Date(this.currYear, this.currMonthNumber - 1, i + 1);
      if (d.getDay() === 6 || d.getDay() === 0) {
        menu.splice(i, 1);
      }
    }
    return menu;
  }

  daysInMonth(month: number, year: number): number {
    return new Date(year, month, 0).getDate();
  }

  getYearNumber(): number {
    let date = new Date();
    return date.getFullYear();
  }

  getPreviousYear(): number {
    if (this.currMonthNumber === 1) {
      return this.currYear - 1;
    } else {
      return this.currYear;
    }
  }

  getNextYear(): number {
    if (this.currMonthNumber === 12) {
      return this.currYear + 1;
    } else {
      return this.currYear;
    }
  }

  getMonthNumber(): number {
    let date = new Date();
    return date.getMonth() + 1;
  }

  getNextMonth(): number {
    if (this.currMonthNumber === 12) {
      return 1;
    }
    return this.currMonthNumber + 1;
  }

  getPreviousMonth(): number {
    if (this.currMonthNumber === 1) {
      return 12;
    }
    return this.currMonthNumber - 1;
  }


}
