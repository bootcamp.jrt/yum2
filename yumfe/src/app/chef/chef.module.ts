import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { ChefRouting } from './chef.routing';
import { ChefApi } from '../remote';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { FoodEditComponent} from './home/food-edit/food-edit.component';
import { FoodComponent } from './home/food/food.component';
import { OrdersComponent } from './orders/orders.component';
import {MdGridListModule} from '@angular/material';
import { MenusComponent } from './menus/menus.component';
import { DailyMenuComponentComponent } from './menus/daily-menu-component/daily-menu-component.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CreateFoodConfirmComponent } from './home/create-food-confirm/create-food-confirm.component';
import { DeleteFoodConfirmComponent } from './home/delete-food-confirm/delete-food-confirm.component';
import { EditFoodConfirmComponent } from './home/edit-food-confirm/edit-food-confirm.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { OrdertotalComponent } from './orders/ordertotal/ordertotal.component';
import { OrdersdayComponent } from './ordersday/ordersday.component';
import { UserorderComponent } from './ordersday/userorder/userorder.component';
import { ChefNavBarComponent } from './chef-nav-bar/chef-nav-bar.component';

@NgModule({
  imports: [
    CommonModule,
    ChefRouting,
    SharedModule,
    FormsModule,
    MdGridListModule,
    MaterialModule,
    FlexLayoutModule,
    ReactiveFormsModule
  ],
  declarations: [
    HomeComponent,
    FoodEditComponent,
    FoodComponent,
    OrdersComponent,
    MenusComponent,
    DailyMenuComponentComponent,
    OrdertotalComponent,
    OrdersdayComponent,
    UserorderComponent,
    CreateFoodConfirmComponent,
    DeleteFoodConfirmComponent,
    EditFoodConfirmComponent,
    ChefNavBarComponent
  ],
  entryComponents: [
    CreateFoodConfirmComponent,
    DeleteFoodConfirmComponent,
    EditFoodConfirmComponent
  ],
  providers: [ChefApi]
})
export class ChefModule { }
