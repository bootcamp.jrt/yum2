import { Component, OnInit, Input } from '@angular/core';
import 'rxjs/add/operator/switchMap';
import { ActivatedRoute, Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import { AuthenticationService } from '../../shared/authentication.service';
import * as remote from '../../remote';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})

export class UsersComponent implements OnInit {

  // The user's that we have opened for editing id
  id;
  // User type DTOs that tranfer use data between components
  user: remote.UserGetId = {};
  userUpdate: remote.UserIdPutByAdmin = { user: {} };
  userApprove: remote.UserExtended = { user: {} };

  // Variables that synchronize the viewing of various components
  loading = true;
  disableSubmit = true;
  showSmallSpinner = false;

  constructor(
    public router: ActivatedRoute,
    public adminApiService: remote.AdminApi,
    public authApiService: remote.AuthApi,
    public snackBar: MdSnackBar,
    public routerRedirect: Router) { }

  // Get the user id parameter from the router and fill the two DTOs
  getUserToUpdate() {
    this.router.params.subscribe(params => {

      this.adminApiService.usersIdGet(+params['id']).subscribe(user => {
        this.user = user;
        this.loading = false;
        this.id = params.id;
        this.fillUserUpdate(); // Populate form with user data
        // Assign the value to the approve component
        this.userApprove.id = params.id;
        this.userApprove.user.approved = this.user.approved;
      });
    });
  }

  // Populate the user to be updated object (Output DTO) with the existing data (Input DTO)
  fillUserUpdate() {
    this.userUpdate = { user: {} };
    this.userUpdate.version = this.user.version;
    this.userUpdate.user.version = this.user.version;
    this.userUpdate.user.firstName = this.user.firstName;
    this.userUpdate.user.lastName = this.user.lastName;
    this.userUpdate.user.email = this.user.email;
    this.userUpdate.user.role = this.user.role;
    this.userUpdate.user.hasPicture = this.user.hasPicture;
    this.userUpdate.approved = this.user.approved;
  }

  ngOnInit() {
    this.getUserToUpdate();
  }

  // Click the submit button to update the user
  onSubmit() {
    this.disableSubmit = true;
    this.showSmallSpinner = true;
    this.adminApiService.usersIdPut(this.id, this.userUpdate)
      .subscribe(response => this.updateUserHandle(), error => this.onSubmitErrorHandling(error));
  }

  // Actions to execute after the successful update of the user
  updateUserHandle() {
    this.disableSubmit = false;
    this.showSmallSpinner = false;
    this.openSnackBar('putok');
    // After a succesful update, return to the Home page
    this.routerRedirect.navigate(['/']);
  }

  // Handle errors when updating a user
  onSubmitErrorHandling = function (error) {
    this.openSnackBar(error);
    this.disableSubmit = false;
    this.showSmallSpinner = false;
    // Handle concurrent modification case
    if (error.status === 409) {
      var latestUserDTO = JSON.parse(error._body);
      this.userUpdate.version = latestUserDTO.version;
      this.userUpdate.version = latestUserDTO.user.version;
      this.userUpdate.user.version = latestUserDTO.user.version;
      this.userUpdate.user.firstName = latestUserDTO.user.firstName;
      this.userUpdate.user.lastName = latestUserDTO.user.lastName;
      this.userUpdate.user.email = latestUserDTO.user.email;
      this.userUpdate.user.role = latestUserDTO.user.role;
      this.userUpdate.approved = latestUserDTO.user.approved;
      // Notify the child component that the form needs to be re-set
      //this.formChanged = true;
    }
  }

  // Emit-handler that keeps the submit disabled while the form is not valid
  handleformState(state) {
    if (state === false) {
      this.disableSubmit = true;
    }
  }

  // Emit handler that waits for the form data to be transmited
  // When the form is valid, it activates the submit button
  handleUpdateData() {
    this.disableSubmit = false;
  }

  // The body of the email we can send the user for password reset
  private body: remote.Body = {};

  // Initialize the process of password reset for a user
  emailResetPwd() {
    this.body.email = this.userUpdate.user.email;
    this.authApiService.authForgotpwdPost(this.body).subscribe(response => {
      this.openSnackBar('emailok');
    }, error => this.openSnackBar(error));
  }

  // Emit-handler that reload the user data when the picture is changed
  handlePicChanged() {
    // Get the latest version of the user
    this.getUserToUpdate();
  }

  openSnackBar(msgType: any) {
    // Success snackbar
    if (msgType === 'putok') {
      this.snackBar.open('User was updated successfully', 'Ok', { duration: 2000, extraClasses: ['snackBarSuccess'] });
    }
    if (msgType === 'emailok') {
      this.snackBar.open('E-mail was sent successfully', 'Ok', { duration: 2000, extraClasses: ['snackBarSuccess'] });
    }
    // Error snackbars
    else {
      var error = JSON.parse(msgType._body);
      if (msgType.status === 400 || msgType.status === 404) {
        this.snackBar.open(error.message, 'Ok', { extraClasses: ['snackBarError'] });
      }
      // The concurrent modification returns a DTO instead of a message
      else if (msgType.status === 409) {
        this.snackBar.open('User already updated. The latest version is returned', 'Ok', { extraClasses: ['snackBarError'] });
      }
      // Catch all internal server errors
      else if (msgType.status === 500) {
        this.snackBar.open('Something went wrong internally', 'Ok', { extraClasses: ['snackBarError'] });
      }
      // Catch the really obscure errors
      else {
        this.snackBar.open('Unknown error', 'Ok', { extraClasses: ['snackBarError'] });
      }
    }
  }

}
