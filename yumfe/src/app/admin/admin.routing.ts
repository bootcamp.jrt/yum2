import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';
import { LoggedComponent } from '../shared/logged/logged.component';
import { CanActivateAdminGuard } from "app/app.guard";

const adminRoutes: Routes = [
  {
    path: 'admin',
    component: LoggedComponent,
    canActivateChild: [CanActivateAdminGuard],
    children: [
      { path: '', component: HomeComponent },
      { path: 'settings', component: SettingsComponent },
      { path: 'users/:id', component: UsersComponent }
    ]
  }
];

export const AdminRouting = RouterModule.forChild(adminRoutes);
