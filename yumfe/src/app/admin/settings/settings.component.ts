import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

import * as remote from '../../remote';
import { GlobalSettingsService } from '../../shared/globalsettings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  // formBuilder: any;

  currencyList = [
    "&euro;",
    "&dollar;",
    "&pound;",
    "&yen;",
    "&#8381;",
    "&#8377;",
    "&#20803;"
  ];


  loading = true;
  loadOnUpdate = false;


  currency = this.currencyList[0];
  gsForm: FormGroup;
  protected txt = '';

  protected gs: remote.GlobalSettings;

  constructor(public adminService: remote.AdminApi,
    public hungryService: remote.HungryApi,
    public snackBar: MdSnackBar,
    public route: Router,
    public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() {

    this.gsForm = new FormGroup({
      timeOfLastOrder: new FormControl(new Date()),
      currency: new FormControl(this.currency),
      notes: new FormControl(''),
      termsOfService: new FormControl(''),
      privacyPolicy: new FormControl(''),
    });

    this.hungryService.globalsettingsGet().subscribe(globalsettings => {
      this.gs = globalsettings;
      this.loading = false;
    }, error => this.loading = false,
      () => {
        this.gsForm.patchValue({ timeOfLastOrder: this.gs.timeOfLastOrder });
        this.gsForm.patchValue({ currency: this.gs.currency });
        this.gsForm.patchValue({ notes: this.gs.notes });
        this.gsForm.patchValue({ termsOfService: this.gs.termsOfService });
        this.gsForm.patchValue({ privacyPolicy: this.gs.privacyPolicy });
      });

  }

  saveChanges({ value, valid }: { value: remote.GlobalSettings, valid: boolean }) {
    this.loadOnUpdate = true;
    value.version = this.gs.version;
    value.versionOfFoodList = this.gs.versionOfFoodList;

    this.adminService.globalsettingsPut(value).subscribe(data => {
      this.openSnackBar('Global settings updated succesfully', 1);
      this.loadOnUpdate = false;
      this.route.navigate(['/admin']);
    },
      error => {
        this.openErrorSnackBar(error.status);
        this.hungryService.globalsettingsGet().subscribe(globalsettings => {
          this.gs = globalsettings;
          this.loadOnUpdate = false;
        }, error => this.loadOnUpdate = false,
          () => {
            this.gsForm.patchValue({ timeOfLastOrder: this.gs.timeOfLastOrder });
            this.gsForm.patchValue({ currency: this.gs.currency });
            this.gsForm.patchValue({ notes: this.gs.notes });
            this.gsForm.patchValue({ termsOfService: this.gs.termsOfService });
            this.gsForm.patchValue({ privacyPolicy: this.gs.privacyPolicy });
          });
      }
    );
  }

  openSnackBar(msg: string, nmb: number) {

    if (nmb === 1) {
      this.snackBar.open(msg, 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    } else if (nmb === 2) {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarConcurrency', 'snackBartext'] });
    } else {
      this.snackBar.open(msg, 'Ok', { extraClasses: ['snackBarError', 'snackBartext'] });
    }
  }

  openSuccessSnackBar() {
    this.snackBar.open('Global settings were updated successfully', undefined, { duration: 5000 });
  };

  openErrorSnackBar(code) {
    if (code === 400) {
      this.openSnackBar('Bad request. Try again', 3);
    } else if (code === 409) {
      this.openSnackBar('Cocurrent modification error. Please try again.', 2);
    }
  }

}
