import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home/home.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';
import { AdminRouting } from './admin.routing';
import { SharedModule } from '../shared/shared.module';
import { UserComponent } from './home/user/user.component';
import { AdminNavBarComponent } from './admin-nav-bar/admin-nav-bar.component';
import { SimpletinyComponent } from './settings/simpletiny/simpletiny.component';
import { TinymcedirectiveDirective } from './settings/tinymcedirective.directive';

import {
  DialogOrdersPastComponent,
  DialogCantDeleteComponent,
  DialogOrdersFutureComponent,
  DialogOrdersPastDisappComponent
} from './home/user/user.component';

@NgModule({
  imports: [
    CommonModule,
    AdminRouting,
    SharedModule,
  ],
  declarations: [
    HomeComponent,
    SettingsComponent,
    UsersComponent,
    UserComponent,
    DialogOrdersPastComponent,
    DialogCantDeleteComponent,
    DialogOrdersFutureComponent,
    DialogOrdersPastDisappComponent,
    AdminNavBarComponent,
    SimpletinyComponent,
    TinymcedirectiveDirective
  ],
  entryComponents: [
    DialogOrdersPastComponent,
    DialogCantDeleteComponent,
    DialogOrdersFutureComponent,
    DialogOrdersPastDisappComponent,
  ]
})
export class AdminModule { }
