import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialogRef, MdDialog } from '@angular/material';
import { MdSnackBar } from '@angular/material';

import * as remote from '../../../remote';
import { AuthenticationService } from '../../../shared/authentication.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})

export class UserComponent implements OnInit {


  @Input() inputUser: remote.UserExtended;
  @Output() userDeleted = new EventEmitter();

  dialogRef: MdDialogRef<any>;
  // Variable to know the id of the current user
  // Will be used to not allow the current user to disapprove or delete himself
  yourId;


  constructor(
    public router: Router,
    public adminApiService: remote.AdminApi,
    public dialog: MdDialog,
    public snackBar: MdSnackBar,
    public authenticationService: AuthenticationService) { }

  ngOnInit() {
    const currentUser = this.authenticationService.getLoggedInUser();
    this.yourId = currentUser.userId;
  }

  // Call DELETE api and emit an event when a user is deleted so that the parent component can update the list.
  onDelete(force: boolean) {
    this.adminApiService.usersIdDelete(this.inputUser.id, force)
      .subscribe(response => {
        this.userDeleted.emit();
        this.snackBar.open('User deletion successful', 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
      }, error => this.onDeleteHandleError(error.status));
  }

  onDeleteHandleError(code) {
    if (code === 405) {
      if (this.inputUser.user.approved) {
        this.open(DialogOrdersPastComponent);
      }
      else {
        this.open(DialogOrdersPastDisappComponent);
      }
    }
    else if (code === 406) {
      this.open(DialogOrdersFutureComponent);
    }
    else if (code === 400) {
      this.open(DialogCantDeleteComponent);
    }
  }

  // Method to open dialogs
  open(thatDialog) {
    this.dialogRef = this.dialog.open(thatDialog);

    // Execute actions from dialog button
    this.dialogRef.afterClosed().subscribe(result => {
      if (result === 'delete') {
        this.onDelete(true);
      }
    });
  }

  // Navigates to the page of the details of the user
  goToUser(user: remote.UserExtended): void {
    const link = ['/admin/users', user.id];
    this.router.navigate(link);
  };

} // End of class


// Dialog Components

@Component({
  template: `
  <p>User has made orders in the past and cannot be deleted.</p>
  <button type="button"class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" (click)="dialogRef.close('disapprove')">Disappove him instead</button>
  <button type="button"class="mdl-button mdl-js-button mdl-button--raised mdl-button--accent" (click)="dialogRef.close('cancel')">Cancel</button>`
})
export class DialogOrdersPastComponent {
  constructor(public dialogRef: MdDialogRef<any>) { }
}

@Component({
  template: `
  <p>User has placed orders in the future.
  <p>If you want to delete him,</p>
  <p>the pending orders will be deleted.</p>
  <button type="button" md-raised-button color="accent" (click)="dialogRef.close('delete')">Delete</button>
  <button type="button" md-raised-button color="primary" (click)="dialogRef.close('cancel')">Cancel</button>`
})
export class DialogOrdersFutureComponent {
  constructor(public dialogRef: MdDialogRef<any>) { }
}

@Component({
  template: `
  <p>User cannot be deleted.</p>
  <button type="button" md-raised-button color="accent" (click)="dialogRef.close()">Ok</button>`
})
export class DialogCantDeleteComponent {
  constructor(public dialogRef: MdDialogRef<any>) { }
}

@Component({
  template: `
  <p>User has placed orders in the past.</p>
  <p>You cannot delete him.</p>
  <p>He is already disapproved.</p>
  <button type="button" md-raised-button color="accent" (click)="dialogRef.close()">OK</button>`
})
export class DialogOrdersPastDisappComponent {
  constructor(public dialogRef: MdDialogRef<any>) { }
}
