import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../shared/authentication.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MdDialogRef, MdDialog } from '@angular/material';
import { MdSnackBar } from '@angular/material';

import { UserComponent } from './user/user.component';
import * as remote from '../../remote';
import { TosService } from '../../shared/tos.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements OnInit {

  myId;
  myToken;

  // Variables that get the input or output of API calls
  userToCreate: remote.UserPut = {};

  // The DTO with all the page users and the total number of users
  allUsers: remote.AllUsers = {};
  // Reactive form object that will become the post DTO
  userGroup: FormGroup;

  // Variables that synchronize the viewing of various components
  usersLoaded = false;
  orderingTypeDisable = false;
  createUserDisable = false;
  orderingWheel = false;

  // Paging and Sorting variables
  selectedShow = 10;
  selectedSort = 'date';
  orderType = 'asc';
  currentPage = 0;
  numOfUsers = 0;

  // Select values population
  shows = [
    { value: 10, viewValue: 10 },
    { value: 20, viewValue: 20 },
    { value: 50, viewValue: 50 },
    { value: 100, viewValue: 100 }
  ];

  sorts = [
    { value: 'email', viewValue: 'Username' },
    { value: 'role', viewValue: 'Role' },
    { value: 'date', viewValue: 'Registration Date' },
    { value: 'approved', viewValue: 'Approval Status' }
  ];

  constructor(
    public authenticationService: AuthenticationService,
    public router: Router,
    public adminApiService: remote.AdminApi,
    public fb: FormBuilder,
    public snackBar: MdSnackBar,
    public tos: TosService,
    public authService: AuthenticationService) { }


  // The basic methos that retrieves all the users from the DB
  // It can take a message as an argument for the cases we must show one to the user
  getUsers(messageType: string): void {
    this.adminApiService.usersGet(this.selectedSort, this.currentPage, this.selectedShow, this.orderType)
      .subscribe(response => {
        this.allUsers = response;
        this.numOfUsers = this.allUsers.totalNumber;
        this.usersLoaded = true;
        this.orderingTypeDisable = false;
        if (messageType !== undefined) {
          this.openSnackBar(messageType);
        }
      },
      error => {
        this.orderingTypeDisable = false;
      }
      );
  }

  // Handle pagination bar logic
  changePageHandler(event) {
    this.currentPage = event;
    // Check what is the max number of pages that you can handle with the new page size
    // Handle corner cases
    let maxPageNo = Math.floor(this.numOfUsers / this.selectedShow);
    if ((maxPageNo % this.selectedShow) === 0) {
      maxPageNo--;
    }
    else if ((maxPageNo % this.selectedShow) > 0) {
      maxPageNo++;
    }
    if (maxPageNo < 0) {
      maxPageNo = 0;
    }

    // With params overcome error message that "value has changed after was read"
    let paramPageNo = 0;
    let paramPageSize = 0;
    if (event > maxPageNo) {
      paramPageNo = maxPageNo;
    }
    else {
      paramPageNo = event;
    }
    // Handle very large pages
    if (this.selectedShow > this.numOfUsers) {
      paramPageSize = this.numOfUsers;
    }
    else {
      paramPageSize = this.selectedShow;
    }
    this.adminApiService.usersGet(this.selectedSort, paramPageNo, paramPageSize, this.orderType)
      .subscribe(response => {
        this.allUsers = response;
        this.numOfUsers = this.allUsers.totalNumber;
        this.usersLoaded = true;
      },
      error => {
      }
      );
  }

  ngOnInit() {
    this.populateForm();

    this.myToken = this.authService.getToken();

  }

  populateForm() {
    this.getUsers(undefined);
    // Populate the formbuilder object and add validation
    this.userGroup = this.fb.group({
      role: ['hungry',],
      firstName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      lastName: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.email, Validators.maxLength(20)]],
      password: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
      rePassword: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(20)]],
    }, { validator: passwordMatcher });
  }

  // Create a new user
  onSubmit({ value }: { value: remote.UserPut }) {
    this.createUserDisable = true;
    value.version = 0;
    value.firstName = value.firstName.trim();
    value.lastName = value.lastName.trim();
    value.email = value.email.trim();
    this.adminApiService.usersPost(value)
      .subscribe(response => {
        this.getUsers('cruser');
        this.createUserDisable = false;
        this.populateForm();
      },
      error => { this.onSubmitErrorHandling(error); });

  }

  // Handle errors when creating a new user
  onSubmitErrorHandling = function (error) {
    this.openSnackBar(error);
    this.createUserDisable = false;
    this.myId = 4;
  }

  // Handle sorting changes in the two select-option
  onOrderingChange = function () {
    // Almost identical checks with the pagination method.
    // Slight differences prevent encapsulation.
    // Could be done better? Probably with NgModelChanges ?
    let curPage = this.currentPage;
    let maxPageNo = Math.floor(this.numOfUsers / this.selectedShow);
    if ((maxPageNo % this.selectedShow) === 0) {
      maxPageNo--;
    }
    if ((maxPageNo % this.selectedShow) > 0) {
      maxPageNo++;
    }
    if (maxPageNo < 0) {
      maxPageNo = 0;
    }

    let paramPageNo = 0;
    let paramPageSize = 0;
    if (curPage > maxPageNo) {
      paramPageNo = maxPageNo;
    }
    else {
      paramPageNo = curPage;
    }

    if (this.selectedShow > this.numOfUsers) {
      paramPageSize = this.numOfUsers;
    }
    else {
      paramPageSize = this.selectedShow;
    }
    this.adminApiService.usersGet(this.selectedSort, paramPageNo, paramPageSize, this.orderType)
      .subscribe(response => {
        this.allUsers = response;
        this.numOfUsers = this.allUsers.totalNumber;
        this.usersLoaded = true;
        this.currentPage = 0;
      },
      error => {
      }
      );
  };

  // Handle ascending/descending sorting
  setOrderingType(type) {
    this.orderingTypeDisable = true;
    this.orderType = type;
    this.getUsers(type);
  };

  // Handles the success and error snackbars
  openSnackBar(msgType: any) {
    // Success snackbars
    if (msgType === 'asc') {
      this.snackBar.open('Sorting in ascedning order successful', 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    }
    else if (msgType === 'desc') {
      this.snackBar.open('Sorting in descedning order successful', 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    }
    else if (msgType === 'cruser') {
      this.snackBar.open('User created successfully', 'Ok', { duration: 5000, extraClasses: ['snackBarSuccess'] });
    }
    else {
      // Error snackbars
      var error = JSON.parse(msgType._body);
      if (msgType.status === 400) {
        this.snackBar.open(error.message, 'Ok', { extraClasses: ['snackBarError'] });
      }
      // Catch all internal server errors
      else if (msgType.status === 500) {
        this.snackBar.open('Something went wrong internally', 'Ok', { extraClasses: ['snackBarError'] });
      }
      // Catch the really obscure errors
      else {
        this.snackBar.open('Unknown error', 'Ok', { extraClasses: ['snackBarError'] });
      }
    }
  };
} // End of class

// Struct that helps password matching validation
export const passwordMatcher = (control): { [key: string]: boolean } => {
  const password = control.get('password');
  const confirm = control.get('rePassword');
  if (!password || !confirm) {
    return null;
  }
  return password.value === confirm.value ? null : { nomatch: true };
};
